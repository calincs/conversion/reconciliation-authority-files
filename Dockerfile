FROM python:3.9-slim-buster
# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

# dependency for installing jdk
RUN mkdir -p /usr/share/man/man1

# Install OpenJDK-11
RUN apt-get update && \
    apt-get install -y openjdk-11-jdk && \
    apt-get install -y ant && \
    apt-get install -y less && \
    apt-get clean;

# Fix certificate issues
RUN apt-get update && \
    apt-get install ca-certificates-java && \
    apt-get clean && \
    update-ca-certificates -f;

# Setup JAVA_HOME
ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-amd64/
RUN export JAVA_HOME

RUN apt update
RUN apt -y install vim
RUN apt -y install tmux
RUN apt -y install wget
RUN echo 'export EDITOR=vim' >> ~/.bashrc

WORKDIR /
COPY ./app /app
WORKDIR /app
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip install --no-cache-dir --upgrade -r requirements.txt

CMD ["python3", "main.py"]