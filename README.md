# Authority Files

Each of these folders contain the code needed to download the latest dump of the respective authority file (or extract relevant subset with SPARQL), extract bibliographic records, and clean the records to match the format needed for the reconciliation service.

Details on how to recreate each extracted authority file are in the corresponding folders.

You can find additional details about assumptions made in the creation of these files in the [wiki](https://gitlab.com/calincs/conversion/reconciliation-service/-/wikis/home).

## Instructions

docker build --no-cache -t recon-authorities .

docker run -p 25:25 -t -i recon-authorities

The API is now accessible locally at http://0.0.0.0:25