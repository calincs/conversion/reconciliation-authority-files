import pandas as pd
import json
import datetime
from getty_extraction.helper_functions import print_and_log


class DataClean:

    def __init__(self, data):
        self.data = data

    def clean_data(self):
        self.remove_invalid_death_years()
        self.remove_undetermined_sex()
        self.remove_invalid_language_and_nationality()
        self.remove_repeat_names()

        # Convert mysets to a string
        print_and_log("Converting Sets")
        for i in self.data:
            for col in self.data[i]:
                self.data[i][col] = repr(self.data[i][col])

        df = pd.DataFrame.from_dict(self.data, orient='index')
        df.to_csv("getty_extraction/results/cleaned_getty_results.tsv", header=True, index=False, index_label='subject_id', sep='\t')

    def remove_invalid_death_years(self):
        current_year = datetime.date.today().year
        for id in self.data:
            death_years = self.data[id].get('death_year')
            if death_years is not None:
                remove_years = []
                for year in death_years:
                    if year is None:
                        remove_years.append(year)
                    elif int(year) > current_year:
                        remove_years.append(year)
                for year in remove_years:
                    self.data[id]['death_year'].remove(year)

    def remove_undetermined_sex(self):
        for id in self.data:
            sex = self.data[id].get('sex')
            if sex is not None:
                if len(sex) > 1 and 'U' in sex:
                    self.data[id]['sex'].remove('U')

    def remove_invalid_language_and_nationality(self):
        for id in self.data:
            headers = ('language', 'language_preferred', 'nationality', 'nationality_preferred')
            for header in headers:
                values = self.data[id].get(header)
                if values is not None:
                    if len(values) > 1 and 'undetermined' in values:
                        self.data[id][header].remove('undetermined')
                    if len(values) == 1 and 'undetermined' in values:
                        self.data[id][header].remove('undetermined')

    def remove_repeat_names(self):
        for id in self.data:
            names = self.data[id].get('name')
            if names is not None:
                remove_names = []
                for name in names:
                    if name is None:
                        remove_names.append(name)
                    elif ',' in name:
                        new_name = name.split(',')[1] + ' ' + name.split(',')[0]
                        if new_name in names and len(new_name.split(' ')) == 2:
                            remove_names.append(name)
                for name in remove_names:
                    self.data[id]['name'].remove(name)