import pandas as pd
from getty_extraction.helper_functions import print_and_log, MySet
import os
import sqlite3


class Database:

    def __init__(self, db_filename, remove=False):
        self.db_filename = db_filename
        if os.path.exists(self.db_filename) and remove:
            print_and_log("Removing old {}".format(self.db_filename))
            os.remove(self.db_filename)

        self.conn = sqlite3.connect(self.db_filename)
        self.c = self.conn.cursor()
        self.columns = {'BIOGRAPHY': {'BIO_ID': 'str',
                                      'BIOGRAPHY': 'str',
                                      'BIRTH_DATE': 'str',   # PLACE_NUMBER
                                      'BIRTH_PLACE': 'str',  # PLACE_NUMBER
                                      'CONTRIBUTOR': 'str',
                                      'DEATH_DATE': 'str',
                                      'DEATH_PLACE': 'str',
                                      'PREFERRED': 'str',
                                      'SEX': 'str',
                                      'SUBJECT_ID': 'str'},
                        'LANGUAGE_RELS': {'LANGUAGE_CODE': 'str',
                                          'PREFERRED': 'str',
                                          'SUBJECT_ID': 'str',
                                          'TERM_ID': 'str',
                                          'QUALIFIER': 'str',
                                          'TERM_TYPE': 'str',
                                          'PART_OF_SPEECH': 'str',
                                          'LANG_STAT': 'str'},
                        'LOOKUP_VALUES': {'DESCRIPTOR': 'str',
                                          'ID': 'str',
                                          'STRING': 'str',
                                          'ID2': 'str'},
                        'NATIONALITY': {'DISPLAY_ORDER': 'str',
                                        'NATIONAL_ID': 'str',
                                        'NATIONALITY_CODE': 'str',
                                        'PREFERRED': 'str',
                                        'SUBJECT_ID': 'str'},
                        'PLACE': {'BRIEF_PLACE': 'str',
                                  'PLACE_NUMBER': 'str'},
                        'PTYPE_ROLE': {'PTYPE_ROLE': 'str',
                                       'PTYPE_ROLE_ID': 'str'},
                        'PTYPE_ROLE_RELS': {'DISPLAY_DATE': 'str',
                                            'DISPLAY_ORDER': 'str',
                                            'END_DATE': 'str',
                                            'HISTORIC_FLAG': 'str',
                                            'PREFERRED': 'str',
                                            'PTYPE_ROLE_ID': 'str',
                                            'START_DATE': 'str',
                                            'SUBJECT_ID': 'str'},
                        'TERM': {'AACR2_FLAG': 'str',
                                 'DISPLAY_DATE': 'str',
                                 'DISPLAY_NAME': 'str',
                                 'DISPLAY_ORDER': 'str',
                                 'END_DATE': 'str',
                                 'HISTORIC_FLAG': 'str',
                                 'OTHER_FLAGS': 'str',
                                 'PREFERRED': 'str',
                                 'START_DATE': 'str',
                                 'SUBJECT_ID': 'str',
                                 'TERM': 'str',
                                 'TERM_ID': 'str',
                                 'VERNACULAR': 'str'}
                        }
        self.query1, self.query2 = self.get_queries()
        self.lookup = {}
        self.all_data = {}

    def build_tables(self):
        print_and_log("Building database")
        for table in self.columns:
            # Get all the column names, put in form (col1, col2, col3)
            cols = "("
            for col in self.columns[table]:
                cols += col + ', '
            cols = cols[:-2] + ")"
            self.c.execute("create table if not exists {} {}".format(table, cols))
            df = pd.read_csv(os.path.join('tables', table + '.out'), sep='\t', names=self.columns[table], dtype=self.columns[table])
            df.to_sql(table, self.conn, if_exists='replace', index=False)
        self.conn.commit()
        self.c.execute("""CREATE TABLE PLACE2 AS SELECT * FROM PLACE""")
        self.conn.commit()

    def get_queries(self):
        with open('getty_extraction/queries/first_query.txt', 'r') as fp:
            query1 = fp.read()
        with open('getty_extraction/queries/second_query.txt', 'r') as fp:
            query2 = fp.read()
        return query1, query2

    def build_dictionary(self):
        print_and_log("Building data dictionary")
        # Build lookup values
        df2 = pd.read_sql_query('SELECT * from LOOKUP_VALUES', self.conn)
        for row in df2.itertuples():
            if self.lookup.get(row[2]) is None:
                self.lookup[row[2]] = row[3]

        df_chunks = pd.read_sql_query('SELECT * FROM MASTER2', self.conn, chunksize=10000)
        first_chunk = True
        col_lookup = {}
        for df in df_chunks:

            if first_chunk is True:
                cols = df.columns
                # Use pandas df to quickly iterate through final table and create dictionary of data
                idx = 1
                for row in cols:
                    col_lookup[row] = idx
                    idx += 1
                first_chunk = False

            for row in df.itertuples():
                unique_id = row[1]
                if self.all_data.get(unique_id) is None:
                    self.all_data[row[1]] = {}
                    for col in col_lookup:
                        self.all_data[row[1]][col] = MySet()

                # All of the column names are defined in the queries
                for col in col_lookup:

                    if col == 'biography':
                        if row[col_lookup.get('biography_preferred')] != 'P':
                            self.all_data[unique_id]['biography'].add(row[col_lookup.get(col)])
                        else:
                            self.all_data[unique_id]['biography_preferred'].add(row[col_lookup.get('biography')])
                    elif col == 'nationality':
                        if row[col_lookup.get('nationality_preferred')] != 'P':
                            self.all_data[unique_id]['nationality'].add(self.get_lookup_val(row[col_lookup.get(col)]))
                        else:
                            self.all_data[unique_id]['nationality_preferred'].add(self.get_lookup_val(row[col_lookup.get('nationality')]))
                    elif col == 'name':
                        if row[col_lookup.get('name_preferred')] != 'P':
                            self.all_data[unique_id]['name'].add(row[col_lookup.get(col)])
                        else:
                            self.all_data[unique_id]['name_preferred'].add(row[col_lookup.get('name')])
                    elif col == 'language':
                        if row[col_lookup.get('language_preferred')] != 'P':
                            self.all_data[unique_id]['language'].add(self.get_lookup_val(row[col_lookup.get(col)]))
                        else:
                            self.all_data[unique_id]['language_preferred'].add(self.get_lookup_val(row[col_lookup.get('language')]))
                    elif col not in ('biography_preferred', 'nationality_preferred', 'language_preferred', 'name_preferred'):
                        value = row[col_lookup.get(col)]
                        if value is not None:
                            self.all_data[unique_id][col].add(value)

        # Build final df to easily convert dictionary to tsv
        return self.all_data

    def get_lookup_val(self, value):
        lookup_val = self.lookup.get(value)
        if lookup_val is not None:
            return lookup_val
        else:
            print(lookup_val)
            return None
