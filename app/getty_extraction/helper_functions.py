from s3Helpers import S3Helper

import os
import zipfile
import re
import wget
import logging
import requests


def get_latest_data():
	# Gets the latest Getty ULAN database dump from Getty or S3
	# Stores it locally and on S3
	# Also checks if the db file already exists for this datadump and downloads it from S3 if so

	response = requests.get("http://ulandownloads.getty.edu")
	latest_filename = re.findall(r"VocabData\/(ulan_rel_[0-9]+\.zip)", str(response.text))[0]
	download_link = f"http://ulandownloads.getty.edu/VocabData/{latest_filename}"

	s3_path = f"recon-api/authority_extraction/data_dumps/{latest_filename}"

	# Check if the file already exists on S3
	s3 = S3Helper()
	s3_file = s3.list_objects(s3_path)

	# If file not backed up in S3 already, download from Getty site
	if s3_file == []:
		print("Latest Getty data dump not found on S3. Downloading now from Getty and uploading to S3")
		wget.download(download_link, out=latest_filename)
		s3.put_file(s3_path, latest_filename)

	# If file is backed up on S3 already, download from S3
	else:
		print("Latest Getty data dump found on S3. Downloading now from S3.")
		s3.fget_object(s3_path, latest_filename)

	# Unzip file in the container and delete zip file
	try:
		with zipfile.ZipFile(latest_filename, 'r') as z:
			z.extractall('tables')
			print("Extracted Getty files")
			if os.path.exists(latest_filename):
				os.remove(latest_filename)
	except Exception as error:
		print("Invalid Getty Data Dump File: ")
		print(error)

	# Check for database file for this data dump on S3
	db_filename = latest_filename.replace('.zip', '.db')
	s3_path = f"recon-api/authority_extraction/data_dumps/{db_filename}"
	s3_file = s3.list_objects(s3_path)
	if s3_file != []:
		print("Latest database file for the Getty dump is already on S3. Downloading now.")
		s3.fget_object(s3_path, db_filename)

	return db_filename


def upload_db(local_filename, s3_filename):
	s3_path = f"recon-api/authority_extraction/data_dumps/{s3_filename}"
	s3 = S3Helper()

	try:
		print("Uploading db file to S3")
		s3.put_file(s3_path, local_filename)
	except Exception as error:
		print("Could not upload db file to S3")
		print(error)


def print_and_log(message):
	"""Prints message and logs to logfile"""
	print(message)
	logging.info(message)


class MySet(set):
	"""
	Subclass of set so I can override the __repr__ for easier viewing in pandas / debugging
	"""
	def __repr__(self):
		"""
		:return: If set is empty returns a empty string, if otherwise returns multiple values with comma seperation
		"""
		if len(self) == 0:
			return ''
		output = ''
		for elm in self:
			output += str(elm) + " ~ "
		return output[:-2]

	def __str__(self):
		"""
		:return: Overrides string method into form: "'item1' 'item2' 'etc...'"
		"""
		if len(self) == 0:
			return ''
		output = ''
		for elm in self:
			output += "'" + str(elm) + "'" + " "
		return output[:-1]
