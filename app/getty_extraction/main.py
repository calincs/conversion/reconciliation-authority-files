import os
import logging
import json

from getty_extraction.helper_functions import get_latest_data, upload_db
from getty_extraction.database import Database
from getty_extraction.cleanData import DataClean

from sparkClean import clean as sparkCleaner
import s3Helpers


def main():
    logging.basicConfig(level=logging.DEBUG, filename="logfile", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")
    db_filename = get_latest_data()

    remake_db = False
    if os.path.exists(db_filename) is False:
        print("Getty DB to be remade")
        remake_db = True

    print("Initializing Getty DB")
    db = Database(db_filename, remove=False)

    # Check if db file already exists locally
    # Build if not
    if remake_db:
        print("build tables")
        db.build_tables()
        upload_db(db_filename, db_filename.replace(".db", "_1.db"))

        print("query1")
        db.c.execute(db.query1)
        upload_db(db_filename, db_filename.replace(".db", "_2.db"))

        print("query2")
        db.c.execute(db.query2)
        upload_db(db_filename, db_filename.replace(".db", ".db"))

    data = db.build_dictionary()
    clean = DataClean(data)
    clean.clean_data()

    db_filename = "ulan_rel_0622.db"

    with open("getty_extraction/results/getty_metadata.json", "w") as meta_fp:
        json.dump({"authorityVersion": db_filename.replace('.db', ''),
                   "authorityDate": "06-22",
                   "authoritySource": 'http://ulandownloads.getty.edu/VocabData/' + db_filename.replace('.db', '.zip')}, meta_fp)


    # # download cleaned_getty_results.tsv from S3
    s3 = s3Helpers.S3Helper()
    # s3.fget_object("recon-api/authority_extraction/data_dumps/cleaned_getty_results.tsv", "/app/getty_extraction/results/cleaned_getty_results.tsv")

    # make a sample version
    with open("/app/getty_extraction/results/cleaned_getty_results.tsv", "r") as f_full:
        with open("/app/getty_extraction/results/cleaned_getty_results_sample.tsv", "w") as f_out:
            sample = [next(f_full) for _ in range(6000)]
            for line in sample:
                f_out.write(line)


    # run sparkcleaner
    print("spark clean")
    sparkCleaner("getty_people", "/app/getty_extraction/results/cleaned_getty_results_sample.tsv", "/app/getty_extraction/results/spark_cleaned_getty_people_sample_results")
    sparkCleaner("getty_people", "/app/getty_extraction/results/cleaned_getty_results.tsv", "/app/getty_extraction/results/spark_cleaned_getty_people_results")

    # upload final data to S3 getty folder dev release
    print("upload to S3")
    s3.bulk_upload("getty_people")
    s3.bulk_upload("getty_people_sample")


if __name__ == '__main__':
    # First check if can be resumed
    main()
    # Start loop
    # while True:
    #     # Wait for 30 days
    #     #print_and_log("Sleeping for 30 days")
    #     time.sleep(2629800)
