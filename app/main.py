import json
import schedule
import time
import requests
import re
import os
from datetime import datetime, date
from dotenv import load_dotenv

from s3Helpers import S3Helper
from wikidata_extraction.main import main as wikidata_main
from viaf_extraction.main import main as viaf_main
from getty_extraction.main import main as getty_main


def str_to_date(str_date):
	# converts a date string of the form yyyy-mm-dd to a datetime date object
	return datetime.strptime(str_date, "%Y-%m-%d").date()


def compare_dates(date1, date2):
	# returns the number of days between date1 and date2 as an integer
	# date1, and date2 should be strings of datetime date objects
	return (str_to_date(date1) - str_to_date(date2)).days


def get_getty_release_version():
	# returns a version of the form 0622 which seems to approximately line up with MMYY
	response = requests.get("http://ulandownloads.getty.edu")
	return re.findall(r"VocabData\/ulan_rel_([0-9]*)\.zip", str(response.text))[0]


def get_viaf_release_date():
	# returns a date of the form 20230327 ie., YYYYMMDD
	response = requests.get("https://viaf.org/viaf/data/")
	return re.findall(r"https:\/\/viaf\.org\/viaf\/data\/viaf-([0-9]*)\-clusters\.xml\.gz", str(response.text))[0]


def get_update_log():
	# Gets the log of authority version dates from S3 and returns as a dictionary

	s3 = S3Helper()
	print(f"{s3.base}/logs/update_log.json")
	log_str = s3.get_object(f"{s3.base}/logs/update_log.json")
	log_dict = json.loads(log_str)

	return log_dict


def update_log(authority, today):
	# Gets the log of authority version dates from S3
	# Updates that log with today's date for the authority specified

	log_dict = get_update_log()
	log_dict[authority] = today

	with open("logs/update_log.json", "w") as log_out:
		json.dump(log_dict, log_out)

	s3 = S3Helper()
	s3.put_file(f"{s3.base}/logs/update_log.json", "logs/update_log.json")


def create_error_log(error_str, authority, today):
	error_dict = {"date": today, "authority": authority, "error": error_str}

	with open("logs/error_log.json", "w") as log_out:
		json.dump(error_dict, log_out)

	s3 = S3Helper()
	s3.put_file(f"{s3.base}/logs/{today}_{authority}.json", "logs/error_log.json")


def check_if_update(authority, today):
	# authority can be "wikidata", "viaf", "getty"

	# get log of authority update dates from S3
	log_dict = get_update_log()

	# wikidata and getty update every 30 days
	if authority == "wikidata":
		if compare_dates(today, log_dict[authority]) > 30:
			return True

	if authority == "getty":
		getty_latest_date = get_getty_release_version()
		if getty_latest_date != log_dict[authority]:
			return True

	# viaf updates upon new data dump release
	if authority == "viaf":
		viaf_latest_date = get_viaf_release_date()
		if viaf_latest_date != log_dict[authority]:
			return True

	return False


def update_authority(authority, today):
	print(f"[{today}]  {authority} updating.")

	success = False

	if authority == "wikidata":
		print("\n\nUPDATING WIKIDATA\n\n")
		wikidata_main()
		success = True  ## TODO

	if authority == "viaf":
		print("\n\nUPDATING VIAF\n\n")
		viaf_main()
		success = True  ## TODO

	if authority == "getty":
		print("\n\nUPDATING GETTY\n\n")
		getty_main()
		success = True  ## TODO

	return success


def scheduled_updates():
	# yyyy-mm-dd
	today = str(date.today())

	for authority in ["wikidata", "getty", "viaf"]:
		print(f"[{today}]  Checking if authority {authority} needs updating.")
		update_status = check_if_update(authority, today)

		if update_status:
			print(f"[{today}]  Authority {authority} updating.")
			success = update_authority(authority, today)

			if success:
				# update log of authority update dates from S3
				print(f"[{today}]  {authority} updated successfully.")

				# log the release version if it exists
				# wikidata will be today's date since we query their live data
				if authority == "viaf":
					version = get_viaf_release_date()
				elif authority == "getty":
					version = get_getty_release_version()
				else:
					version = today

				update_log(authority, version)

			else:
				# update error log file
				# TODO test error log properly
				create_error_log("test error info", authority, today)
				print(f"[{today}]  {authority} update failed.")

		else:
			print(f"[{today}]  {authority} already up to date.")


# Every 24 hours, check if any authorities need to be updated.
# Update if yes.
def main():
	load_dotenv("/app/.env")

	testing = os.environ.get("TESTING")

	if testing == "True":
		schedule.every(0.1).minutes.do(scheduled_updates)
	else:
		schedule.every().day.at("01:00").do(scheduled_updates)

	while True:
		schedule.run_pending()
		time.sleep(1)


if __name__ == "__main__":
	main()
