import io
import os
from minio import Minio
from minio.error import S3Error
from minio.commonconfig import CopySource
import time
from wikidata_extraction.helperFunctions import print_and_log
from pathlib import Path


"""
Data Upload Strategy By the authority files creation service

when a new data file is ready for upload to S3:
changes both release and archive to IN_PROGRESS
copies all files in release to archive
changes archive status to COMPLETE
check that no recon job is currently running (TODO)
deletes files that were successfully copied
uploads new files to release
changes release status to COMPLETE


How recon service should act
- before starting a job check that files in release has COMPLETE status
    - if COMPLETE
        - then proceed as usual
    - if IN_PROGRESS
        - check if archive files are COMPLETE
            - if COMPLETE
                - use those for the current job
            - if IN_PROGRESS
                - keep polling, and don't start the job until it says COMPLETE
"""


class S3Helper:

    def __init__(self):
        self.access = os.environ.get("LINCS_S3_ACCESS")
        self.secret = os.environ.get("LINCS_S3_SECRET")
        self.client = Minio('aux.lincsproject.ca',
                            access_key=self.access,
                            secret_key=self.secret,
                            secure=True,
                            region='ca')
        self.bucket = "users"
        self.job_id = int(time.time())
        # Blank files used for logging upload status
        Path("IN_PROGRESS").touch()
        Path("COMPLETE").touch()

        self.deploy_env = self.get_env_bucket()
        self.base = os.path.join("recon-api", "authority_extraction", self.deploy_env)

    def get_local_data_paths(self, authority: str):
        """
        Returns the paths within the docker container for final authority files and metadata
        That should be uploaded to S3
        :param authority: getty_people, viaf_works, viaf_expressions, viaf_authors, wikidata_authors, wikidata_works and all of those options with _sample at the end
        """

        local_base = f"{authority.split('_')[0]}_extraction/results/"
        meta_source = f"{local_base}{authority.split('_')[0]}_metadata.json"

        full_source = f"{local_base}spark_cleaned_{authority}_results"
        context_source = f"{local_base}spark_cleaned_{authority}_results_db_context"
        id_source = f"{local_base}spark_cleaned_{authority}_results_db_id"

        return [full_source, context_source, id_source, meta_source]

    def get_s3_data_paths(self, authority: str, release_path: str):
        """
        Example output for full_target for wikidata_works with release for release_path
            "users/recon-api/authority_extraction/dev/wikidata_works/release/wikidata-works_cleaned.parquet"
        :param authority: getty_people, viaf_works, viaf_expressions, viaf_authors, wikidata_authors, wikidata_works and all of those options with _sample at the end
        :param release_path: archive or release
        """
        authority_inner = authority.replace("_", "-")

        full_target = os.path.join(self.base, authority.replace("_sample", ""), release_path, f"{authority_inner}_cleaned.parquet")
        context_target = os.path.join(self.base, authority.replace("_sample", ""), release_path, f"{authority_inner}_db_context")
        id_target = os.path.join(self.base, authority.replace("_sample", ""), release_path, f"{authority_inner}_db_id")
        metadata_target = os.path.join(self.base, authority.replace("_sample", ""), release_path, "metadata.json")

        return [full_target, context_target, id_target, metadata_target]

    def get_s3_status_paths(self, authority: str, release_path: str, release_status: str):
        """
        Outputs paths for where COMPLETE or IN_PROGRESS files go in S3 for each authority
        :param base: users/recon-api/authority_extraction/dev/ or  users/recon-api/authority_extraction/prod/
        :param authority: getty_people, viaf_works, viaf_expressions, viaf_authors, wikidata_authors, wikidata_works and all of those options with _sample at the end
        :param release_path: archive or release
        :param release_status: COMPLETE or IN_PROGRESS
        """

        status_target = os.path.join(self.base, authority.replace("_sample", ""), release_path, release_status)

        return status_target

    def check_file(self, filepath):
        """
        Checks to see if a file exits on the s3
        :param filepath: full path of file on s3
        """
        objects = self.list_objects(file_prefix=filepath)
        if len(objects) > 0:
            return True
        else:
            return False

    def change_s3_status(self, authority, archive=True, complete=True):
        """
        Change S3 status file to in progress or complete
        :param authority: getty_people, viaf_works, viaf_expressions, viaf_authors, wikidata_authors, wikidata_works and all of those options with _sample at the end
        :param archive: if true will modify the authority's /archive's status file else /release's
        :param complete: if true will remove IN_PROGRESS file and upload COMPLETE file
                         if false will remove COMPLETE file and upload IN_PROGRESS file
        """

        if archive:
            release_status = "archive"
        else:
            release_status = "release"

        # complete is False so we change the status of the target folder to in progress
        if complete is False:

            # Try to delete complete file
            try:
                self.client.remove_object(self.bucket, self.get_s3_status_paths(authority, release_status, "COMPLETE"))
            except S3Error as e:
                print_and_log(e)

            # Change to in progress
            self.put_file(self.get_s3_status_paths(authority, release_status, "IN_PROGRESS"), "IN_PROGRESS")

        # complete is True so we change the status of the target folder to complete
        elif complete is True:

            # Try to delete progress file
            try:
                self.client.remove_object(self.bucket, self.get_s3_status_paths(authority, release_status, "IN_PROGRESS"))
            except S3Error as e:
                print_and_log(e)

            # Change to complete
            self.put_file(self.get_s3_status_paths(authority, release_status, "COMPLETE"), "COMPLETE")

    def bulk_upload(self, authority):
        """
        Uploads authority files along with maintaining IN_PROGRESS & COMPLETE files
        :param authority: getty_people, viaf_works, viaf_expressions, viaf_authors, wikidata_authors, wikidata_works and all of those options with _sample at the end
        """

        # Change release and archive status to in progress
        self.change_s3_status(authority, archive=True, complete=False)
        self.change_s3_status(authority, archive=False, complete=False)

        # Copy all release items into archive for this authority
        # Then say that archive is complete
        self.update_archive(authority)

        # Upload new files to release and update status in S3 to complete
        self.update_release(authority)
        self.change_s3_status(authority, archive=False, complete=True)

    def put_file(self, s3_path, file_path):
        """
        Upload a local file (input_bytes) to the s3 bucket (client) and save it at output_path
        Takes input data in bytes and converts it into a stream
        :param s3_path: filepath where you want saved on s3
        :param file_path: local location of file
        """
        try:
            with open(file_path, 'rb') as fp:
                input_bytes = fp.read()
        except FileNotFoundError as e:
            print(e)
            return 0
        input_stream = io.BytesIO(input_bytes)
        try:
            self.client.put_object(self.bucket, s3_path, input_stream, len(input_bytes))
            print(f"[{self.job_id}] {file_path} Data successfully uploaded to S3.")
            self.job_id += 1
            return 1
        except S3Error as err:
            print(f"[{self.job_id}] File could not be uploaded to s3 storage. \n", err)
            return 0

    def put_folder(self, s3_dir, file_dir):
        """
        Uploads each file in the local file_dir directory to the s3_dir directory
        """
        for filename in os.listdir(file_dir):
            self.put_file(os.path.join(s3_dir, filename.replace(".csv", ".tsv")), os.path.join(file_dir, filename))

    def update_release(self, authority):
        """
        Uploads latest results to appropriate release folder in S3
        """

        # data files
        self.put_folder(self.get_s3_data_paths(authority, "release")[0], self.get_local_data_paths(authority)[0])
        self.put_folder(self.get_s3_data_paths(authority, "release")[1], self.get_local_data_paths(authority)[1])
        self.put_folder(self.get_s3_data_paths(authority, "release")[2], self.get_local_data_paths(authority)[2])

        # metadata file
        self.put_file(self.get_s3_data_paths(authority, "release")[3], self.get_local_data_paths(authority)[3])

    def update_archive(self, authority):
        """
        Delete archives, copies release to archive, updates archive status, if copy is successful deletes release
        :param authority: getty_people, viaf_works, viaf_expressions, viaf_authors, wikidata_authors, wikidata_works and all of those options with _sample at the end
        """

        archive_files = self.get_s3_data_paths(authority, "archive")
        release_files = self.get_s3_data_paths(authority, "release")

        # Delete archive contents except for progress file
        for archive_file in archive_files:
            objects = self.list_objects(file_prefix=archive_file)
            for obj in objects:
                self.delete_file(obj)

        # Copy release to archive except for progress file
        final_copy_status = True
        for file_index in [0, 1, 2, 3]:
            release_objects = self.list_objects(file_prefix=release_files[file_index])
            for release_object in release_objects:
                copy_status = self.copy_file(old_file_path=release_object, new_file_path=release_object.replace(release_files[file_index], archive_files[file_index]))
            if copy_status is not None:
                print(f"FAILED TO COPY S3 FILES FROM RELEASE TO ARCHIVE:  {release_object}  to  {release_object.replace(release_files[file_index], archive_files[file_index])}")
                final_copy_status = False

        # TODO do this later. Once we've fully tested.
        # TODO handle case where there are not files in release yet
        # Don't continue if the copy failed
        #if final_copy_status is True:

        # Change status of archive to COMPLETE
        self.change_s3_status(authority, archive=True, complete=True)

        # Delete release contents except for progress file
        for release_file in release_files:
            objects = self.list_objects(file_prefix=release_file)
            for obj in objects:
                self.delete_file(obj)

    def copy_file(self, old_file_path, new_file_path):
        print_and_log("Moving {} to {}".format(old_file_path, new_file_path))
        try:
            result = self.client.copy_object(self.bucket,
                                             new_file_path,
                                             CopySource(self.bucket, old_file_path),)
            return result
        except S3Error as e:
            print_and_log(e)
        except Exception as e:
            print_and_log(e)

        return None

    def delete_file(self, s3_file_path):
        print_and_log(f"Deleting {s3_file_path} from s3")
        try:
            self.client.remove_object(self.bucket, s3_file_path)
        except S3Error as e:
            print_and_log(e)

    def list_objects(self, file_prefix=None):
        object_list = []
        objects = self.client.list_objects(self.bucket, recursive=True, prefix=file_prefix)
        try:
            for object in objects:
                object_list.append(object.object_name)
        except S3Error as e:
            print_and_log(e)

        return object_list

    def get_object(self, object_name):
        data = ""
        try:
            response = self.client.get_object(self.bucket, object_name)
            if response:
                for d in response.stream(32 * 1024):
                    data_decode = d.decode()
                    data_decode_clean = data_decode.replace("'", '"')
                    if data == "":
                        data = data_decode_clean
                    else:
                        data = data + ", " + data_decode_clean
                response.close()
                response.release_conn()
            else:
                print(f"No S3 response on get_object {object_name}")
        except Exception as err:
            print("S3 get object error: ", err)

        return data

    def fget_object(self, object_name, filename):
        self.client.fget_object(self.bucket, object_name, filename)

    def check_success(self, dir_path, job_id):
        """
        Checks the success of a job
        Return 1 if there is a success file in the job's output folder
        Return 0 if there is no success file
        """
        obj_path = dir_path + job_id + "/_SUCCESS"
        try:
            self.client.stat_object(self.bucket, obj_path)
            return 1
        except S3Error as e:
            print(e)
            return 0
        finally:
            return 0

    def get_env_bucket(self):
        # Get the name used within S3 paths based on the deployment (review, stage, prod)
        # Return "dev" or "prod" since those are used for S3 paths for authority files
        # Stage falls under prod in this case

        deploy_target = os.environ.get("DEPLOY_TARGET")

        if "review" in deploy_target:
            env_bucket = "dev"
        else:
            env_bucket = "prod"

        return env_bucket
