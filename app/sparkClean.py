
from pyspark.context import SparkConf, SparkContext
from pyspark.sql import SparkSession

from sparkNormalize import author_normalize, blocking, common_data_normalize, dates_normalize, duplicate_original_cols
from sparkNormalize import gender_normalize, name_list_split, register_udfs, drop_extra_columns, titles_normalize
from sparkNormalize import separate_ids, separate_originals, rename_columns, temp_rename_columns


def get_spark():
	conf = SparkConf()
	conf.set('spark.driver.memory', '10g')
	conf.set("spark.sql.shuffle.partitions", "8")
	sc = SparkContext.getOrCreate(conf=conf)
	spark = SparkSession(sc)
	return spark


def clean(target_kg, input_path, output_base_path):
	# target_kg options: wikidata_works, wikidata_authors, viaf_works, viaf_expressions, getty_people

	debug = True

	spark = get_spark()
	df = spark.read.option("header", "true").option("delimiter", "\t").csv(input_path)

	if debug:
		print("1" * 20)
		print(df.show(5))

	df = temp_rename_columns(target_kg, df)

	print("2" * 20)
	print(df.show(5))

	df = drop_extra_columns(target_kg, df)

	#df = name_list_split(target_kg, df)

	dfWhitespace, dfPunctuation, dfNameConcat, dfGender, secLongestWord, longestWord, firstWord, lastWord = register_udfs(spark)
	df = duplicate_original_cols(target_kg, df)
	if debug:
		print("3" * 20)
		print(df.show(5))

	df = author_normalize(target_kg, df, dfNameConcat)
	if debug:
		print("4" * 20)
		print(df.show(5))

	df = dates_normalize(target_kg, df)
	if debug:
		print("5" * 20)
		print(df.show(5))

	df = titles_normalize(target_kg, df)
	if debug:
		print("6" * 20)
		print(df.show(5))

	try:
		df = gender_normalize(target_kg, df, dfGender)
	except Exception:
		print("Skipped gender_normalize. Column likely did not exist")
	if debug:
		print("7" * 20)
		print(df.show(5))

	df = common_data_normalize(target_kg, df, dfWhitespace, dfPunctuation)
	if debug:
		print("8" * 20)
		print(df.show(5))
	df = blocking(target_kg, df, longestWord, secLongestWord, firstWord, lastWord)
	if debug:
		print("9" * 20)
		print(df.show(5))

	df, df_id = separate_ids(target_kg, df)
	if debug:
		print("10" * 20)
		print(df.show(5))

		print("11" * 20)
		print(df_id.show(5))

	df, df_og = separate_originals(target_kg, df)
	if debug:
		print("12" * 20)
		print(df.show(5))

	df = rename_columns(target_kg, df)
	if debug:
		print("14" * 20)
		print(df.show(5))

	df_id = rename_columns(target_kg + "_id", df_id)
	if debug:
		print("15" * 20)
		print(df_id.show(5))

	df_og = rename_columns(target_kg + "_context", df_og)
	if debug:
		print("16" * 20)
		print(df_og.show(5))

	# Save each df as tsv in the container

	# This one is for debugging only
	df\
		.repartition(1)\
		.write.format("com.databricks.spark.csv")\
		.mode('overwrite')\
		.option("header", "true")\
		.option("delimiter", "\t")\
		.save(output_base_path + "_full_tsv")\

	# note being saved as .csv but with tabs as delimiter. Couldn't get spark to output as .tsv
	df_id\
		.repartition(1)\
		.write.format("com.databricks.spark.csv")\
		.mode('overwrite')\
		.option("header", "true")\
		.option("delimiter", "\t")\
		.save(output_base_path + "_db_id")\

	df_og\
		.repartition(1)\
		.write.format("com.databricks.spark.csv")\
		.mode('overwrite')\
		.option("header", "true")\
		.option("delimiter", "\t")\
		.save(output_base_path + "_db_context")\

	# Save as parquet
	df.write.mode("overwrite").parquet(output_base_path)
