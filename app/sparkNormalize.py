import string
import pyspark.sql.functions as f
from pyspark.sql import types
from pyspark.sql.functions import col, expr, lit, lower, concat
from pyspark.sql.functions import regexp_extract, udf
from pyspark.sql.dataframe import DataFrame


def register_udfs(spark):
	"""
	Register all user defined functions related to data normalization
	"""

	dfWhitespace = udf(lambda z: df_whitespace(z))
	spark.udf.register("dfWhitespace", dfWhitespace)

	dfPunctuation = udf(lambda z: df_punctuation(z))
	spark.udf.register("dfPuntuation", dfPunctuation)

	dfNameConcat = udf(lambda z: df_name_concat(z))
	spark.udf.register("dfNameConcat", dfNameConcat)

	dfGender = udf(lambda z: df_gender(z))
	spark.udf.register("dfGender", dfGender)

	secLongestWord = udf(lambda z, y: second_longest_word(z, y))
	spark.udf.register("secLongestWord", secLongestWord)

	longestWord = udf(lambda z: longest_word(z))
	spark.udf.register("longestWord", longestWord)

	firstWord = udf(lambda z, y: first_word(z, y))
	spark.udf.register("firstWord", firstWord)

	lastWord = udf(lambda z: last_word(z))
	spark.udf.register("lastWord", lastWord)

	return dfWhitespace, dfPunctuation, dfNameConcat, dfGender, secLongestWord, longestWord, firstWord, lastWord


# TODO: string normalize should include all columns used for reconciling
def get_headings(setting, target_kg):
	"""
	List of columns that need various normalizations for each authority file

	The "duplicate" headings are ones that will be duplicated, resulting in columnName and columnName_original.
	We duplicate them so that after we link the data using the normalized values,
	we can return the original value to the user

	The "normalize" headings are ones that will undergo general string cleaning
	(like lowercasing, remove punctuation, etc.)
	"""
	headings = {
		"viaf_works": {
			"duplicate": [
				"workName1",
				"workName2",
				"authorName1",
				"authorName2"],
			"string_normalize": [
				"workName1",
				"workName2",
				"authorName1",
				"authorName2"],
			"date_normalize": [
				"authorBirthYear",
				"authorDeathYear"],
			"gender_normalize": [
				"authorSex"]
		},
		"viaf_expressions": {
			"duplicate": [
				"expressionName1",
				"expressionName2",
				"workName1",
				"workName2",
				"authorName1",
				"authorName2"],
			"string_normalize": [
				"expressionName1",
				"expressionName2",
				"authorName1",
				"authorName2"],
			"date_normalize": [
				"authorBirthYear",
				"authorDeathYear"],
			"gender_normalize": [
				"authorSex"]
		},
		"viaf_authors": {
			"duplicate": [
				"authorName1",
				"authorName2"],
			"string_normalize": [
				"authorName1",
				"authorName2"],
			"date_normalize": [
				"authorBirthYear",
				"authorDeathYear"],
			"gender_normalize": [
				"authorSex"]
		},
		"wikidata_authors": {
			"duplicate": [
				"author_Name_1",
				"author_Name_2"],
			"string_normalize": [
				"author_Name_1",
				"author_Name_2"],
			"date_normalize": [
				"author_BirthYear",
				"author_DeathYear"],
			"gender_normalize": [
				"author_Sex"]
		},
		"wikidata_works": {
			"duplicate": [
				"work_Name_1",
				"work_Name_2",
				"author_Name_1",
				"author_Name_2"],
			"string_normalize": [
				"work_Name_1",
				"work_Name_2",
				"author_Name_1",
				"author_Name_2"],
			"date_normalize": [
				"author_BirthYear",
				"author_DeathYear",
				"work_PublicationDate"],
			"gender_normalize": [
				"author_Sex"]
		},
		"getty_people": {
			"duplicate": [
				"author_Name_1",
				"author_Name_2",
				"author_BirthPlace",
				"author_DeathPlace"],
			"string_normalize": [
				"author_Name_1",
				"author_Name_2",
				"author_BirthPlace",
				"author_DeathPlace"],
			"date_normalize": [
				"author_BirthYear",
				"author_DeathYear"],
			"gender_normalize": [
				"author_Sex"]
		}
	}
	return headings[target_kg][setting]


def stopwords():
	# based on the nltk stop word list with some additions based on other common short words found in the data
	# these words won't be used as blocking keys for work titles
	return [line.strip() for line in open("stopwords.txt", 'r')]


def drop_extra_columns(target_kg, df):
	"""
	Rename and drop certain columns for each authority file to match the expected format of the reconciliation service
	The expected columns list for each is listed in ...
	TODO list in one place what columns should be in each authority file, which are used for linking, which are extra info
	"""
	if target_kg == "viaf_works":
		#df = df.withColumn("unique_id", df["workID"])
		# df = df.select("*").withColumn("unique_id", monotonically_increasing_id())
		df = df.drop("workLibraries")
		pass
	elif target_kg == "viaf_expressions":
		#df = df.withColumn("unique_id", df["expressionID"])
		df = df.drop("expressionLibraries")
		# df = df.drop("expressionName1:1")
		# df = df.drop("expressionName2:1")
	elif target_kg == "viaf_authors":
		df = df.drop("workWorldcat")
		df = df.drop("workWikidata")
		df = df.drop("workID")
		df = df.drop("workName2")
		df = df.drop("workName1")
		df = df.drop("workLibraries")
		df = df.drop("workLanguage")
	return df


def temp_rename_columns(target_kg, df):
	if target_kg == "getty_people":
		# make unique id column out of person IDs
		df = df.withColumn("unique_id", df["subject_id"])

		# add http://vocab.getty.edu/page/ulan/ to subject_id
		df = df.withColumn("author_Getty_URI", concat(lit("http://vocab.getty.edu/page/ulan/"), col("subject_id")))

		# rename columns
		#df = df.withColumnRenamed("subject_id", "author_Getty_URI")
		df = df.withColumnRenamed("biography", "author_Description")
		df = df.withColumnRenamed("biography_preferred", "author_DescriptionAll")
		df = df.withColumnRenamed("birth_year", "author_BirthYear")
		df = df.withColumnRenamed("death_year", "author_DeathYear")
		df = df.withColumnRenamed("sex", "author_Sex")
		df = df.withColumnRenamed("birth_place", "author_BirthPlace")
		df = df.withColumnRenamed("death_place", "author_DeathPlace")
		df = df.withColumnRenamed("nationality", "author_NationalityAll")
		df = df.withColumnRenamed("nationality_preferred", "author_Nationality")
		df = df.withColumnRenamed("language", "author_LanguageAll")
		df = df.withColumnRenamed("language_preferred", "author_Language")
		df = df.withColumnRenamed("name", "author_Name_2")
		df = df.withColumnRenamed("name_preferred", "author_Name_1")
		df = df.withColumnRenamed("role", "author_Role")


		# replace ~ with space comma space
		# pull out 1 more top name and add as author_Name_2
		# handle date ranges

	return df


def rename_columns(target_kg, df):
	if target_kg == "viaf_works":
		df = df.withColumnRenamed("workName1_original", "work_Name_1")
		df = df.withColumnRenamed("workName2_original", "work_Name_2")
		df = df.withColumnRenamed("authorName1_original", "author_Name_1")
		df = df.withColumnRenamed("authorName2_original", "author_Name_2")
		df = df.withColumnRenamed("workName1", "work_Name_1")
		df = df.withColumnRenamed("workName2", "work_Name_2")
		df = df.withColumnRenamed("workName3", "work_Name_3")
		df = df.withColumnRenamed("authorName1", "author_Name_1")
		df = df.withColumnRenamed("authorName2", "author_Name_2")
		df = df.withColumnRenamed("authorCountryOrigin", "author_CountryOrigin")
		df = df.withColumnRenamed("authorBirthYear", "author_BirthYear")
		df = df.withColumnRenamed("authorDeathYear", "author_DeathYear")
		df = df.withColumnRenamed("authorID", "author_VIAF_URI")
		df = df.withColumnRenamed("authorISNI", "author_ISNI_ID")
		df = df.withColumnRenamed("authorWorldcat", "author_Worldcat_URI")
		df = df.withColumnRenamed("authorSex", "author_Sex")
		df = df.withColumnRenamed("authorWikidata", "author_Wikidata_URI")
		df = df.withColumnRenamed("work_ID", "work_VIAF_URI")
		df = df.withColumnRenamed("authorLanguage", "author_Language")
		df = df.withColumnRenamed("workLanguage", "work_Language")
		df = df.withColumnRenamed("workID", "work_VIAF_URI")
		df = df.withColumnRenamed("workWorldcat", "work_Worldcat_URI")
		df = df.withColumnRenamed("workWikidata", "work_Wikidata_URI")
		df = df.drop('work_VIAF_URI')
		df = df.drop('work_Worldcat_URI')
		df = df.drop('work_Wikidata_URI')
	if target_kg == "viaf_works_context":
		df = df.withColumnRenamed("workName1_original", "work_Name_1")
		df = df.withColumnRenamed("workName2_original", "work_Name_2")
		df = df.withColumnRenamed("authorName1_original", "author_Name_1")
		df = df.withColumnRenamed("authorName2_original", "author_Name_2")
		df = df.withColumnRenamed("authorCountryOrigin", "author_CountryOrigin")
		df = df.withColumnRenamed("authorBirthYear", "author_BirthYear")
		df = df.withColumnRenamed("authorDeathYear", "author_DeathYear")
		df = df.withColumnRenamed("authorID", "author_VIAF_URI")
		df = df.withColumnRenamed("authorISNI", "author_ISNI_ID")
		df = df.withColumnRenamed("authorWorldcat", "author_Worldcat_URI")
		df = df.withColumnRenamed("authorSex", "author_Sex")
		df = df.withColumnRenamed("authorWikidata", "author_Wikidata_URI")
		df = df.withColumnRenamed("work_ID", "work_VIAF_URI")
		df = df.withColumnRenamed("authorLanguage", "author_Language")
		df = df.withColumnRenamed("workLanguage", "work_Language")
	if target_kg == "viaf_works_id":
		df = df.withColumnRenamed("workID", "work_VIAF_URI")
		df = df.withColumnRenamed("workWorldcat", "work_Worldcat_URI")
		df = df.withColumnRenamed("workWikidata", "work_Wikidata_URI")
	if target_kg == "viaf_authors":
		df = df.withColumnRenamed("authorName1_original", "author_Name_1")
		df = df.withColumnRenamed("authorName2_original", "author_Name_2")
		df = df.withColumnRenamed("authorName1", "author_Name_1")
		df = df.withColumnRenamed("authorName2", "author_Name_2")
		df = df.withColumnRenamed("authorCountryOrigin", "author_CountryOrigin")
		df = df.withColumnRenamed("authorBirthYear", "author_BirthYear")
		df = df.withColumnRenamed("authorDeathYear", "author_DeathYear")
		df = df.withColumnRenamed("authorID", "author_VIAF_URI")
		df = df.withColumnRenamed("authorISNI", "author_ISNI_ID")
		df = df.withColumnRenamed("authorWorldcat", "author_Worldcat_URI")
		df = df.withColumnRenamed("authorSex", "author_Sex")
		df = df.withColumnRenamed("authorWikidata", "author_Wikidata_URI")
		df = df.withColumnRenamed("authorLanguage", "author_Language")
		df = df.drop('author_VIAF_URI')
		df = df.drop('author_Worldcat_URI')
		df = df.drop('author_Wikidata_URI')
		df = df.drop('author_ISNI_ID')
	if target_kg == "viaf_authors_context":
		df = df.withColumnRenamed("authorName1_original", "author_Name_1")
		df = df.withColumnRenamed("authorName2_original", "author_Name_2")
		df = df.withColumnRenamed("authorCountryOrigin", "author_CountryOrigin")
		df = df.withColumnRenamed("authorBirthYear", "author_BirthYear")
		df = df.withColumnRenamed("authorDeathYear", "author_DeathYear")
		df = df.withColumnRenamed("authorID", "author_VIAF_URI")
		df = df.withColumnRenamed("authorISNI", "author_ISNI_ID")
		df = df.withColumnRenamed("authorWorldcat", "author_Worldcat_URI")
		df = df.withColumnRenamed("authorSex", "author_Sex")
		df = df.withColumnRenamed("authorWikidata", "author_Wikidata_URI")
		df = df.withColumnRenamed("authorLanguage", "author_Language")
	if target_kg == "viaf_authors_id":
		df = df.withColumnRenamed("authorID", "author_VIAF_URI")
		df = df.withColumnRenamed("authorISNI", "author_ISNI_ID")
		df = df.withColumnRenamed("authorWorldcat", "author_Worldcat_URI")
		df = df.withColumnRenamed("authorWikidata", "author_Wikidata_URI")
	if target_kg == "viaf_expressions":
		df = df.withColumnRenamed("workName1_original", "work_Name_1")
		df = df.withColumnRenamed("workName2_original", "work_Name_2")
		df = df.withColumnRenamed("expressionName1_original", "expression_Name_1")
		df = df.withColumnRenamed("expressionName2_original", "expression_Name_2")
		df = df.withColumnRenamed("expressionName3_original", "expression_Name_3")
		df = df.withColumnRenamed("authorName1_original", "author_Name_1")
		df = df.withColumnRenamed("authorName2_original", "author_Name_2")
		df = df.withColumnRenamed("workName1", "work_Name_1")
		df = df.withColumnRenamed("workName2", "work_Name_2")
		df = df.withColumnRenamed("workName3", "work_Name_3")
		df = df.withColumnRenamed("expressionName1", "expression_Name_1")
		df = df.withColumnRenamed("expressionName2", "expression_Name_2")
		df = df.withColumnRenamed("expressionName3", "expression_Name_3")
		df = df.withColumnRenamed("authorName1", "author_Name_1")
		df = df.withColumnRenamed("authorName2", "author_Name_2")
		df = df.withColumnRenamed("authorCountryOrigin", "author_CountryOrigin")
		df = df.withColumnRenamed("authorBirthYear", "author_BirthYear")
		df = df.withColumnRenamed("authorDeathYear", "author_DeathYear")
		df = df.withColumnRenamed("authorID", "author_VIAF_URI")
		df = df.withColumnRenamed("authorISNI", "author_ISNI_ID")
		df = df.withColumnRenamed("authorWorldcat", "author_Worldcat_URI")
		df = df.withColumnRenamed("authorSex", "author_Sex")
		df = df.withColumnRenamed("authorWikidata", "author_Wikidata_URI")
		df = df.withColumnRenamed("work_ID", "work_VIAF_URI")
		df = df.withColumnRenamed("authorLanguage", "author_Language")
		df = df.withColumnRenamed("workLanguage", "work_Language")
		df = df.withColumnRenamed("expressionLanguage", "expression_Language")
		df = df.withColumnRenamed("workID", "work_VIAF_URI")
		df = df.withColumnRenamed("workWorldcat", "work_Worldcat_URI")
		df = df.withColumnRenamed("workWikidata", "work_Wikidata_URI")
		df = df.withColumnRenamed("expressionWikidata", "expression_Wikidata_URI")
		df = df.withColumnRenamed("expressionID", "expression_VIAF_URI")
		#df = df.drop('work_VIAF_URI')
		df = df.drop('work_Worldcat_URI')
		df = df.drop('work_Wikidata_URI')
		#df = df.drop("expression_VIAF_URI")
		df = df.drop('work_Name_2')
		df = df.drop('work_Name_1')
	if target_kg == "viaf_expressions_context":
		df = df.withColumnRenamed("workName1_original", "work_Name_1")
		df = df.withColumnRenamed("workName2_original", "work_Name_2")
		df = df.withColumnRenamed("workName1", "work_Name_1")
		df = df.withColumnRenamed("workName2", "work_Name_2")
		df = df.withColumnRenamed("expressionName1_original", "expression_Name_1")
		df = df.withColumnRenamed("expressionName2_original", "expression_Name_2")
		df = df.withColumnRenamed("expressionName3_original", "expression_Name_3")
		df = df.withColumnRenamed("authorName1_original", "author_Name_1")
		df = df.withColumnRenamed("authorName2_original", "author_Name_2")
		df = df.withColumnRenamed("authorCountryOrigin", "author_CountryOrigin")
		df = df.withColumnRenamed("authorBirthYear", "author_BirthYear")
		df = df.withColumnRenamed("authorDeathYear", "author_DeathYear")
		df = df.withColumnRenamed("authorID", "author_VIAF_URI")
		df = df.withColumnRenamed("authorISNI", "author_ISNI_ID")
		df = df.withColumnRenamed("authorWorldcat", "author_Worldcat_URI")
		df = df.withColumnRenamed("authorSex", "author_Sex")
		df = df.withColumnRenamed("authorWikidata", "author_Wikidata_URI")
		df = df.withColumnRenamed("work_ID", "work_VIAF_URI")
		df = df.withColumnRenamed("expressionID", "expression_VIAF_URI")
		df = df.withColumnRenamed("authorLanguage", "author_Language")
		df = df.withColumnRenamed("workLanguage", "work_Language")
		df = df.withColumnRenamed("expressionLanguage", "expression_Language")
		df = df.withColumnRenamed("workID", "work_VIAF_URI")
	if target_kg == "viaf_expressions_id":
		df = df.withColumnRenamed("workID", "work_VIAF_URI")
		df = df.withColumnRenamed("expressionID", "expression_VIAF_URI")
		df = df.withColumnRenamed("workWorldcat", "work_Worldcat_URI")
		df = df.withColumnRenamed("expressionWikidata", "expression_Wikidata_URI")
		df = df.withColumnRenamed("workWikidata", "work_Wikidata_URI")
	if target_kg == "wikidata_works_context":
		df = df.withColumnRenamed("work_Name_1_original", "work_Name_1")
		df = df.withColumnRenamed("work_Name_2_original", "work_Name_2")
		df = df.withColumnRenamed("author_Name_1_original", "author_Name_1")
		df = df.withColumnRenamed("author_Name_2_original", "author_Name_2")
	if target_kg == "wikidata_authors_context":
		df = df.withColumnRenamed("author_Name_1_original", "author_Name_1")
		df = df.withColumnRenamed("author_Name_2_original", "author_Name_2")
	if target_kg == "getty_people_context":
		df = df.withColumnRenamed("author_Name_1_original", "author_Name_1")
		df = df.withColumnRenamed("author_Name_2_original", "author_Name_2")
		df = df.withColumnRenamed("author_BirthPlace_original", "author_BirthPlace")
		df = df.withColumnRenamed("author_DeathPlace_original", "author_DeathPlace")
	return df


def duplicate_original_cols(target_kg, df):
	"""
	Create a duplicate of columns so that we can still access the original value after normalizing.
	"""
	columns = get_headings("duplicate", target_kg)
	for column in columns:
		df = df.withColumn(column + "_original", df[column])
	return df


def df_whitespace(input_string: str):
	"""
	Remove duplicated whitespace, trailing whitespace from a string
	"""
	if input_string is not None:
		return " ".join(input_string.split())
	else:
		return input_string


def df_punctuation(input_string: str):
	"""
	Replaces punctuation in a string with a space
	"""
	if input_string is not None:
		translator = str.maketrans('', '', string.punctuation)
		return input_string.translate(translator)
	else:
		return input_string


def df_name_concat(full_name: str):
	"""
	concatenates first name and last name with a space
	if those are both null then it inverts the full name order
	"""
	if full_name is not None and full_name.strip() != ",":
		if "," in full_name:
			name_parts = full_name.split(",")
		elif ";" in full_name:
			name_parts = full_name.split(";")
		else:
			# assume name was already in correct order
			return full_name

		# remove spaces and empty strings from list
		name_parts = [x for x in name_parts if x]

		try:
			reordered_name = name_parts[-1]
		except:
			print("ERROR reordering name: ", full_name)
			reordered_name = ""
		if len(name_parts) >= 2:
			for word in name_parts[:-1]:
				reordered_name = reordered_name + " " + word
		return reordered_name
	return None


def author_normalize(target_kg, df_input, dfNameConcat):
	"""
	Invert authorTitle so that it's first name last name order instead of lastName, firstName

	"""
	if target_kg in ["viaf_works", "viaf_expressions", "viaf_authors"]:
		df_input = df_input.withColumn("authorName1", dfNameConcat("authorName1"))
		df_input = df_input.withColumn("authorName2", dfNameConcat("authorName2"))
	if target_kg in ["getty_people"]:
		df_input = df_input.withColumn("author_Name_1", dfNameConcat("author_Name_1"))
		df_input = df_input.withColumn("author_Name_2", dfNameConcat("author_Name_2"))
	return df_input


def fix_zero_length_strings(df: DataFrame):
	"""Convert any zero length strings or strings that contain only whitespace to a true null
	Args:
		df (DataFrame): Input Spark dataframe
	Returns:
		DataFrame: Spark Dataframe with clean strings

	https://github.com/moj-analytical-services/splink_data_normalisation/blob/master/splink_data_normalisation/fix_string.py
	"""
	string_cols = [item[0] for item in df.dtypes if item[1].startswith('string')]

	stmt = """
	case
	when trim({c}) = '' then null
	else trim({c})
	end
	"""
	for c in string_cols:
		df = df.withColumn(c, expr(stmt.format(c=c)))

	return df


def df_gender(gender: str):
	"""
	translates variations in gender values to F or M
	6581072 and 6581097 represent the wikidata entities for male and female
	"""
	if gender is not None:
		if "f" in gender.lower() or gender == "1" or "6581072" in gender:
			gender_check = "F"
			return "F"
		elif "m" in gender.lower() or gender == "0" or "6581097" in gender:
			gender_check = "M"
			return "M"
	else:
		return None


def dates_normalize(target_kg, df_input):
	"""
	Date columns converted to only contain years

	"""
	columns = get_headings("date_normalize", target_kg)
	for header in columns:
		df_input = df_input.withColumn(header, regexp_extract(header, r'([1|2][0-9]{3})', 1))
	return df_input


def gender_normalize(target_kg, df_input, dfGender):
	columns = get_headings("gender_normalize", target_kg)
	for header in columns:
		df_input = df_input.withColumn(header, dfGender(header))
	return df_input


def common_data_normalize(target_kg, df_input, dfWhitespace, dfPunctuation):
	"""
	cols_inplace will be overwritten with the normalized values
	this is done because they have already been duplicated at a previous step
	cols_newcol are duplicated and then normalized so we still have access to the original value
	"""
	columns = get_headings("string_normalize", target_kg)
	for column in columns:
		# lowercase the string column
		df_input = df_input.withColumn(column, lower(col(column)))

		# replace punctuation with whitespace
		df_input = df_input.withColumn(column, dfPunctuation(column))

		# remove extra whitespaces
		df_input = df_input.withColumn(column, dfWhitespace(column))

	# convert all zero length strings to nulls
	df_input = fix_zero_length_strings(df_input)

	return df_input


def titles_normalize(target_kg, df_input):
	if target_kg in ["wikidata_works"]:
		df_input = df_input.withColumn('work_Name_3', lit(None).cast(types.NullType()))
		# splits workTitle1 on first colon (if present) and adds to work_Name3
		df_input = df_input.withColumn("work_Name_3", f.split("work_Name_1", "(?<=^[^:]*)\\:")[1]).\
			withColumn("work_Name_1", f.split("work_Name_1", "(?<=^[^:]*)\\:")[0])

	if target_kg in ["viaf_works", "viaf_expressions"]:
		df_input = df_input.withColumn('workName3', lit(None).cast(types.NullType()))
		# splits workTitle1 on first colon (if present) and adds to workTitle3
		df_input = df_input.withColumn("workName3", f.split("workName1", "(?<=^[^:]*)\\:")[1]).\
			withColumn("workName1", f.split("workName1", "(?<=^[^:]*)\\:")[0])

	if target_kg in ["viaf_expressions"]:
		df_input = df_input.withColumn('expressionName3', lit(None).cast(types.NullType()))
		# splits workTitle1 on first colon (if present) and adds to workTitle3
		df_input = df_input.withColumn("expressionName3", f.split("expressionName1", "(?<=^[^:]*)\\:")[1]).\
			withColumn("expressionName1", f.split("expressionName1", "(?<=^[^:]*)\\:")[0])
	return df_input


def name_list_split(target_kg, df_input):
	# if target_kg in ["getty_people"]:
	# 	df_input = df_input.withColumn("author_Name_2", f.split("author_NameAll", "(.*) , ")[0])

	if target_kg in ["wikidata_authors", "getty_people"]:
		df_input = df_input.withColumn("author_Name_2", f.split("author_Name_2", "(.*) , ")[0])
		df_input = df_input.withColumn("author_Name_2", f.split("author_Name_3", "(.*) , ")[0])
	return df_input


def longest_word(s):
	"""
	return the longest word in string s
	given a tie, it will choose the first lexographically
	"""
	if s:
		word_list = s.split(" ")
		word_list.sort()
		word_list.sort(key=len, reverse=True)
		stop_words = stopwords()
		if len(word_list) == 1:
			return word_list[0]
		elif word_list[0] not in stop_words:
			return word_list[0]
	return None


def second_longest_word(s, tblock1):
	"""
	return the second longest word in string s
	given a tie for first longest word, it will choose the second lexographically
	"""
	if s:
		word_list = s.split(" ")
		word_list.sort()
		word_list.sort(key=len, reverse=True)
		stop_words = stopwords()
		try:
			word = word_list[1]
			if len(word) > 2 and word != tblock1 and word not in stop_words:
				return word
		except:
			if len(word_list[0]) > 2 and word_list[0] != tblock1 and word not in stop_words:
				return word_list[0]
	return None


def last_word(s):
	"""
	returns the last name in a full name. The name must be at least 2 characters long to avoid initials
	"""

	def check_last_word(s_list):
		if len(s_list) > 0:
			if len(s_list[-1]) > 1:
				return s_list[-1]
			else:
				check_last_word(s_list[:-1])
		return None

	if s:
		word_list = s.split(" ")
		return check_last_word(word_list)
	else:
		return None


def first_word(s, ablock1):
	"""
	returns the first name in a full name. The name must be at least 2 characters long to avoid initials
	"""

	def check_first_word(s_list):
		if len(s_list) > 0:
			if len(s_list[0]) > 1 and s_list[0] != ablock1:
				return s_list[0]
			else:
				check_first_word(s_list[1:])
		return None

	if s:
		word_list = s.split(" ")
		return check_first_word(word_list)
	else:
		return None


def blocking(target_kg, df, longestWord, secLongestWord, firstWord, lastWord):
	"""
	add blocking key columns to the dataframe
	"""

	# title blocking keys
	if target_kg in ["viaf_works"]:
		df = df.withColumn('tblock1', longestWord('workName1'))
		df = df.withColumn('tblock2', secLongestWord('workName1', 'tblock1'))
	if target_kg in ["wikidata_works"]:
		df = df.withColumn('tblock1', longestWord('work_Name_1'))
		df = df.withColumn('tblock2', secLongestWord('work_Name_1', 'tblock1'))
	if target_kg in ["viaf_expressions"]:
		df = df.withColumn('tblock1', longestWord('expressionName1'))
		df = df.withColumn('tblock2', secLongestWord('expressionName1', 'tblock1'))

	# author blocking keys
	if target_kg in ["viaf_works", "viaf_expressions"]:
		df = df.withColumn('ablock1', lastWord('authorName1'))
		df = df.withColumn('ablock2', firstWord('authorName1', 'ablock1'))
	if target_kg in ["wikidata_works", "wikidata_authors", "getty_people"]:
		df = df.withColumn('ablock1', lastWord('author_Name_1'))
		df = df.withColumn('ablock2', firstWord('author_Name_1', 'ablock1'))

	# # if any keys are null, fill with another key
	# if target_kg in ["viaf-works", "viaf-expressions", "wikidata-works"]:
	# 	df = df.withColumn("ablock1", coalesce(df.ablock1, df.tblock1))
	# 	df = df.withColumn("ablock2", coalesce(df.ablock2, df.tblock2))
	# 	df = df.withColumn("tblock1", coalesce(df.tblock1, df.ablock1))
	# 	df = df.withColumn("tblock2", coalesce(df.tblock2, df.ablock2))

	# TODO delete tblock2 or ablock2 values if they are the same as tblock1 or ablock2


	# convert all zero length strings to nulls
	df = fix_zero_length_strings(df)
	return df


def separate_ids(target_kg, df):
	# moves the columns containing identifiers to a separate dataframe for faster lookups
	if target_kg == "viaf_works":
		df_id = df.select(
			"unique_id",
			"workID",
			"workWorldcat",
			"workWikidata")
		#df = df.drop(*["workID", "workWorldcat", "workWikidata"])  # keep the author identifiers in the comparison dataframe for record linkage
	elif target_kg == "viaf_expressions":
		df_id = df.select(
			"unique_id",
			"expressionID",
			"expressionWikidata",
			"workID")
		#df = df.drop(*["expressionID", "expressionWikidata"])
	elif target_kg == "viaf_authors":
		df_id = df.select(
			"unique_id",
			"authorID",
			"authorISNI",
			"authorWikidata",
			"authorWorldcat")
	elif target_kg == "wikidata_works":
		df_id = df.select(
			"unique_id",
			"work_Wikidata_URI",
			"work_ISBN10_ID",
			"work_ISBN13_ID",
			"work_OCLC_ID",
			"work_OCLCControl_ID",
			"work_VIAF_URI",
			"author_ISNI_ID",
			"author_VIAF_URI",
			"author_Wikidata_URI")
	elif target_kg == "wikidata_authors":
		df_id = df.select(
			"unique_id",
			"author_ISNI_ID",
			"author_VIAF_URI",
			"author_Wikidata_URI")
	elif target_kg == "getty_people":
		df_id = df.select(
			"unique_id",
			"author_Getty_URI")
	return df, df_id


def separate_originals(target_kg, df):
	# move the columns containing unnormalized strings to separate dataframe for faster lookups
	# drop columns from main df if they won't be used for linking by spark. This will result in faster dataframe loading during linking
	if target_kg == "viaf_works":
		df_og = df.select(
			"unique_id",
			"workName1_original",
			"workName2_original",
			"authorName1_original",
			"authorName2_original",
			"authorLanguage",
			"authorCountryOrigin",
			"workLanguage",
			"authorBirthYear",
			"authorDeathYear",
			"authorID",
			"authorISNI",
			"authorWorldcat",
			"authorSex",
			"authorWikidata")
		df = df.drop(*[
			"workName1_original",
			"workName2_original",
			"authorName1_original",
			"authorName2_original",
			"authorLanguage",
			"authorCountryOrigin",
			"workLanguage"])
	elif target_kg == "viaf_authors":
		df_og = df.select(
			"unique_id",
			"authorName1_original",
			"authorName2_original",
			"authorLanguage",
			"authorCountryOrigin",
			"authorBirthYear",
			"authorDeathYear",
			"authorID",
			"authorISNI",
			"authorWorldcat",
			"authorSex",
			"authorWikidata")
		df = df.drop(*[
			"authorName1_original",
			"authorName2_original",
			"authorLanguage",
			"authorCountryOrigin"])
	elif target_kg == "viaf_expressions":
		df_og = df.select(
			"unique_id",
			"workID",
			"workName1",
			"workName2",
			"expressionName1_original",
			"expressionName2_original",
			"authorBirthYear",
			"authorCountryOrigin",
			"authorDeathYear",
			"authorLanguage",
			"authorName1_original",
			"authorName2_original",
			"authorSex",
			"expressionLanguage",
			"authorID",
			"authorISNI",
			"authorWorldcat",
			"authorWikidata")
		df = df.drop(*[
			"expressionLanguage",
			"expressionName1_original",
			"expressionName2_original",
			"authorName1_original",
			"authorName2_original",
			"workName1",
			"workName2",
			"workName3",
			"authorCountryOrigin",
			"authorLanguage"])
	elif target_kg == "wikidata_works":
		df_og = df.select(
			"unique_id",
			"work_Wikidata_URI",
			"work_Name_1_original",
			"work_Name_2_original",
			"work_CountryOrigin_URI",
			"work_CountryOrigin",
			"work_Edition",
			"work_Format_URI",
			"work_Format",
			"work_ISBN10_ID",
			"work_ISBN13_ID",
			"work_Language_URI",
			"work_Language",
			"work_OCLC_ID",
			"work_OCLCControl_ID",
			"work_PublicationDate",
			"work_Publisher_URI",
			"work_Publisher",
			"work_VIAF_URI",
			"author_ISNI_ID",
			"author_VIAF_URI",
			"author_Wikidata_URI",
			"author_BirthYear",
			"author_DeathYear",
			"author_Name_1_original",
			"author_Name_2_original",
			"author_Sex",
			"work_WikidataType_URI",
			"work_WikidataType",
			"work_Genre_URI",
			"work_Genre",
			"work_Description",
			"author_Description")
		df = df.drop(*[
			"work_Name_1_original",
			"work_Name_2_original",
			"author_Name_1_original",
			"author_Name_2_original"
			"work_CountryOrigin_URI",
			"work_CountryOrigin",
			"work_Edition",
			"work_Format_URI",
			"work_Format",
			"work_Language_URI",
			"work_Language",
			"work_Publisher_URI",
			"work_Publisher",
			"work_WikidataType_URI",
			"work_WikidataType",
			"work_Genre_URI",
			"work_Genre",
			"work_Description",
			"author_Description"])
	elif target_kg == "wikidata_authors":
		df_og = df.select(
			"unique_id",
			"author_ISNI_ID",
			"author_VIAF_URI",
			"author_Wikidata_URI",
			"author_BirthYear",
			"author_DeathYear",
			"author_Name_1_original",
			"author_Name_2_original",
			"author_Sex",
			"author_Description")
		df = df.drop(*[
			"author_Name_1_original",
			"author_Name_2_original",
			"author_Description"])
	elif target_kg == "getty_people":
		df_og = df.select(
			"unique_id",
			"author_Getty_URI",
			"author_BirthYear",
			"author_DeathYear",
			"author_Name_1_original",
			"author_Name_2_original",
			"author_Sex",
			"author_Description",
			"author_DescriptionAll",
			"author_BirthPlace_original",
			"author_DeathPlace_original",
			"author_NationalityAll",
			"author_Nationality",
			"author_LanguageAll",
			"author_Language",
			"author_Role")
		df_og = df_og.withColumn("author_Description", concat(col("author_Description"), lit(" , "), col("author_DescriptionAll")))
		df_og = df_og.withColumn("author_Nationality", concat(col("author_Nationality"), lit(" , "), col("author_NationalityAll")))
		df_og = df_og.withColumn("author_Language", concat(col("author_Language"), lit(" , "), col("author_LanguageAll")))
		df_og = df_og.drop(*["author_DescriptionAll", "author_NationalityAll", "author_LanguageAll"])
		df = df.drop(*[
			"author_Name_1_original",
			"author_Name_2_original",
			"author_BirthPlace_original",
			"author_DeathPlace_original",
			"author_DescriptionAll",
			"author_Description",
			"author_NationalityAll",
			"author_Nationality",
			"author_LanguageAll",
			"author_Language",
			"author_Role",
			"subject_id"])
	return df, df_og
