# VIAF Bibliographic Data Extraction

This tool is designed to parse the [VIAF data dump](https://viaf.org/viaf/data/), in particular the clusters.xml.gz file described as "This file contains one 'native' XML record per line for each VIAF cluster" (updated on a rolling basis). 

There are two datasets that are created from this dump.  One based off primary work titles (work_based) and another based off expressions of primary titles (expression based). These are exported as tsv's and saved as tables in the database. 

The program automatically checks if the latest VIAF cluster is downloaded and used.  Data is processed on a line by line basis where each line represents a VIAF URI.  Finally, the .tsv results are uploaded to an S3 bucket.

### VIAF Types
VIAF has the following nametypes which are the parents of all VIAF entities:

- Personal : Generally an Author
- Geographic : (Not Used)
- Corporate : (Not Used)
- UniformTitleWork : Generally a written work
- UniformTitleExpression : Generally a written work expression

Each of the following nametypes generally follow this structure:

- Personal : Author information with a list of works with and without a VIAF URI
- UniformTitleWork : Work information with a link to the author URI and a list of Expressions always with URI's
- UniformTitleExpression : Expression information with a link to the author URI and always a link to the main work URI

Each of the following nametypes are parsed using the following methods declared in __parseViaf.py__:

- Personal : ParseViaf.process_author_root()
- UniformTitleWork : ParseViaf.process_work_root(expression=False)
- UniformTitleExpression : ParseViaf.process_work_root(expression=True)

### Data Structure

Each row in the final results starts as a work.py Work() object/class.  All column headers are represented under the __properties__ dictionary for the Work class.  If you want to add a new column you have to add it to this dictionary and also add it to __queries/mergeQuery.txt__ and __queries/mergeQueryExpressions.txt__ in alphabetical order.

### Information is gathered in the following order:

Some author information is stored in UniformTitleWork and UniformTitleExpression but more detailed information is stored in Personal worktypes, hence the data from UniformTitleWork and UniformTitleExpression has to be joined with Personal. 

The entire process can be described with the following major steps.

1. Gather author information from __Personal__ nametypes, also known as __author_base__
    1. Gather all primary works listed under the __author_base__ and combine with author data (creating new rows for each work)
        1. There is a very large amount of works added from author base that do not have a VIAF work id
2. Gather work information from __UniformTitleWork__ nametypes, also known as __work_base__
    1. Get all expressions of the work and save to __work_expression_base__ (not currently used as expression base 
       captures almost everything except incorrectly entered entities.
3. Left Join the author_base and work_base tables based on the work VIAF ID and save table as __works__
4. Gather expression information from __UniformTitleExpression__ nametypes, also known as __expression_base__
    2. Find the parent / primary work VIAF ID if it exists
    3. Inner Join the expression_base and author_base tables based on the parent work VIAF ID and save table as 
    __expressions__
5. Do post cleaning and reverse Wikidata lookup via Pandas python library and save 
   <expressions/works>_cleaned tables and respective TSV's
   1. Both work VIAF ids, Author VIAF id and Author ISNI are reverse looked up via Wikidata's query service in bulk. 
      (Process described below)
    
Except for a few outliers almost all expressions should map to a primary work title via a work VIAF ID. Every expression base will have an expression VIAF ID but not every work will have a VIAF ID as works can be listed under author_base but not be referenced to a VIAF id.

### Database Tables

All information is stored in data.db and cleaned data is saved as a tsv. The following tables exist in data.db and can be used for debugging where information came from.

author_base : created using Personal nametypes. A row per work, but created by going through person records and getting all their works.
author_all_base : created using Personal nametypes. Same as author_base except it has all people even if they don't have works. And it groups works for a person into one cell.
expression_base : created using UniformTitleExpression 
work_base : creating using the main work title from UniformTitleWork
work_expression_base : created using expressions from UniformTitleWork (Not needed but used in tests)

expressions : the result of the join between author_base and expression_base
works : the result of the join between author_base and work_base

expressions_cleaned : expressions table gone through clean process
works_cleaned : works table gone through clean process

### Data Gathered
Each row in the output data can be represented as a __work.py__ Work() object with the following properties:

- authorBirthYear - Year only
- authorCountryOrigin
- authorDeathYear - Year only
- authorISNI : XXXX XXXX XXXX XXXX
- authorLanguage
- authorName1
- authorName2
- authorSex : male, female, null
- authorVIAFID : https://viaf.org/viaf/XXXXXXXXXXXXXXXXXXXXXXX
- authorWKP : wikidata uri entry | https://wikidata.org/entity/Q########
- authorWorldcat : worldcat uri entry | https://www.worldcat.org/identities/lccn-n##########
- expressionName1
- expressionName2
- expressionVIAFID : will be null for all work_base data, each expression will have a expressionVIAFID
- expressionWorldcat : https://www.worldcat.org/oclc/#######
- workLanguage
- workLibraries : works and expressions are linked to libraries which the information is gathered, this shows up as a 
flag on VIAF entities URL/URI.
- workName1
- workName2
- workName3
- workVIAFID : https://viaf.org/viaf/XXXXXXXXXXXXXXXXXXXXXXX
- workWikidataID : https://www.wikidata.org/wiki/Q#######
- workWorldcat : https://www.worldcat.org/oclc/#######

# Sources
For the purposes of linking new data to VIAF, we don’t need to use all of the multilingual author names that are available in VIAF. To reduce the number of names we prioritize names that come from a primarily english or french external library since that is the expected primary use case for LINCS. When there are more than two author names we only gather names and titles from the following library sources (Library names are listed [here]):

(https://viaf.org/).

X400's (Alternate Labels: __altNamePath__, Multi-Language) : "VIAF", "SUDOC", "CAOONL", "LC", "XR"
mainHeadingsEL (Generally English Headings) -> work : "VIAF", "SUDOC", "CAOONL", "LC", "XR, "DNB" "WKP"
mainHeadingsEL -> author : "VIAF", "SUDOC", "CAOONL", "LC", "XR", "WKP"

## Personal
Extracted using the process_author_root method.  A root being the tree root/parent of the xml file. Most information about the author is extracted from the personal base. Any of these property paths can be found in nameSpaces.py  

- authorBirthYear - bDayPath
- authorCountryOrigin - authorNationalityPath
- authorDeathYear - dDayPath
- authorISNI - sourcesPath: matches ISNI|XXXXXXXXXXXXXXXX form
- authorLanguage - authorLangPath
- authorName1 - mainHeadingEL->subfieldPath['a/b']
- authorName2 - altNamePath->subfieldPath : x400's with code a/b that are from selected sources (workAltSource)
- authorSex - genderPath
- authorVIAFID - viafIDPath
- authorWKP - sourcesPath: matches WKP|XXXXXXXXXXXXXXX form
- authorWorldcat - sourcesPath: matches LC|XXXXXXXX form
- expressionName1 - Not used
- expressionName2 - Not used
- expressionVIAFID - Not used
- expressionWorldcat - Not used
- workLanguage - Not used
- workLibraries - englishPropPath->englishSourcePath / workAltSource : Only libraries from allowed list are used
- workName1 - workPath / ttlPath
- workName2 - Not used
- workName3 - Not used
- workVIAFID - viafIDPath
- workWikidataID - Not used
- workWorldcat - Not used

## UniformTitleWork / UniformTitleExpression

Both UniformTitleWork and UniformTitleExpression nametypes use the same process_work_root methods with the argument expression handling the differences on how they are parsed.

- authorBirthYear - mainHeadingEL->subfieldPath['d']
- authorCountryOrigin - Not used
- authorDeathYear - mainHeadingEL->subfieldPath->['d']
- authorISNI - Not used
- authorLanguage - Not used
- authorName1 - one random pop : mainHeadingEL->subfieldPath->['a/b']
- authorName2 - all remaining values
- authorSex - Not used
- authorVIAFID - authorPath->['id'] / mainHeadingEL->subfieldPath->['0']
- authorWKP - Not used
- authorWorldcat - sourcesPath: matches LC|XXXXXXXX form
- expressionName1 - 
- expressionName2 - 
- expressionVIAFID - viafIDPath
- expressionWorldcat - matches XR|VIAFEXP####### form
- workLanguage - mainHeadingEL->subfieldPath->['l']
- workLibraries - englishPropPath->englishSourcePath / workAltSource : Only libraries from allowed list are used
- workName1 - Not used
- workName2 - mainHeadingEL->subfieldPath['t']
- workName3 - x400s from select libraries : altNamePath->workAltSource
- workVIAFID - viafIDPath
- workVIAFID (expression) - workPath->['id']
- workWikidataID - Not used
- workWorldcat - matches XR|VIAFWORK####### form

# Wikidata Reverse Lookup

Many VIAF works do not have the sameas triple property to wikidata entities which allow data to be mapped to a Wikidata URI. Any work that does not have a link to a Wikidata entry is reverse looked up via querying wikidata for works that have property VIAFID or ISNI matching the VIAF work with missing wikidata entry. Reverse lookup queries are done in bulk with their mapped results stored in a dictionary, then in the final cleaning steps pandas will search all VIAF id's and ISNI's and will map back Wikidata IDS that are associated.

# Joined Data

__queries/mergeQuery.txt__ and __mergeQueryExpressions__ are what joins together all author_base works and all works/expression base works.  __queries/mergeQuery.txt__ does an inner join between the author and work base and __mergeQueryExpressions__ does an inner join between author and expression base as any non-hit data will already be stored in works_cleaned.  Any author based works without a workVIAFID will still be maintained, and entries with matching workVIAFID's will be combined.  

# Data Cleaning

- Years with months and days included are stripped to just year
- Gender 'a', 'b', 'u' are converted to 'female', 'male', 'null' via merge query
- Worknames are reduced to two columns
- Authornames are reduced to two values

# Checkpoints

- Since the VIAF XML file is parsed line by line locally a tally of the current line is tracked in 
  ./checkpoint/<FILENAME>.xml.txt with the first line in this file being the total number of lines and the second line 
  being the last saved checkpoint.
- Data cleaning is done via pandas via reading data in chunks from the sql database and written to 
  <works/expressions>_cleaned in the sql database.  If data cleaning is interrupted it continues by counting how many 
  chunks exist in <works/expressions_cleaned> and skips that many chunks ahead to resume.
  
# Tests

- Assert all expression work id's exist in works
- Assert expression/works has same count as cleaned counterparts
- Assert all expression_base and work_expression_base have a expression id
- Assert that every entity in work_base has a work viaf id
- All expressions work id's in workbase
- Are all work_expression_base in expression_base?
- Assert that work_base workVIAF IDs is a subset of works
- All author_base workVIAF IDs in works
- Assert that all author base works are in works, and those that are not are expressions
- All works from expressions in works
- Assert all author_base works not in work_base do not have a viaf ID
- Assert all author_base have author VIAF ID

# Blacklist

- Blacklisted item can be added to the __data/blacklist.json__ file
- Any work with author "bible", "bible.", "bibel", "bibel.","biblia", "biblia.", "united states.", "united states" are 
  automatically removed
- Any work title Works. or [without title] are automatically removed

### Arguments

Arguments are mainly used for debugging

- -c --clean : will skip the cleaning process
- -e --expressions : will skip creating the expression_based entities
- -w --works : will skip creating the work_based entities
- -s --small : uses test.xml file
- -t --test : will skip running tests on final data
- -u --upload : will skip uploading results to s3
