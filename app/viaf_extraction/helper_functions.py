from viaf_extraction.nameSpaces import *
from SPARQLWrapper import SPARQLWrapper, JSON
import sys
import pandas as pd
from difflib import SequenceMatcher
import os.path
import gzip
import shutil
import urllib.request
import re
import logging
import time
import json

logging.basicConfig(level=logging.DEBUG, filename="logfile", filemode="a+",
                    format="%(asctime)-15s %(levelname)-8s %(message)s")


class ISNI(str):
    """
    Converts a 16 digit isni into XXXX XXXX XXXX XXXX form
    """
    def __init__(self, isni):
        if len(isni) != 16:
            self.parsed = ''
        else:
            parsed = ''
            for block in [isni[i:i+4] for i in range(0, len(isni), 4)]:
                parsed += block + " "
            self.parsed = parsed[:-1]

    def __repr__(self):
        return self.parsed


class MySet(set):
    """
    Subclass of set so I can override the __repr__ for easier viewing in pandas / debugging
    """
    def __repr__(self):
        """
        :return: If set is empty returns a empty string, if otherwise returns multiple values with comma seperation
        """
        if len(self) == 0:
            return ''
        output = ''
        for elm in self:
            output += str(elm) + " ~ "
        return output[:-2]

    def __str__(self):
        """
        :return: Overrides string method into form: "'item1' 'item2' 'etc...'"
        """
        if len(self) == 0:
            return ''
        output = ''
        for elm in self:
            output += "'" + str(elm) + "'" + " "
        return output[:-1]


def check_table(cursor, table):
    """
    Checks if a table exists in a cursor to a database
    """
    query = "SELECT NAME FROM sqlite_master WHERE type = 'table' AND name NOT LIKE 'sqlite_%';"
    cursor.execute(query)
    if (table,) in cursor.fetchall():
        return True
    else:
        return False


def clean_exp_titles(expression_name_1, sort=True):
    """
    Method used by Pandas for splitting up multiple values for expression titles if they exist
    :param expression_name_1: primary expression names
    :return: pd series tuple (expressionName1, expressionName2)
    """
    if expression_name_1 is None:
        return pd.Series([None, None])
    if "~" in expression_name_1:
        titles = expression_name_1.split("~")
        if sort is True:
            titles = sorted(titles, key=len)
        expression_name_1 = titles.pop(0)
        expression_name_2 = titles.pop(0)
        return pd.Series([expression_name_1, expression_name_2])
    else:
        return pd.Series([expression_name_1, None])


def clean_names(row, sort=False):
    """
    Used by pandas to reduce names
    :param row: row sent from pandas
    :param sort: sort by length
    :return: pd series tuple (name1, name2)
    """
    names1 = ['']
    names2 = ['']
    if row['authorName1'] is not None:
        if sort is True:
            names1 = sorted(row['authorName1'].split("~"), key=len)
        else:
            names1 = row['authorName1'].split("~")
    if row['authorName2'] is not None:
        if sort is True:
            names2 = sorted(row['authorName2'].split("~"), key=len)
        else:
            names2 = row['authorName2'].split("~")
    return pd.Series([remove_trail_lead(names1[0]), remove_trail_lead(names2[0])])


def clean_titles(row, sort=True):
    """
    Used by pandas to reduce title names
    :param row: a tuple containing (workname1, workname2, workname3)
    :param sort: if sort is true return the shortest length results when more than 2
    :return: pd series tuple (workname1, workname2)
    """
    workName1 = row['workName1']
    workName2 = row['workName2']
    workName3 = row['workName3']

    # If all are none return none
    if pd.isnull(workName1) and pd.isnull(workName2) and pd.isnull(workName3):
        return pd.Series([None, None])

    if pd.isnull(workName1) and pd.isnull(workName2):
        return pd.Series([workName3, None])

    if pd.isnull(workName1):
        workName1 = workName2
        workName2 = workName3
        workName3 = None
    if pd.isnull(workName2):
        workName2 = workName3
        workName3 = None
    if workName1 == workName2:
        workName2 = workName3
        workName3 = None
    if workName1 == workName3:
        workName3 = None
    if workName2 == workName3:
        workName3 = None

    # if workName1 is not null but the others are
    if not pd.isnull(workName1) and pd.isnull(workName2) and pd.isnull(workName3):
        if "~" in workName1:
            titles = workName1.split("~")
            if sort is True:
                titles = sorted(titles, key=len)
            workName1 = titles.pop(0)
            workName2 = titles.pop(0)
            return pd.Series([workName1, workName2])
        else:
            return pd.Series([workName1, None])

    # If more than two titles exist we cannot distribute the split values
    if "~" in workName1:
        titles = workName1.split("~")
        if sort is True:
            titles = sorted(titles, key=len)
        workName1 = titles.pop(0)
    if "~" in workName2:
        titles = workName2.split("~")
        if sort is True:
            titles = sorted(titles, key=len)
        workName2 = titles.pop(0)

    return pd.Series([workName1, workName2])


def get_results(query, endpoint_url="https://query.wikidata.org/sparql", timeout=500, max_attempts=15):
    """
    Performs Sparql query using self.endpoint_url
    :param query: wikidata query
    :param endpoint_url: host to query
    :param timeout: timeout for wikidata query, defaults to ten seconds
    :param max_attempts: max attempts to try again
    :return:
    """
    try:
        # user_agent = "WDQS-example Python/%s.%s" % (sys.version_info[0], sys.version_info[1])
        # for more info on user agent; see https://w.wiki/CX6
        user_agent = "LINCS-https://lincsproject.ca//%s.%s" % (sys.version_info[0], sys.version_info[1])
        sparql = SPARQLWrapper(endpoint_url, agent=user_agent)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        sparql.setTimeout(timeout)
        results = sparql.query().convert()
        return results
    except Exception as e:
        logging.info(e)
        logging.info(query)
        attempts = 1
        while True:
            attempts += 1
            time.sleep(3)
            try:
                results = sparql.query().convert()
                return results
            except Exception as e2:
                logging.info(e2)
            if attempts > max_attempts:
                logging.info("Max query attempts reached for query:\n{}".format(query))
                return None


def get_latest_xml():
    """
    Get's latest xml full URL
    :return: string of the full url to latest viaf clusters.xml.gz file
    """

    opener = urllib.request.FancyURLopener({})
    url = "https://viaf.org/viaf/data/"
    f = opener.open(url)
    content = f.read()
    data_file = "https://viaf.org/viaf/data/viaf-DATE-clusters.xml.gz"
    latest_date = re.findall("https:\/\/viaf\.org\/viaf\/data\/viaf-([0-9]*)\-clusters\.xml\.gz", str(content))[0]
    data_file = data_file.replace("DATE", latest_date)
    return data_file


def get_xml(wait=False):
    """
    Checks if latest cluster xml file is available and downloads latest if required.  Will remove old files
    :param wait: If true will wait until a new file exists and will download it
    :return: filename of downloaded VIAF clusters xml file
    """

    data_file = get_latest_xml()
    latest_file = data_file.split("/")[-1].replace(".gz", "")

    # If wait is true keep sleeping for 1 day until the clusters.xml file is updated
    if wait is True:
        old_data_file = data_file
        data_file = get_latest_xml()
        while data_file == old_data_file:
            # Wait for a day
            time.sleep(86400)
            data_file = get_latest_xml()
            latest_file = data_file.split("/")[-1].replace(".gz", "")

    for file in os.listdir():
        if ("clusters.xml" in file) and (file != latest_file):
            print_and_log("Removing old xml file {}".format(file))
            os.remove(file)
    if os.path.isfile(latest_file):
        print_and_log("Using {}".format(latest_file))
    else:
        full_file_path = os.path.join(os.getcwd(), 'downloads', latest_file + ".gz")
        print_and_log("Downloading XML {}".format(data_file))
        error_no = -1
        # If file exists resume it (it will auto detect if finished), else just download it
        while error_no != 0:
            if os.path.isfile(full_file_path):
                # wget -c will know if the file is complete or not and return 0, very handy
                error_no = os.system('wget -c {} -P {}'.format(data_file, (os.path.join(os.getcwd(), 'downloads'))))
            else:
                error_no = os.system('wget {} -P {}'.format(data_file, (os.path.join(os.getcwd(), 'downloads'))))

        print("Extracting XML data")
        with gzip.open(full_file_path, 'rb') as f_in:
            # Extact into root directory, remove archive
            with open(latest_file, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
            os.remove(full_file_path)

    # Update authorityMetadata.txt
    with open("authorityMetadata.txt", "w") as meta_fp:
        json.dump({"authorityVersion": latest_file,
                   "authoritySource": "https://viaf.org/viaf/data/"}, meta_fp)
    return latest_file


def parse_year(year):
    """
    Extracts year
    """
    if year is None:
        return None
    split_year = year.split("-")
    if len(year) == 4:
        return year
    if len(year) == 3:
        return year
    for i in split_year:
        if len(i) in (3, 4):
            return i
    return year


def print_and_log(message):
    """Prints message and logs to logfile"""
    print(message)
    logging.info(message)


def process_subfields(subfields: list):
    """
    Alot of information is stored in an xml's subfields, this extracts all that info
    :param subfields: a collection of subfield results
    :return: Dictionary of parsed information : authorVIAFid, author full name, work titles, author birth/death year
    """
    parsed_subfields = {}
    name1 = None
    name2 = None
    full_name = None
    d_birth = None
    d_death = None
    f_birth = None
    f_death = None
    for subfield in subfields:
        if subfield.get("code") == '0':
            authorID = subfield.text
            if "viaf" in authorID:
                authorID = viafRoot + re.sub('\D', '', authorID)
            else:
                authorID = None
            if authorID is not None:
                parsed_subfields['authorVIAFID'] = authorID
        if subfield.get("code") == 'a':
            name1 = subfield.text
        if subfield.get("code") == 'b':
            name2 = subfield.text
        if subfield.get("code") == 't':
            parsed_subfields['workName2'] = subfield.text
        if subfield.get("code") == 'l':
            parsed_subfields['auth_or_work_language'] = subfield.text
        if subfield.get("code") in 'df':
            birth_and_death = subfield.text.replace(".", '').replace("(", '').replace(")", '')
            birth_and_death = birth_and_death.split("-")
            if len(birth_and_death) == 1:
                if subfield.get("code") == 'd':
                    d_birth = birth_and_death[0]
                else:
                    f_birth = birth_and_death[0]
            elif len(birth_and_death) == 2:
                if subfield.get("code") == 'd':
                    d_birth = birth_and_death[0]
                    d_death = birth_and_death[1]
                else:
                    f_birth = birth_and_death[0]
                    f_death = birth_and_death[1]

    # I really don't know the difference between d and f subfield codes, d seems to be more reliable though
    if d_birth is None:
        parsed_subfields['authorBirthYear'] = f_birth
    else:
        parsed_subfields['authorBirthYear'] = d_birth
    if d_death is None:
        parsed_subfields['authorDeathYear'] = f_death
    else:
        parsed_subfields['authorDeathYear'] = d_death

    if name2 is None:
        if name1 is not None:
            full_name = name1
    else:
        full_name = "{} {}".format(name2, name1)
    parsed_subfields['authorName1'] = full_name
    return parsed_subfields


def remove_trail_lead(string):
    """
    Removes beginning and ending blank chars from a string.
    :param string: string to be stripped
    :return:
    """
    if string is not None and len(string) > 0:
        if string[0] == ' ':
            string = string[1:]
        if len(string) > 0:
            if string[-1] == ' ':
                string = string[:-1]
    return string


def strip_main_title(titles):
    """
    Removes potential repeats that just have a "."
    :param titles: a MySet() of titles
    :return:
    """
    new_set = MySet()
    for title in titles:
        new_set.add(title.replace(".", "").lower())
    if (len(titles) > 1) and (len(new_set) == 1):
        return new_set
    else:
        return titles



