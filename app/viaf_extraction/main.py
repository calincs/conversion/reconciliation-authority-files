import logging
import os
import requests
import shutil
import gzip
import re

import s3Helpers

from viaf_extraction.parseViaf import ParseViaf
from viaf_extraction.helper_functions import *
from sparkClean import clean as sparkCleaner


def gunzip_file(ungz_file, gz_file):
	with gzip.open(gz_file, 'rb') as f_in:
		with open(ungz_file, 'wb') as f_out:
			shutil.copyfileobj(f_in, f_out)


def gzip_file(ungz_file, gz_file):
	with open(ungz_file, 'rb') as orig_file:
		with gzip.open(gz_file, 'wb') as zipped_file:
			zipped_file.writelines(orig_file)


# TODO this seems slower than it has to be
def download_gzip_file(url, local_filename):
	r = requests.get(url, stream=True)
	with open(local_filename, 'wb') as f:
		for chunk in r.raw.stream(1024, decode_content=False):
			if chunk:
				f.write(chunk)


def create_viaf_sample(file_path, filename):
	# create a sample viaf authority file of the first n lines of the full viaf xml file
	sample_filename = filename.replace(".xml", "_sample.xml")
	lines_number = 10000

	with open(file_path + filename) as input_file:
		with open(file_path + sample_filename, 'w') as output_file:
			head = [next(input_file) for _ in range(lines_number)]
			for line in head:
				output_file.write(line)

	return sample_filename


def viaf_to_s3():

	# This function only called if we already know there is a new version of VIAF

	# Check the most recent version of viaf for filename
	# Most recent dump on viaf
	# returns a date of the form 20230327 ie., YYYYMMDD
	response = requests.get("https://viaf.org/viaf/data/")
	latest_filename = re.findall(r"https:\/\/viaf\.org\/viaf\/data\/viaf-([0-9]*)\-clusters\.xml\.gz", str(response.text))[0]

	print("latest filename: ", latest_filename)

	# Is most recent full viaf dump saved in S3?
	# check what version we have on S3
	# Check if the file already exists on S3
	s3_path = f"recon-api/authority_extraction/data_dumps/"
	s3 = s3Helpers.S3Helper()
	s3_files = s3.list_objects(s3_path)

	full_downloaded = False
	for file in s3_files:
		if latest_filename in file:
			full_downloaded = True
			print("Latest VIAF dump already saved in S3.")


	# Is most recent sample version saved in S3?
	sample_downloaded = False
	for file in s3_files:
		if latest_filename + "_sample" in file:
			sample_downloaded = True
			print("Latest VIAF sample dump already saved in S3.")


	# If we don't have the full version on S3
	if full_downloaded is False:

		# Download zip from viaf.org
		filename = f"viaf-{latest_filename}-clusters.xml.gz"
		url = f"https://viaf.org/viaf/data/{filename}"
		viaf_data_directory = "/app/viaf_extraction/downloads/"

		url = os.environ.get("VIAF_SOURCE").replace("{latest_filename}", latest_filename)

		print(f"Downloading {url}")
		download_gzip_file(url, viaf_data_directory + filename)

		# upload to S3
		print(f"\nUploading {filename} to S3.")
		s3.put_file(s3_path + filename, viaf_data_directory + filename)

		# un-compress in container
		print(f"Unzipping {filename} locally.")
		if ".gz" in filename:
			gunzip_file(viaf_data_directory + filename.replace(".gz", ""), viaf_data_directory + filename)

		# create sample in container
		sample_filename = create_viaf_sample(viaf_data_directory, filename.replace(".gz", ""))

		# gzip sample file
		print(f"Zipping {sample_filename} locally.")
		gzip_file(viaf_data_directory + sample_filename, viaf_data_directory + sample_filename + ".gz")

		# upload sample to S3
		print(f"Uploading {sample_filename} to S3.")
		s3.put_file(s3_path + sample_filename + ".gz", viaf_data_directory + sample_filename + ".gz")


	# If we have full version but not sample
	# then download full version from S3
	# unzip and create sample
	# upload sample
	# TODO
	if sample_downloaded is False:
		pass

	# check that it succeeded
	# return false if not succeeded
	downloaded = False
	s3_files = s3.list_objects(s3_path)
	for file in s3_files:
		if latest_filename in file:
			downloaded = True
	return downloaded, latest_filename


def viaf_to_local():
	# if viaf full and sample xml files are not stored locally in the container, download them from LINCS S3

	s3 = s3Helpers.S3Helper()
	s3_path = f"recon-api/authority_extraction/data_dumps/"
	#s3_files = s3.list_objects(s3_path)

	response = requests.get("https://viaf.org/viaf/data/")
	latest_filename = re.findall(r"https:\/\/viaf\.org\/viaf\/data\/viaf-([0-9]*)\-clusters\.xml\.gz", str(response.text))[0]
	filename = f"viaf-{latest_filename}-clusters.xml.gz"
	sample_filename = f"viaf-{latest_filename}-clusters_sample.xml.gz"
	viaf_data_directory = "/app/viaf_extraction/downloads/"


	# check if the files are already stored locally before downloading
	# since they may have been downloaded directly from viaf.org if they weren't already cached on S3
	if not os.path.isfile(viaf_data_directory + filename):
		if not os.path.isfile(viaf_data_directory + filename.replace(".gz", "")):
			s3.fget_object(s3_path + filename, viaf_data_directory + filename)

	if not os.path.isfile(viaf_data_directory + sample_filename):
		if not os.path.isfile(viaf_data_directory + sample_filename.replace(".gz", "")):
			s3.fget_object(s3_path + filename.replace(".xml", "_sample.xml"), viaf_data_directory + filename.replace(".xml", "_sample.xml"))

	if not os.path.isfile(viaf_data_directory + filename.replace(".gz", "")):
		gunzip_file(viaf_data_directory + filename.replace(".gz", ""), viaf_data_directory + filename)
	if not os.path.isfile(viaf_data_directory + sample_filename.replace(".gz", "")):
		gunzip_file(viaf_data_directory + sample_filename.replace(".gz", ""), viaf_data_directory + sample_filename)

	return viaf_data_directory + filename.replace(".gz", ""), viaf_data_directory + sample_filename.replace(".gz", "")


def main():
	"""
	:param begin: If true will not resume from checkpoint and will remove data.db
	:param filename: If None will attempt to find filename
	"""
	# Setup logger
	logging.basicConfig(level=logging.DEBUG, filename="logfile", filemode="a+",
						format="%(asctime)-15s %(levelname)-8s %(message)s")


	# download viaf to s3 if it's not already there
	in_s3, viaf_filename = viaf_to_s3()

	# if in_s3 is False:
	# 	print("Failed to upload latest VIAF dump to S3.")

	# download
	full_local, sample_local = viaf_to_local()

	# output metadata about the viaf data dump used
	with open("viaf_extraction/results/viaf_metadata.json", "w") as meta_fp:
		json.dump({"authorityVersion": viaf_filename,
				"authorityDate": re.findall(r"([0-9]+)", viaf_filename)[0],
				"authoritySource": "https://viaf.org/viaf/data/"}, meta_fp)

	s3 = s3Helpers.S3Helper()

	# sample data first then full data so we catch errors in the smaller file first

	# create SAMPLE data for works and expressions and authors
	viaf = ParseViaf(begin=True, sample=True)
	viaf.run(expressions=True, works=True, authors=True, xml_filename=sample_local)
	viaf.clean()
	viaf.test()

	sparkCleaner("viaf_works", "/app/viaf_extraction/results/sample_cleaned_works_results.tsv", "/app/viaf_extraction/results/spark_cleaned_viaf_works_sample_results")
	sparkCleaner("viaf_expressions", "/app/viaf_extraction/results/sample_cleaned_expressions_results.tsv", "/app/viaf_extraction/results/spark_cleaned_viaf_expressions_sample_results")
	sparkCleaner("viaf_authors", "/app/viaf_extraction/results/sample_cleaned_authors_results.tsv", "/app/viaf_extraction/results/spark_cleaned_viaf_authors_sample_results")

	s3.bulk_upload("viaf_works_sample")
	s3.bulk_upload("viaf_expressions_sample")
	s3.bulk_upload("viaf_authors_sample")

	# create full data for works and expressions and authors
	viaf = ParseViaf(begin=True, sample=False)
	viaf.run(expressions=True, works=True, authors=True, xml_filename=full_local)
	viaf.clean()
	viaf.test()

	sparkCleaner("viaf_works", "/app/viaf_extraction/results/cleaned_works_results.tsv", "/app/viaf_extraction/results/spark_cleaned_viaf_works_results")
	sparkCleaner("viaf_expressions", "/app/viaf_extraction/results/cleaned_expressions_results.tsv", "/app/viaf_extraction/results/spark_cleaned_viaf_expressions_results")
	sparkCleaner("viaf_authors", "/app/viaf_extraction/results/cleaned_authors_results.tsv", "/app/viaf_extraction/results/spark_cleaned_viaf_authors_results")

	s3.bulk_upload("viaf_works")
	s3.bulk_upload("viaf_expressions")
	s3.bulk_upload("viaf_authors")

	# TODO delete any files created during this so that it can run again without trying to use old files
	# os.remove(os.path.join("checkpoint", filename+".txt"))


if __name__ == '__main__':
	main()
