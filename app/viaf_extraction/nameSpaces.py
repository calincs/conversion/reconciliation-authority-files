"""
NAME TYPES
----------
# Personal
# Geographic
# Corporate
# UniformTitleWork
# UniformTitleExpression
"""

ns = {'v': 'http://viaf.org/viaf/terms#'}
expressionPath = "v:titles/v:expression"
englishSourcePath = "v:sources/v:s"
altNamePath = "v:x400s/v:x400"
authorLangPath = "v:languageOfEntity/v:data/v:text"
authorNationalityPath = "v:nationalityOfEntity/v:data/v:text"
ttlPath = 'v:titles/v:work/v:title'
workPath = 'v:titles/v:work'
typePath = 'v:nameType'
bDayPath = 'v:birthDate'
dDayPath = 'v:deathDate'
genderPath = 'v:fixed/v:gender'
namesPath = 'v:mainHeadings/v:data/v:text'
titleIDPath = 'v:titles/'
authorPath = 'v:titles/v:author'
englishPropPath = 'v:mainHeadings/v:mainHeadingEl'
subfieldPath = 'v:datafield/v:subfield'
foreignTitle = "v:mainHeadings/v:data/v:text"
viafIDPath = 'v:viafID'
sourcesPath = 'v:sources/v:source'
workAltSource = 'v:sources/v:s'
wikiVIAFCode = 'wdt:P214'
wikiISNICode = 'wdt:P213'
# URI Root's
worldCatRoot = 'https://www.worldcat.org/identities/lccn-'
wikidataRoot = 'https://wikidata.org/entity/'
viafRoot = 'https://viaf.org/viaf/'
