import math
import os
import re
import json
import time
import logging
import sqlite3 as sq

from xml.etree import cElementTree as ET
from alive_progress import alive_bar

from viaf_extraction.work import Work
from viaf_extraction.nameSpaces import *
from viaf_extraction.helper_functions import *


class ParseViaf:
    def __init__(self, begin, sample):
        """
        :param begin: if begin is true the code will remove data.db
        :return:
        """
        self.works = {}

        if sample:
            self.sample_path = "sample_"
        else:
            self.sample_path = ""

        db_file_path = os.path.join('/app/viaf_extraction/results/', f'{self.sample_path}data.db')

        # If begin is true and data.db exists, it should be removed.  If data.db DNE create it.
        if (begin is True and os.path.isfile(db_file_path)) or not os.path.isfile(db_file_path):
            if os.path.isfile(db_file_path):
                print_and_log("Removing old data.db file")
                os.remove(db_file_path)
            else:
                print_and_log(f"No {db_file_path} file found, creating it")

            # Create tables
            self.conn = sq.connect(db_file_path)
            self.c = self.conn.cursor()
            temp = Work(self.c)
            temp.create_table(name='authors')
            temp.create_table(name='author_base')
            temp.create_table(name='work_base')
            temp.create_table(name='expression_base')
            temp.create_table(name='work_expression_base')
        else:
            print_and_log("Beginning resume process")
            self.conn = sq.connect(db_file_path)
            self.c = self.conn.cursor()

        # Max page size 2tb
        self.c.execute("PRAGMA max_page_count = 2147483646")
        # Temp files are stored in memory
        self.c.execute("PRAGMA temp_store = 2")
        self.conn.commit()
        self.alt_sources = ("VIAF", "SUDOC", "CAOONL", "LC", "XR", "NLA", "B2Q")
        self.work_sources = ("VIAF", "SUDOC", "CAOONL", "LC", "XR", "DNB", "NLA", "B2Q")
        self.author_sources = ("VIAF", "SUDOC", "CAOONL", "LC", "XR", "BNC", "NLA", "B2Q")
        with open("/app/viaf_extraction/data/blacklist.json") as file:
            json_blacklist = json.load(file)
            self.author_blacklist = json_blacklist['authors']
            self.work_blacklist = json_blacklist['works']
        try:
            with open(os.path.join("/app/viaf_extraction/data/", "wkp_ids.json")) as file:
                self.wikidataID = json.load(file)
                self.wikidataID['author']['viaf'] = MySet(self.wikidataID['author']['viaf'])
                self.wikidataID['author']['isni'] = MySet(self.wikidataID['author']['isni'])
                self.wikidataID['work'] = MySet(self.wikidataID['work'])
        except:
            self.wikidataID = {"author": {'viaf': MySet(), 'isni': MySet()}, "work": MySet()}
        try:
            with open(os.path.join("/app/viaf_extraction/data/", "wkpMap.json")) as file:
                self.wikidataID_map = json.load(file)
        except:
            self.wikidataID_map = {"author": {'viaf': {}, 'isni': {}}, "work": {'viaf': {}}}
        with open(os.path.join("/app/viaf_extraction/queries/", "bulk_labels.rq"), 'r') as f:
            self.bulk_query = f.read()
        self.max_query = 200
        self.n_lines = 0
        self.current_line = 1

    def bulk_wikidata_query(self, identifiers, work_or_author, id_type):
        """
        Takes in a list of ids (viaf or isni) and queries wikidata for matching wikidata URI's that contain these ids.
        :param identifiers: list of ids
        :param work_or_author: work or author base for when writing to wikidataID_map
        :param id_type: will be viaf or isni
        :return:
        """
        query = self.bulk_query.replace("?ALL_VALUES", str(identifiers))
        query = query.replace("id_type", id_type)
        if id_type == 'viaf':
            query = query.replace("?property", wikiVIAFCode)
        elif id_type == 'isni':
            query = query.replace("?property", wikiISNICode)
        results = get_results(query)
        if results is not None:
            for result in results['results']['bindings']:
                identifier = result[id_type].get("value")
                wiki_id = result['subject'].get("value")
                self.wikidataID_map[work_or_author][id_type][viafRoot + identifier] = wiki_id

    def clean(self):
        """
        Takes the final authors, works and expressions tables, cleans them, and uploads cleaned results to s3
        :return: None
        """

        chunksize = 10000
        start_time = time.time()

        for output in ("works", "expressions", "authors"):
            first_line = True
            print_and_log("Cleaning {}".format(output))
            linecount = "SELECT COUNT (*) FROM {}".format(output)
            self.c.execute(linecount)
            linecount = int(self.c.fetchone()[0])
            checkpoint = 0

            # Check if cleaned tables exist, find resume point, if needs to be deleted have to do manually
            query = "SELECT COUNT (*) FROM " + output + "_cleaned"
            try:
                self.c.execute(query)
                # If you want to delete table and cancel resume
                # print_and_log("Deleting " + output + "_cleaned")
                # self.c.execute("DROP TABLE " + output + "_cleaned")
                # self.conn.commit()
                checkpoint = int(self.c.fetchone()[0]/chunksize)
                if checkpoint != 0:
                    first_line = False
            except Exception as e:
                print_and_log(e)
                pass

            query = "SELECT * FROM " + output
            if (((checkpoint+1) * chunksize) <= linecount) or checkpoint == 0:
                with alive_bar(math.ceil(linecount/chunksize)) as bar:
                    for df in pd.read_sql_query(query, self.conn, chunksize=chunksize):
                        if checkpoint != 0:
                            checkpoint -= 1
                            bar()
                        else:
                            df['authorWKP'] = df[['authorWKP',
                                                  'authorVIAFID',
                                                  'authorISNI']].apply(lambda x: self.get_wkp_author_id(x), axis=1)
                            df['workWikidataID'] = df[['workWikidataID',
                                                       'workVIAFID']].apply(lambda x: self.get_wkp_work_id(x), axis=1)
                            df['authorBirthYear'] = df['authorBirthYear'].apply(lambda x: parse_year(x))
                            df['authorDeathYear'] = df['authorDeathYear'].apply(lambda x: parse_year(x))
                            df[['workName1', 'workName2']] = df[['workName1',
                                                             'workName2',
                                                             'workName3']].apply(lambda x: clean_titles(x, sort=True),
                                                                                 axis=1)
                            df[['authorName1', 'authorName2']] = df[['authorName1',
                                                                     'authorName2']].apply(lambda x: clean_names(x, sort=True),
                                                                                           axis=1)
                            if output == 'expressions':
                                df.rename(columns={'expressionVIAFID': 'expressionID'}, inplace=True)
                                df.rename(columns={'workWikidataID': 'expressionWikidata'}, inplace=True)
                                df[['expressionName1', 'expressionName2']] = df['expressionName1'].apply(lambda x: clean_exp_titles(x, sort=True))
                                df.rename(columns={'workLanguage': 'expressionLanguage'}, inplace=True)
                                df.rename(columns={'workLibraries': 'expressionLibraries'}, inplace=True)
                            else:
                                df = df.drop(['expressionName1',
                                              'expressionName2',
                                              'expressionVIAFID',
                                              'expressionWorldcat'], axis=1)
                                df.rename(columns={'workWikidataID': 'workWikidata'}, inplace=True)
                            df.rename(columns={'workVIAFID': 'workID'}, inplace=True)
                            df.rename(columns={'authorVIAFID': 'authorID'}, inplace=True)
                            df.rename(columns={'authorWKP': 'authorWikidata'}, inplace=True)
                            del df['workName3']

                            df.to_sql(output + "_cleaned", self.conn, if_exists='append', index=False)
                            # First line includes the header, else do not include it
                            if first_line is True:
                                df.to_csv(f"/app/viaf_extraction/results/{self.sample_path}cleaned_" + output + "_results.tsv", index=False, header=True,
                                          mode="a", sep='\t')
                                first_line = False
                            else:
                                df.to_csv(f"/app/viaf_extraction/results/{self.sample_path}cleaned_" + output + "_results.tsv", index=False, header=False,
                                          mode="a", sep='\t')
                            bar()

            # Add unique ID
            row = None
            with open(f"/app/viaf_extraction/results/{self.sample_path}cleaned_" + output + "_results.tsv", "r") as f_in:
                with open(f"/app/viaf_extraction/results/{self.sample_path}cleaned_" + output + "_results_indexed.tsv", "w") as f_out:
                    for line in f_in:
                        if row is None:
                            f_out.write("unique_id\t" + line)
                            row = 0
                        else:
                            f_out.write(str(row) + "\t" + line)
                            row = row + 1
            os.remove(f"/app/viaf_extraction/results/{self.sample_path}cleaned_" + output + "_results.tsv")
            os.rename(f"/app/viaf_extraction/results/{self.sample_path}cleaned_" + output + "_results_indexed.tsv",
                      f"/app/viaf_extraction/results/{self.sample_path}cleaned_" + output + "_results.tsv")

        print_and_log("Data cleaning total took : {:.1f} minutes".format((time.time() - start_time) / 60))

    def get_wkp_author_id(self, row):
        """
        Pandas function to check if a row has authorWKP and attempts to find wikidata id from the wikidataID_map using
        both viafID and isni
        :param row: Row data passed from pandas
        :return: wikidata uri or None if it does not exist
        """
        if row['authorWKP'] is not None:
            return row['authorWKP']
        wikiID = None
        viafID = row['authorVIAFID']
        if viafID is not None:
            wikiID = self.wikidataID_map['author']['viaf'].get(viafID)
        if wikiID is not None:
            return wikiID
        isniID = row['authorISNI']
        if isniID is not None:
            wikiID = self.wikidataID_map['author']['isni'].get(isniID)
        return wikiID

    def get_wkp_work_id(self, row):
        """
        Pandas function to check if a row has workWikidataID and attempts to find wikidata id from the wikidataID_map
        using workVIAFID
        :param row: Row data passed from pandas
        :return: wikidata uri
        """
        if row['workWikidataID'] is not None:
            return row['workWikidataID']
        viafID = row['workVIAFID']
        wikiID = self.wikidataID_map['work']['viaf'].get(viafID)
        return wikiID

    def process_author_root(self, root, line):
        """
        Process any line with Personal name type
        Creates a row in author base for every work -- the author information is repeated for each work.
        :param root: XML root
        :param line: The line extracted from the xml, mainly just used for debugging
        :return:
        """
        alt_names = MySet()
        names = MySet()
        name_sources = MySet()

        # Get all possible Author Names
        all_mainHeadingEL = root.findall(englishPropPath, ns)
        for mainHeadingEL in all_mainHeadingEL:
            name1 = None
            name2 = None
            source = mainHeadingEL.find(englishSourcePath, ns)

            if len(all_mainHeadingEL) > 2:
                lang_filter = True
            else:
                lang_filter = False

            if lang_filter is True:
                if source.text not in self.author_sources:
                    continue

            name_sources.add(source.text)

            for subfield in mainHeadingEL.findall(subfieldPath, ns):
                if subfield.get("code") == 'a':
                    name1 = subfield.text
                if subfield.get("code") == 'b':
                    name2 = subfield.text
            if name2 is None:
                full_name = name1
            else:
                full_name = "{} {}".format(name2, name1)
            names.add(full_name)

        # alt titles search
        x400s = root.findall(altNamePath, ns)
        lang_filter = False
        if len(x400s) > 2:
            lang_filter = True
        for x400 in x400s:
            for source in x400.findall(workAltSource, ns):
                if lang_filter is True:
                    if source.text in self.alt_sources:
                        name_sources.add(source.text)
                        for subfield in x400.findall(subfieldPath, ns):
                            if subfield.get('code') == 'a':
                                if subfield.text not in names:
                                    alt_names.add(subfield.text)
                elif lang_filter is False:
                    for subfield in x400.findall(subfieldPath, ns):
                        if subfield.get('code') == 'a':
                            if subfield.text not in names:
                                alt_names.add(subfield.text)

        # Get other author properties
        bday = root.find(bDayPath, ns).text
        dday = root.find(dDayPath, ns).text
        gender = root.find(genderPath, ns)
        authorID = root.find(viafIDPath, ns)
        authorLang = root.find(authorLangPath, ns)
        authorNationality = root.find(authorNationalityPath, ns)
        sources = root.findall(sourcesPath, ns)

        # Get all source links
        author_IDS = {}
        for source in sources:
            text = source.text.split("|")
            base = text[0]
            value = text[1].replace(" ", "")
            if base == "WKP":
                author_IDS["author" + base] = wikidataRoot+value
            if base == "ISNI":
                author_IDS["author" + base] = value
            if base == "LC":
                author_IDS["authorWorldcat"] = worldCatRoot+value

        # If wikidata id was not found, add it to bulk query
        if authorID is not None:
            if author_IDS.get("authorWKP") is None:
                self.wikidataID['author']['viaf'].add(authorID.text)
                isni = author_IDS.get("authorISNI")
                if isni is not None:
                    self.wikidataID['author']['isni'].add(repr(ISNI(isni)))

        # If bulk query at max do query
            for identifier in self.wikidataID['author']:
                if len(self.wikidataID['author'][identifier]) >= self.max_query:
                    self.bulk_wikidata_query(self.wikidataID['author'][identifier], 'author', identifier)
                    self.wikidataID['author'][identifier] = MySet()

        # Iterate through all works
        all_works = root.findall(workPath, ns)

        # uses work model but we will have an entity for every author and group the works
        # instead of one for every work, repeating the authors
        author_entity = Work(self.c)
        # create list of all works for our author database
        allWorksNested = []

        for work in all_works:
            skip_work = False

            # Create row / work entity
            work_entity = Work(self.c)

            # Get work ID
            workID = work.get('id')
            if workID is not None:
                split_id = workID.split("|")
                if split_id[0] == "VIAF":
                    workID = viafRoot + split_id[1]
                work_entity.set_property("workVIAFID", workID)

            # Get work titles (Should only ever be one, other titles are stored in expressions)
            titles = MySet()
            all_work_titles = work.findall("v:title", ns)
            for title in all_work_titles:
                if title is not None:
                    if title.text in self.work_blacklist:
                        # If title is in blacklist and there are other works, skip this work iteration
                        skip_work = True
                        if len(all_work_titles) > 1:
                            continue
                        else:
                            break
                    else:
                        # If title is not in blacklist, add it to titles
                        titles.add(title.text)
                work_entity.set_property('workName1', repr(titles))

            #allWorksNested.append([workID, "|".join(titles)])
            for title in titles:
                allWorksNested.append(title.lower())

            if skip_work is True:
                continue

            # Add Author Properties to work
            if len(names) > 0:
                work_entity.set_property('authorName1', repr(names))

            if len(alt_names) > 0:
                work_entity.set_property('authorName2', repr(alt_names))

            if len(name_sources) > 0:
                work_entity.set_property('workLibraries', repr(name_sources))

            if authorLang is not None:
                work_entity.set_property('authorLanguage', authorLang.text)

            if authorNationality is not None:
                work_entity.set_property('authorCountryOrigin', authorNationality.text)

            if authorID is not None:
                work_entity.set_property('authorVIAFID', viafRoot + authorID.text)

            if bday not in (None, '0', 0):
                work_entity.set_property('authorBirthYear', bday)

            if dday not in (None, '0', 0):
                work_entity.set_property('authorDeathYear', dday)

            if gender is not None:
                work_entity.set_property('authorSex', gender.text)

            for unique_id in author_IDS:
                work_entity.set_property(unique_id, author_IDS[unique_id])

            # Insert values into db
            work_entity.update_db(table_name='author_base')


        # Add author properties to author
        author_entity.set_property("allWorks", allWorksNested)

        if len(names) > 0:
            author_entity.set_property('authorName1', repr(names))

        if len(alt_names) > 0:
            author_entity.set_property('authorName2', repr(alt_names))

        if authorLang is not None:
            author_entity.set_property('authorLanguage', authorLang.text)

        if authorNationality is not None:
            author_entity.set_property('authorCountryOrigin', authorNationality.text)

        if authorID is not None:
            author_entity.set_property('authorVIAFID', viafRoot + authorID.text)

        if bday not in (None, '0', 0):
            author_entity.set_property('authorBirthYear', bday)

        if dday not in (None, '0', 0):
            author_entity.set_property('authorDeathYear', dday)

        if gender is not None:
            author_entity.set_property('authorSex', gender.text)

        for unique_id in author_IDS:
            author_entity.set_property(unique_id, author_IDS[unique_id])

        # Insert values into db
        author_entity.update_db(table_name='authors')

    def process_root(self, root, line, expressions, works, authors):
        """
        Decides name type and process to be used to parse
        :param works: if false skips getting work_base results
        :param expressions: if false skips getting expression_base results
        :param root: xml root
        :param line: raw xml line
        :return: None
        """
        for name_type in root.findall(typePath, ns):
            if name_type.text == "Personal":
                if expressions is True or works is True or authors is True:
                    self.process_author_root(root, line)
            elif name_type.text == 'UniformTitleWork' and works is True:
                self.process_work_root(root, line, expression=False)
            elif name_type.text == 'UniformTitleExpression' and expressions is True:
                self.process_work_root(root, line, expression=True)

    def process_work_root(self, root, line, expression=False):
        """
        Process any UniformTitleWork or UniformTitleExpression root
        :param root: xml root
        :param line: raw xml line (used for debugging)
        :param expression: if nametype is UniformTitleExpression this will be True
        :return:
        """
        # Create row of data
        work_entity = Work(self.c)

        # All sets of information gathered
        work_names = MySet()
        alt_titles = MySet()
        author_names = MySet()
        work_sources = MySet()
        language = None

        # get other sources URL
        sources = root.findall(sourcesPath, ns)
        for source in sources:
            if source.text.split("|")[0] == 'XR':
                # VIAFWORKLCn does not work VIAFWORKPLWABN
                if ('VIAFWORKLCn' in source.text) or ('VIAFWORKPLWABN' in source.text):
                    pass
                else:
                    worldcat_url = "https://www.worldcat.org/oclc/" + re.findall("\d+", source.text)[0]
                    if 'EXP' in source.text:
                        work_entity.set_property('expressionWorldcat', worldcat_url)
                    else:
                        work_entity.set_property('workWorldcat', worldcat_url)

        workID = None
        if expression is False:
            workID = viafRoot + root.find(viafIDPath, ns).text

        else:
            expressionID = viafRoot + root.find(viafIDPath, ns).text
            work_entity.set_property('expressionVIAFID', expressionID)
            # Get parent work ID for expression base
            for work in root.findall(workPath, ns):
                workID = work.get("id")
                if "VIAF" in workID:
                    workID = workID.split("|")[1]
                workID = viafRoot + workID
        if workID is not None:
            work_entity.set_property('workVIAFID', workID)

        # Get author ID
        authorIDs = root.find(authorPath, ns)
        authorID = None
        if authorIDs is not None:
            aID = authorIDs.attrib
            if aID is not None:
                temp = aID.get("id")
                if temp is not None:
                    if "VIAF" in temp:
                        authorID = viafRoot + temp.split("|")[1]
        if authorID is not None:
            work_entity.set_property('authorVIAFID', authorID)

        # # If wikidata id was not found, add it to bulk query, and do query if at max
        if workID is not None:
            self.wikidataID['work'].add(workID.split("/")[-1])
        if len(self.wikidataID['work']) > self.max_query:
            self.bulk_wikidata_query(self.wikidataID['work'], 'work', 'viaf')
            self.wikidataID['work'] = MySet()

        # Get English labels
        englishProps = []
        lang_filter = False
        mainHeadingELs = root.findall(englishPropPath, ns)
        for mainHeadingEL in mainHeadingELs:
            if len(mainHeadingELs) > 2:
                lang_filter = True
            source = mainHeadingEL.find(englishSourcePath, ns)
            if lang_filter is True:
                if source.text in self.work_sources:
                    englishProps.append(process_subfields(mainHeadingEL.findall(subfieldPath, ns)))
            else:
                englishProps.append(process_subfields(mainHeadingEL.findall(subfieldPath, ns)))
            work_sources.add(source.text)

        for propDict in englishProps:
            for column in propDict:
                if column == 'auth_or_work_language':
                    work_entity.set_property('workLanguage', propDict.get(column))
                elif column == "workName2":
                    work_names.add(propDict.get(column))
                elif column == "authorName1":
                    author_names.add(propDict.get(column))
                else:
                    work_entity.set_property(column, propDict.get(column))

        # Remove all works with "Bibel / Bible / United States" authors
        for name in author_names:
            if name.lower() in self.author_blacklist:
                return None

        # Remove worknames such as "Works. ", "[without title] "
        for work_name in work_names:
            if work_name in self.author_blacklist:
                work_names.remove(work_name)

        if len(work_names) > 0:
            if expression is True:
                work_entity.set_property("workName1", repr(strip_main_title(work_names)))
            else:
                work_entity.set_property("workName2", repr(strip_main_title(work_names)))

        # If still no title found, grab a non english title
        else:
            foreign_title = root.find(foreignTitle, ns)
            if foreign_title is not None:
                work_entity.set_property("workName2", foreign_title.text)

        if len(author_names) > 0:
            work_entity.set_property("authorName1", author_names.pop())
            if len(author_names) > 0:
                work_entity.set_property("authorName2", repr(author_names))

        # Second check for main title
        if len(work_names) == 0:
            titles = root.findall(namesPath, ns)
            for title in titles:
                if len(work_names) == 0:
                    work_names.add(title.text)
                else:
                    if title not in work_names:
                        alt_titles.add(title.text)

        # alt titles search
        x400s = root.findall(altNamePath, ns)
        lang_filter = False

        if len(x400s) > 2:
            lang_filter = True
        for x400 in x400s:
            for source in x400.findall(workAltSource, ns):
                if lang_filter is True:
                    if source.text in self.alt_sources:
                        for subfield in x400.findall(subfieldPath, ns):
                            if subfield.get('code') == 't':
                                if subfield.text not in work_names:
                                    work_sources.add(source.text)
                                    alt_titles.add(subfield.text)
                elif lang_filter is False:
                    for subfield in x400.findall(subfieldPath, ns):
                        if subfield.get('code') == 't':
                            if subfield.text not in work_names:
                                alt_titles.add(subfield.text)
                                if source.text is not None:
                                    work_sources.add(source.text)

        if len(alt_titles) > 0:
            if expression is True:
                work_entity.set_property('expressionName1', repr(alt_titles))
            else:
                work_entity.set_property('workName3', repr(alt_titles))
        else:
            if expression is True:
                # If a workName1 is found and no expressions are found, the expression title is usually
                # the same as the work
                workName1 = work_entity.properties['workName1'][0]
                if workName1 is not None:
                    work_entity.set_property('expressionName1', workName1)
                else:
                    work_entity.set_property('expressionName1', work_entity.properties['workName2'][0])

        if len(work_sources) > 0:
            work_entity.set_property("workLibraries", repr(work_sources))

        if language is not None:
            work_entity.set_property("workLanguage", language)

        if expression is False:
            # First update the work base then iterate through expressions
            work_entity.update_db(table_name='work_base')
            # Search for expressions of work
            # Get expressions from work base
            expressions = root.findall(expressionPath, ns)
            for exp in expressions:
                work_expression = Work(self.c)
                expressionID = viafRoot+exp.get("id").split("|")[1]

                work_expression.set_property("workVIAFID", workID)
                work_expression.set_property("expressionVIAFID", expressionID)

                language = exp.find("v:lang", ns)
                if language is not None:
                    work_expression.set_property("workLanguage", language.text)

                source = exp.find("v:sources", ns)
                if source is not None:
                    if source.text is not None:
                        work_expression.set_property("workLibraries", source.text)

                parsed_exp = process_subfields(exp.findall("v:datafield/v:subfield", ns))
                if parsed_exp.get('auth_or_work_language') is not None:
                    parsed_exp["workLanguage"] = parsed_exp.pop('auth_or_work_language')
                for prop in parsed_exp:
                    work_expression.set_property(prop, parsed_exp.get(prop))
                work_expression.update_db(table_name='work_expression_base')

        else:
            work_entity.update_db(table_name='expression_base')

    def run(self, expressions, works, authors, xml_filename):
        """
        Runs the main program
        :param expressions: if false will skip creating expression_based entities
        :param works: if false will skip creating work_based entities
        :param authors: if false will skip creating author entities
        :param xml_filename: local path of xml file
        :return:
        """

        start_time = time.time()
        checkpoint_path = os.path.join("/app/viaf_extraction/checkpoint/", self.sample_path + xml_filename.split("/")[-1].replace(".xml", "") + ".txt")
        # Attempt to load checkpoint file
        # TODO haven't updated checkpoint for authors
        try:
            if os.path.isfile(checkpoint_path):
                with open(checkpoint_path, 'r') as cp_file:
                    checkpoint = cp_file.readlines()
                    if len(checkpoint) == 2:
                        print_and_log("Automatically resuming from line {}".format(checkpoint[1].strip()))
                        self.current_line = int(checkpoint[1].strip())
                    if len(checkpoint) in (1, 2):
                        self.n_lines = int(checkpoint[0].strip())
                    else:
                        pass

            # If current line is not one, go to that line and continue processing from there
            if (self.current_line != 1) and (self.current_line != self.n_lines):
                print_and_log("Resuming Processing root XML file from line {}".format(self.current_line))
                with open(xml_filename, "r") as xml_all_lines:
                    count = 0
                    with alive_bar(self.n_lines) as bar:
                        # Go to checkpoint
                        while count <= self.current_line:
                            next(xml_all_lines)
                            bar()
                            count += 1
                        # Continue from checkpoint
                        for line in xml_all_lines:
                            line = line[line.find("<"):]
                            root = ET.fromstring(line)
                            self.process_root(root, line, expressions, works, athors)
                            with open(checkpoint_path, 'w') as cp_file:
                                cp_file.write(str(self.n_lines) + "\n" + str(count))
                            count += 1
                            bar()

            # If current line is one, start from beginning of xml file
            else:
                # If length of xml file is not counted, get the line count
                if self.n_lines == 0:
                    with open(xml_filename, "r") as xml_all_lines:
                        line_time = time.time()
                        while True:
                            try:
                                next(xml_all_lines)
                                self.n_lines += 1
                            except:
                                break
                        print_and_log("getting {} lines count took : {:.1f} minutes".format
                                      (self.n_lines, (time.time() - line_time) / 60))
                        with open(checkpoint_path, "w") as cp_file:
                            cp_file.write(str(self.n_lines))

                # If not using checkpoint, start from beginning of file and process line by line
                if self.current_line != self.n_lines:
                    with open(xml_filename, "r") as xml_all_lines:
                        print_and_log("Processing root XML file")
                        with alive_bar(self.n_lines) as bar:
                            for line in xml_all_lines:
                                line = line[line.find("<"):]
                                root = ET.fromstring(line)
                                self.process_root(root, line, expressions, works, authors)
                                with open(checkpoint_path, 'w') as cp_file:
                                    cp_file.write(str(self.n_lines) + "\n" + str(self.current_line))
                                self.current_line += 1
                                bar()

                print_and_log("Processing Lines took : {:.1f} minutes".format((time.time() - start_time) / 60))

        except Exception as e:
            print_and_log(e)
            # If something errors out, make sure all checkpoint data is saved
            self.conn.commit()
            with open("/app/viaf_extraction/data/wkpMap.json", "w") as outfile:
                json.dump(self.wikidataID_map, outfile)
            with open("/app/viaf_extraction/data/wkp_ids.json", "w") as outfile:
                # Convert sets to lists to they can be converted to JSON
                self.wikidataID['author']['viaf'] = list(self.wikidataID['author']['viaf'])
                self.wikidataID['author']['isni'] = list(self.wikidataID['author']['isni'])
                self.wikidataID['work'] = list(self.wikidataID['work'])
                json.dump(self.wikidataID, outfile)
            print_and_log("Something went wrong, try re-running from saved checkpoint")
            quit()

        # Do final reverse lookups:
        if works is True or expressions is True:
            # Do final VIAF and ISNI Wikidata lookup
            for identifier in self.wikidataID['author']:
                self.bulk_wikidata_query(self.wikidataID['author'][identifier], 'author', identifier)
            # Do final work Wikidata lookup
            self.bulk_wikidata_query(self.wikidataID['work'], 'work', 'viaf')
            start_time = time.time()
            self.conn.commit()
            print_and_log("Base commit took : {:.1f} minutes".format((time.time() - start_time) / 60))

            start_time = time.time()
            with open("/app/viaf_extraction/data/wkpMap.json", "w") as outfile:
                json.dump(self.wikidataID_map, outfile)
            print_and_log("Writing reverse lookup table took : {:.1f} minutes".format((time.time() - start_time) / 60))

            with open("/app/viaf_extraction/queries/mergeQuery.txt") as file:
                query = file.read()

            start_time = time.time()
            try:
                print_and_log("Left joining work_base and author_base into [works]")
                self.c.execute(query)
                self.conn.commit()
            except sq.OperationalError as e:
                if check_table(self.c, "works"):
                    print_and_log("Dropping [works] data table and re-joining work_base and author_base")
                    self.c.execute("""DROP TABLE works""")
                    self.c.execute(query)
                    self.conn.commit()
                else:
                    print_and_log(e)
            except Exception as e:
                logging.info(e)

            print_and_log("Merging work base with author base took : {:.1f} minutes".format((time.time() - start_time) / 60))

            with open("/app/viaf_extraction/queries/mergeQueryExpressions.txt") as file:
                query = file.read()
            start_time = time.time()

            try:
                print_and_log("Inner joining expression_base and author_base into [expressions]")
                self.c.execute(query)
                self.conn.commit()
            except sq.OperationalError as e:
                if check_table(self.c, "expressions"):
                    print_and_log("Dropping [expressions] data table and re-joining expression_base & author_base")
                    self.c.execute("""DROP TABLE expressions""")
                    self.c.execute(query)
                    self.conn.commit()
                else:
                    print_and_log(e)
            except Exception as e:
                logging.error(e)

            print_and_log("Merging expression base with author base took : {:.1f} minutes".format((time.time() - start_time) / 60))

    def test(self):
        """
        Tests to ensure the cleaned data is correct
        """
        # Get all works works_base table
        self.c.execute("""SELECT DISTINCT workVIAFID from work_BASE where workVIAFID is not null""")
        work_base_ids = set(self.c.fetchall())
        # Get all works from works table
        self.c.execute("""SELECT DISTINCT workVIAFID from works where workVIAFID is not null""")
        all_works = set(self.c.fetchall())
        # Get all author_based works with a viaf id
        self.c.execute("""SELECT DISTINCT workVIAFID from author_base where workVIAFID is not null""")
        author_base_ids = set(self.c.fetchall())
        # Get all expressions with a base viaf id
        self.c.execute("""SELECT DISTINCT workVIAFID from expression_base where workVIAFID is not null""")
        expressions_with_viaf = set(self.c.fetchall())
        # Get all expressions ids
        self.c.execute("""SELECT DISTINCT expressionVIAFID from expression_base where expressionVIAFID is not null""")
        expression_ids = set(self.c.fetchall())
        # Get all work -> expression ids
        self.c.execute("""SELECT DISTINCT expressionVIAFID from work_expression_base where expressionVIAFID is not null""")
        work_expression_ids = set(self.c.fetchall())

        # Assert expression/works has same count as cleaned counterparts
        for table in ("expressions", "works"):
            query = """SELECT COUNT (*) from {}""".format(table)
            self.c.execute(query)
            base_count = int(self.c.fetchone()[0])
            query = """SELECT COUNT (*) from {}_cleaned""".format(table)
            self.c.execute(query)
            cleaned_count = int(self.c.fetchone()[0])
            assert base_count == cleaned_count

        # Assert all expression_base and work_expression_base have a expression id
        self.c.execute("""SELECT COUNT (*) from work_expression_base where expressionVIAFID is null""")
        assert int(self.c.fetchone()[0]) == 0
        self.c.execute("""SELECT COUNT (*) from expression_base where expressionVIAFID is null""")
        assert int(self.c.fetchone()[0]) == 0

        # Assert that every entity in work_base has a work viaf id
        self.c.execute("""SELECT COUNT (*) from work_base where workVIAFID is null""")
        assert int(self.c.fetchone()[0]) == 0

        # Assert all author_base have author VIAF ID
        self.c.execute("""SELECT COUNT (*) from author_base where authorVIAFID is null""")
        assert int(self.c.fetchone()[0]) == 0

        # All expressions work id's in workbase
        self.c.execute("""SELECT workVIAFID from expressions where workVIAFID is not null""")
        expression_workids = set(self.c.fetchall())
        try:
            assert expression_workids.issubset(work_base_ids)
        except:
            diff_ids = expression_workids.difference(work_base_ids)
            print_and_log("The following expressions have a expression_base workVIAFID that are not in work_base. "
                          "More often this is due to incorrectly entered expressions")
            for diff_id in diff_ids:
                print(diff_id)

        # Are all work_expression_base in expression_base?
        work_expressions_not_in_expressions = work_expression_ids.difference(expression_ids)
        print_and_log("The following works that link to expressions are incorrectly linked, more often they just link"
                      "to themselves")
        for work in work_expressions_not_in_expressions:
            print_and_log(work)

        # Assert that work_base workVIAF IDs is a subset of works
        try:
            assert work_base_ids.issubset(all_works)
        except:
            print_and_log("A work_base workVIAF id is not in works workVIAF ID, likely an error with the join")

        # All author_base workVIAF IDs in works
        try:
            assert author_base_ids.issubset(all_works)
        except:
            print_and_log("A author_base workVIAF id is not in works workVIAF ID, likely an error with the join")

        # Assert that all author base works are in works, and those that are not are expressions
        should_be_expressions = author_base_ids.difference(work_base_ids)
        if should_be_expressions.issubset(expression_ids) is False:
            not_in_expressions = should_be_expressions.difference(expression_ids)
            print_and_log("The following id's should be Personal:")
            for express in not_in_expressions:
                print_and_log(express)
        print_and_log('All assert tests passed')
