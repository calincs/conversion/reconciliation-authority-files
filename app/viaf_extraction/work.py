from viaf_extraction.helper_functions import print_and_log


class Work:

    def __init__(self, cursor):
        self.c = cursor
        # These are all the headers that will be saved to database, value1 is value, value2 is the datatype
        self.properties = {
            "authorBirthYear": [None, "text"],
            "authorCountryOrigin": [None, "text"],
            "authorDeathYear": [None, "text"],
            "authorISNI": [None, "text"],
            "authorLanguage": [None, "text"],
            "authorName1": [None, "text"],
            "authorName2": [None, "text"],
            "authorSex": [None, "text"],
            "authorVIAFID": [None, "text"],
            "authorWKP": [None, "text"],
            "authorWorldcat": [None, "text"],
            "expressionName1": [None, "text"],
            "expressionName2": [None, "text"],
            "expressionVIAFID": [None, "text"],
            "expressionWorldcat": [None, "text"],
            "workLanguage": [None, "text"],
            "workLibraries": [None, "text"],
            "workName1": [None, "text"],
            "workName2": [None, "text"],
            "workName3": [None, "text"],
            "workVIAFID": [None, "text"],
            "workWikidataID": [None, "text"],
            "workWorldcat": [None, "text"],
            "allWorks": [None, "text"]  ## used when creating author_base. Will be list of lists [[workTitle, workVIAF], [workTitle, workVIAF], ...]
        }
        self.placeholder = '('
        for i in range(len(self.properties) - 1):
            self.placeholder += "?, "
        self.placeholder += "?)"

    def __repr__(self):
        string = ''
        for prop in self.properties:
            value = self.properties.get(prop)
            if value is not None:
                string += "{} : {}\n".format(prop, value)
        return string

    def update_db(self, table_name):

        # WARNING NOT SAFE FOR PRODUCTION DUE TO INJECTION POSSIBILITY
        query = "INSERT INTO {} VALUES ".format(table_name) + self.placeholder
        self.c.execute(query, tuple(self.properties.get(i)[0] for i in self.properties))

    def create_table(self, name):
        create = True
        check_query = "SELECT name FROM sqlite_master WHERE type='table' AND name='{}';".format(name)
        self.c.execute(check_query)
        data = self.c.fetchall()
        if len(data) > 0:
            print_and_log("Using pre-existing table {}".format(name))
            create = False
            # self.c.execute("DROP TABLE {};".format(name))

        if create is True:
            print_and_log("Creating table {}".format(name))
            query_string = "CREATE TABLE {} ( ".format(name)
            for header in self.properties:
                query_string += header + " " + self.properties.get(header)[1] + ', '
            query_string = query_string[:-2]
            query_string += " )"
            self.c.execute(query_string)

    def set_property(self, header, value):
        try:
            value = str(value)
            self.properties[header][0] = value
        except Exception as e:
            print(e)
            print("Value cannot be converted to string")
