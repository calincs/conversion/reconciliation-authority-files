# Wikidata Bibliographic Data Extraction

### Summary
Code to extract Wikidata entities and properties of all written works and their authors.  There are two major branches of data we have to collect.  One branch being authors and their associated details saved as __results/author_results.tsv__ and __results/author_results_cleaned.tsv__.  The other branch would be works, and their associated details saved as __results/work_results.tsv__ and __results/work_results_cleaned.tsv__.  

Before building all the information about the works, you need to build a set of authorIDs/URIs and a set of work IDs/URIs that are used to query information in bulk.  

#### Works

The set of works can be built using two different methods. The first method is used in the code.

1.  Work based works: Gathered simply by using the __get_works.rq__ query. 
2.  Author based works: Using the __get_authors.rq__ query to get a list of authors, then querying the works of those authors
    
At this time only work_based works are being used as it seems to get all necessary works.

#### Authors

The Author data can be gathered using two different methods. Currently only second method is used in the code.

1.  Work based authors: Author Wikidata IDs are gathered in bulk using the ?author P:50 ?work query using the works gathered from 
    __get_works.rq__. Then author information is queried and merged with work data.
    1. These results are saved in __work_results.tsv__ and are considered work_base (more details below) results
    2. Only authors who have works directly associated with them on Wikidata will be present in this dataset 
2. Author based authors: Author Wikidata IDs are gathered using __get_authors.rq__ 
    1. Author information is queried in bulk and is saved to __author_results.tsv__ 
    2. The reason for this second export is that not all authors have works associated with them, and would be missed in the works tsv.
      

## Overview


### Getting work properties
Any properties that are gathered for the works are defined in the __work_properties.json__ file and are separated into three main types all with their own columns: 

- work_prop : these are properties based on the works such as author, publisher, title etc...
- identifiers : these are id based properties of works i.e. ISBN, OCLC, etc...
- author_prop : these are properties based on author such as date of birth, sex/gender, family name, etc.


### Adding new columns

Any newly added properties/columns will have to be added to their respective type in __work_properties.json__

For any column that would return a URI (for example, author/wdt:P50 almost always returns a URI) and you would prefer a label you must add the "labeled": "True" parameters to that column in __work_properties.json__.

Additionally, there are also the following properties:
- combine_cols : When dealing with multiple value returns, will merge all into a single column separated by ','

The main python script will automate the queries to get all these properties for you. These queries are built in the __init__ of the Wiki class and create the following skeletons used for queries.
- bulk_author_properties.rq : Used in conjunction with building the work_base table
- bulk_author_properties2.rq : Used when building the author_base table
- bulk_work_properties.rq : Used to get work properties in bulk after list of works is collected using get_works.rq

Do not update the above queries manually as they will be overwritten on init. 

### Querying Wikidata In Bulk

In the queries folder there are multiple queries that start with __bulk__. These queries all have in common the following 
line at the beginning of the where clause:

```VALUES ?author { ?ALL_VALUES } ```

as seen in the bulk_labels query:
```sql
SELECT DISTINCT ?subject ?label
WHERE
{
  {
    VALUES ?subject { ?ALL_VALUES }
    SERVICE wikibase:label { 
     bd:serviceParam wikibase:language "[AUTO_LANGUAGE],?LANGUAGE" .
     ?subject rdfs:label ?label . 
    }
  }
}

```

Using the __get_results()__ method in __helperFunctions.py__ you can pass any of the bulk queries along with a set() of full URI values using the __bulk_values__ argument.  This will then replace ?ALL_VALUES with a string of wd:Q******** values stripped from the full URI values.  If this query fails or times out it will then split the set of values into four groups and query them individually.  For a maximum of __max_attempts__ times it will double the partition size each time until all data is received.  

The maximum amount of entities for different bulk queries are controlled using the following variables in the __WikidataQuery__ init(): 
- max_entity_query: used when collecting work or author properties
- max_label_query : used when collecting a collection of labels.

You can increase these numbers to attempt to speed things up, but if the value is too large you will just cause additional work from having to constantly partition the values.

The queries __bulk_labels.rq__, __bulk_author_works.rq__ and __bulk_labels_all_lang.rq__ are not built automatically and thus any changes would need to be done directly to the file. On __init__ the ?LANGUAGE variable in the __bulk_labels.rq__ is replaced with the language argument.


### Index / Unique Identifiers

By default, works with multiple authors will be duplicated into a separate row for each of the individual authors (alt_mode enabled). In this case, if a work has just one author, its unique identifier will be the work wikidata URI. If there are multiple authors the unique id will be "<work_wikidata_uri>\_<author_wikidata_uri>"


#### Estimated time

The query __get_works.rq__ currently gets about 1.4 million works. If using the cached label dictionary with a max of 20 headers you can expect less than 2 hours or the following times when run on the server.

```
Getting all work based properties took: 74.2 minutes 
Getting all work based labels took: 4.4 minutes 
Getting all author based props and labels labels took: 28.1 minutes 
Mapping back author results took: 0.1 minutes 

real    110m14.623s 
user    5m37.111s 
sys     0m39.323s
```

# Blacklist

Any items that you do not wish to have must be added to the __queries/get_remove_works.rq__ query.  Currently the 
following are blacklisted:

- All biographical articles 
- All electronic games (videogames) and subclasses
- All scholarly work
- All audio/visual work
- All musical work

The blacklist works by gathering a set of work id's from the query and removing these id's from the set creating from __queries/get_works.rq__

# Cleaning Methods

As of November 2020, the script cleanData.py was designed to organize data collected in main.py using default enabled alt mode. The DataClean class only has the argument author_base which if enabled will clean author_results.tsv and will always overwrite any previously existing .tsv files. If disabled, the Class will clean results.tsv. DataClean.clean is ran for both author_base=True and author_base=False

1.  All leading and trailing blank chars ' ' are removed from every value (helps with matching)
2.  Any workLabel that does not have an english label and no __title__ value exists will have labels for all languages 
    queried and one chosen at random.
3. Publication dates are split into multiple columns, and are squashed to just the year and repeated values of the same 
   year are deleted.  If more than one year value exists, the minimum is used.
4. Author dates are comma separated values in a single column and squashed just like publication dates. If the 
   difference between the min and max birth/death date is greater than 4, all results are removed. Otherwise, the floor 
   of the average is used and an asterisk is added to the end of the year.
5. The following columns follow a one or nothing rule where if more than one result exists all results are removed. 
   [countryOfOrigin, editionNumber, formatOfCreativeWork, languageOfWorkOrName, publisher]
6. The following columns have multiple values and are squashed into a single column with comma separated values. 
   [isbn10, isbn13, oclcWorkId, viafId]
7. If no work alt_label exists but a __title__ value exists, the title value is used.
8. Work alt_label is generally a comma separated list. The first randomly chosen value in alt_label that is not a 
   duplicate of the main work label is used.
9. 2 Work titles are chosen following the hierarchy (where any repeats are removed):
  1. Work Label
  2. Title
  3. Alt Label (Chosen at random)
10. Genre columns are removed
11. 3 Author names are chosen following the hierarchy (where any repeats are removed):
   1. Author Label (__LiteralAuthor__ name used if none exist)
   2. Combined names (chosen at random from a combination of __firstName__ and __lastName__ if only one first name exists)
   3. Name in native language
   4. Alt label (chosen at random)
12. Multiple column names are changed to match other reconciliation services column names.


# Arguments (mainly used for debugging)
If you do not wish to overwrite all data you can pass one or more of the following arguments to skip overwriting the files.  More information on the label dictionary can be found below.
- -d --delete : will delete the work label dictionary and rebuild it from scratch [Default: Not enabled]

The label dictionary is stored as __data/label_dictionary.json__. Columns with labels under work_prop['cols'] in __work_properties.json__ i.e. genre, country of origin, language, author etc... are generally static and do not need to be updated. Hence they are stored in a dictionary / json for fast lookup and to drastically reduce label query time. If you were to add a new labeled column and run the program using defaults it will add any newly associated labels to the label dictionary.  The only time you would want to delete the label dictionary is if you are concerned that a commonly used label such as an author's name has recently changed.

- -t --tsv : will not overwrite/update the tsv and json output. [Default: Not Enabled]

For example if this argument is passed the new results from running the program will not overwrite __work_results__.tsv/json__ and __author_results__.tsv/json.  This is mainly a debugging feature  

- -a --authors : skips creating a tsv with just author properties [Default: Not Enabled]

If you do not wish to create the author based __author_results.tsv/json__ use this argument.
- -w --works : skips creating work based author+work properties [Default: Not Enabled]

If you do not wish to run and create the work based __work_results.tsv/json__ use this argument. 

- -c --clean : Skips cleaning work_results.tsv and author_results.tsv and does not create __author_results_cleaned.tsv__ and __work_results.tsv__ [Default: Not Enabled]

- -s --second : different method that keeps multiple authors in one row (disables alt_mode) [Default: Not Enabled]

If this argument is passed it will disable alt_mode and will create a new column By default alt_mode is enabled, this means that if a work has more than one author

##### For example:

```python main.py``` 

This is the standard usage. It will use and update the label dictionary (if it exists) with any newly found URI's labels, but will not update pre-existing work based URI's with new labels. Saves new results.json/tsv files.

```python main.py -t``` 

Will delete the dictionary and save the new result for the dictionary, and the tsv / json output

```python main.py -aw``` 

Will skip building __results.tsv__ and __author_results__.tsv but will clean the tables if they already exist.

```python main.py --clean``` 

Will skip the cleaning process. Cleaned results are always overwritten if this option is not enabled.
