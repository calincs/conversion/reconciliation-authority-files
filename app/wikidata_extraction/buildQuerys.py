def build_query(self, uri_type, author_base=False):
    """
    Build query to get all work/author properties and labels in work_properties.json in a bulk manner using the
    ?ALL_VALUES variable.
    :writes: SPARQL queries/get_work_properties.rq query
    :return: None
    """

    if uri_type == "work":
        query = "SELECT DISTINCT ?work ?id_label ?alt_label ?work_Description"
    if uri_type == "author":
        if author_base is False:
            query = "SELECT DISTINCT ?author ?label ?label_alt ?author_Description"
        else:
            query = "SELECT DISTINCT ?author ?author_0_label ?author_0_label_alt ?author_Description"

    ### SELECT HEADERS ###
    # Add ?book headers
    if uri_type == "work":
        for headers_lit in self.query_metadata["work_prop"]['cols']:
            query += f"?{headers_lit} "
        # Add ID based headers
        for identifier in self.query_metadata["ids"]['cols']:
            query += f"?{identifier} "
    if uri_type == "author":
        author_label_vars = {}
        for header in self.query_metadata["author_prop"]['cols']:
            if self.query_metadata["author_prop"]['cols'][header]['labeled'] == "True":
                if author_base is True:
                    header = f"author_0_{header}"
                variable = f"?{header}_label "
                author_label_vars[header] = variable
                variable += f"?{header} "
                query += variable
            else:
                if author_base is True:
                    header = f"author_0_{header}"
                query += f"?{header} "

    query += "\nWHERE"
    query += "\n{"
    query += "\n\t{"
    if uri_type == "work":
        query += "\n\t\tVALUES ?work { ?ALL_VALUES } "
    if uri_type == "author":
        query += "\n\t\tVALUES ?author { ?ALL_VALUES } "

    ### GET OPTIONAL DATA ###
    if uri_type == "work":
        for header_group in self.query_metadata:
            # skip author properties, need to be done individually to reduce churn
            if header_group == "author_prop":
                continue
            for header in self.query_metadata[header_group]['cols']:
                query += '\n\t\tOPTIONAL { ' + self.query_metadata[header_group]['base'] + \
                         " " + self.query_metadata[header_group]['cols'][header]['id'] + \
                         ' ?' + header + ' }'

    if uri_type == "author":
        for header in self.query_metadata["author_prop"]['cols']:
            if author_base is False:
                query += '\n\t\tOPTIONAL { ' + self.query_metadata["author_prop"]['base'] + \
                         " " + self.query_metadata["author_prop"]['cols'][header]['id'] + \
                         ' ?' + header + ' }'
            else:
                query += '\n\t\tOPTIONAL { ' + self.query_metadata["author_prop"]['base'] + \
                         " " + self.query_metadata["author_prop"]['cols'][header]['id'] + \
                         ' ?' + "author_0_" + header + ' }'

    ### GET MAIN LABEL ###
    query += "\n\t\tSERVICE wikibase:label { "
    query += '\n\t\t\tbd:serviceParam wikibase:language "[AUTO_LANGUAGE],{}" .'.format(self.language)
    if uri_type == "work":
        query += "\n\t\t\t?work rdfs:label ?id_label . "
        query += "\n\t\t\t?work skos:altLabel ?alt_label . "
        query += "\n\t\t\t?work schema:description ?work_Description . "
    if uri_type == "author":
        if author_base is False:
            query += "\n\t\t\t?author rdfs:label ?label . "
            query += "\n\t\t\t?author skos:altLabel ?label_alt . "
            query += "\n\t\t\t?author schema:description ?author_Description . "
        else:
            query += "\n\t\t\t?author rdfs:label ?author_0_label . "
            query += "\n\t\t\t?author skos:altLabel ?author_0_label_alt . "
            query += "\n\t\t\t?author schema:description ?author_Description . "

        for var in author_label_vars:
            query += "\n\t\t\t?" + var + " rdfs:label " + author_label_vars[var] + " . "
    query += "\n\t\t}"
    query += "\n\t}"
    query += "\n}"

    # Write file (Even though just the variable is needed, query can be used for testing)
    if uri_type == "work":
        with open("wikidata_extraction/queries/bulk_work_properties.rq", 'w') as file:
            file.write(query)
    if uri_type == "author":
        if author_base is False:
            with open("wikidata_extraction/queries/bulk_author_properties.rq", 'w') as file:
                file.write(query)
        else:
            with open("wikidata_extraction/queries/bulk_author_properties2.rq", 'w') as file:
                file.write(query)

    return query


def get_labeled_headers(self, uri_type):
    """
    Iterates through all headers and add their value to a set if they should be labeled
    :param self:
    :return: set of labeled header strings
    """
    # Get work based labels
    labeled_headers = set()
    if uri_type == "work":
        for col in self.query_metadata['work_prop']['cols']:
            if col == "creator_0" or col == "author_0":
                continue
            if self.query_metadata['work_prop']['cols'][col].get("labeled") == "True":
                labeled_headers.add(col.split("_")[0])
        return labeled_headers

    if uri_type == "author":
        for col in self.query_metadata['author_prop']['cols']:
            if self.query_metadata['work_prop']['cols'][col].get("labeled") == "True":
                labeled_headers.add(col.split("_")[0])
        return labeled_headers
