import pandas as pd
import json
import numpy as np
from wikidata_extraction.helperFunctions import *
import time

# logging.basicConfig(level=logging.DEBUG, filename="logfile", filemode="a+",
#                     format="%(asctime)-15s %(levelname)-8s %(message)s")

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format="%(asctime)-15s %(levelname)-8s %(message)s")


# TO FIX
# some lists are ending up with space comma then an item work_CountryOrigin_URI work_CountryOrigin work_Genre_URI work_Genre work_WikidataType work_WikidataType_URI work_ISBN10_ID work_ISBN13_ID work_Language_URI work_Language work_Publisher_URI work_Publisher
# when combining columns like genre_1_label and genre_0_label, it's only taking genre_1_label and making genre_0_label a blank string in the list.
# work_OCLCcontrol_ID is getting empty prefix in every row at the start https://worldcat.org/OCLC/ , https://worldcat.org/OCLC/68262369
# merge work_OCLCcontrol_ID work_OCLC_ID maybe? in sample no values for work_OCLC_ID

class DataClean:

    def __init__(self, author_base, max_label, service, max_headers=5):
        """
        :param author_base: If true will clean author_results.tsv otherwise will clean work_results.tsv
        """
        self.author_base = author_base
        self.file_found = True
        self.service = service
        self.max_headers = max_headers
        if author_base is False:
            try:
                with open("wikidata_extraction/results/work_results.json") as fp:
                    self.data = json.load(fp)
            except FileNotFoundError:
                print("work_results.json not found")
                self.file_found = False
        else:
            try:
                with open("wikidata_extraction/results/author_results.json") as fp:
                    self.data = json.load(fp)
            except:
                print("author_results.json not found")
                self.file_found = False
        with open("wikidata_extraction/queries/bulk_labels_all_lang.rq") as fp:
            self.bulk_label_query = fp.read()
        self.max_label_query = max_label

    def check_year_duplicates(self, id, cols):
        """
        Method for checking / cleaning years in a set of columns
        :param id: the unique id associated with the col values
        :param cols: a set of columns from the same category, i.e. (dateOfBirth_1, dateOfBirth_2)
        :return:
        """
        year_set = set()
        sorted_cols = sorted(cols, reverse=True)

        for col in cols:
            value = self.data[id][col]
            if pd.isnull(value):
                return None

            dates = value.split(" , ")
            for date in dates:
                year = parse_year(date)
                if year is not None:
                    year_set.add(year)

        if len(cols) == 1:
            # Used for author based years
            if len(year_set) == 0:
                self.data[id].pop(cols[0])

            elif len(year_set) == 1:
                self.data[id][cols[0]] = year_set.pop()
            elif len(year_set) >= 2:
                if (max(year_set) - min(year_set)) < 4:
                    self.data[id][cols[0]] = str(int(np.floor(np.mean(tuple(year_set))))) + '*'
                else:
                    self.data[id].pop(cols[0])

        elif len(cols) > 1:
            # This should only apply to publication dates

            if len(year_set) == 0:
                for col in sorted_cols:
                    if len(cols) > 1:
                        cols.remove(col)
                    else:
                        self.data[id].pop([col])

            elif len(year_set) == 1:
                for col in sorted_cols:
                    if len(cols) > 1:
                        cols.remove(col)
                        self.data[id].pop(col)
                    else:
                        self.data[id][col] = year_set.pop()

            else:
            # If more than one year, take the min
                for col in sorted_cols:
                    if len(cols) > 1:
                        cols.remove(col)
                        self.data[id].pop(col)
                    else:
                        self.data[id][col] = str(int(str(min(year_set)))) + "*"

    def get_bulk_labels(self, label_set, map_set, author=False):
        """
        Takes a set of id's from label_set and queries the label in all languages and maps back results
        :param label_set: a set of labeled entities commonly workid or authorids
        :param map_set: is a map of label entity your searching -> a set of id's associated with it
        :param author: used to differentiate getting a work label vs an author label.
        :return:
        """
        results = self.service.query.get_results(self.bulk_label_query, bulk_values=label_set, query_type='label')
        result_set = {}
        if results is None:
            return None

        for result in results:
            id = result['subject'].get('value')
            if id not in result_set:
                result_set[id] = set()
            result_set[id].add(result['label'].get('value'))

        for result_id in result_set:
            unique_ids = map_set.get(result_id)
            for unique_id in unique_ids:
                if len(result_set[result_id]) == 0:
                    logging.info("id {} has no label".format(result_id))
                elif len(result_set[result_id]) == 1:
                    if author is False:
                        value = result_set[result_id].pop()
                        result_set[result_id].add(value)
                        self.data[unique_id]['id_label'] = value
                    else:
                        value = result_set[result_id].pop()
                        result_set[result_id].add(value)
                        self.data[unique_id]['author_0_label'] = value

                else:
                    if author is False:
                        value = result_set[result_id].pop()
                        value2 = result_set[result_id].pop()
                        result_set[result_id].add(value)
                        result_set[result_id].add(value2)
                        self.data[unique_id]['id_label'] = value
                        self.data[unique_id]['alt_label'] = value2
                    else:
                        value = result_set[result_id].pop()
                        result_set[result_id].add(value)
                        self.data[unique_id]['author_0_label'] = value

    def run(self):
        """
        Runs main cleaning program
        :return:
        """

        start_time = time.time()

        if self.file_found is not True:
            return None

        # ID's where all values are squashed to one column
        combined_ids = []

        # Fill the above two values by reading json
        with open("wikidata_extraction/work_properties.json", "r") as fp:
            all_headers = json.load(fp)
        for group in all_headers:
            for col in all_headers[group]['cols']:
                if all_headers[group]['cols'][col].get("combine_cols") == "True":
                    combined_ids.append(col)
                    if all_headers[group]['cols'][col].get("labeled") == "True":
                        combined_ids.append(f"{col}_label")

        ids = tuple(self.data)

        # Remove leading and trailing blanks
        for id in ids:
            cols = tuple(self.data[id])
            for col in cols:
                self.data[id][col] = remove_blanks(self.data[id][col])

        if self.author_base is False:
            # Deal with non english work_id labels
            label_set = set()
            label_map = {}
            for id in ids:
                id_label = self.data[id].get('id_label')
                if id_label is None:
                    continue
                if id_label[0] == "Q":
                    if len(id_label) > 1:
                        if self.data[id].get('id_label')[1] in {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}:
                            title = self.data[id].get('title_0')
                            if title is not None:
                                self.data[id]['id_label'] = title
                                self.data[id].pop('title_0')
                            else:
                                # If no title given, add it to the label_set used for querying
                                label_set.add(self.data[id]['work_id'])
                                if self.data[id]['work_id'] not in label_map:
                                    label_map[self.data[id]['work_id']] = set()
                                label_map[self.data[id]['work_id']].add(id)
                        else:
                            continue
                    if len(label_set) >= self.max_label_query:
                        self.get_bulk_labels(label_set, label_map)
                        label_set = set()
                        label_map = {}

            # Run bulk labels one last time
            if len(label_set) > 0:
                self.get_bulk_labels(label_set, label_map)

            for id in ids:
                # Clean Publication Date
                cols = list(i for i in self.data[id] if 'publicationDate' in i)
                if len(cols) != 0:
                    self.check_year_duplicates(id, cols)

                # If no alt_label exists get one from title
                if self.data[id].get("alt_label") is None:
                    title = remove_blanks(self.data[id].get('title_0'))
                    if title is not None:
                        if title not in self.data[id].get('id_label'):
                            self.data[id]['alt_label'] = title

                # Concatenate multiple id's columns
                for combined_id in combined_ids:
                    first_col = self.data[id].get(combined_id)
                    if first_col is None:
                        first_col = ''
                    self.data[id][combined_id] = ''

                    current_count = 1
                    next_col = self.data[id].get(combined_id.replace('_0', '_' + str(current_count)))
                    if next_col is None:
                        self.data[id][combined_id] = first_col
                    else:
                        # all_ids will collect all the rest of the columns that should be grouped with this one (ie _1 _2 etc.)
                        all_ids = ''
                        while next_col is not None:
                            all_ids += " , " + self.data[id].pop(combined_id.replace('_0', '_' + str(current_count)))
                            next_col = self.data[id].get(combined_id.replace('_0', '_' + str(current_count + 1)))
                            current_count += 1
                            if current_count >= self.max_headers:
                                all_ids += ', ...'
                                while next_col is not None:
                                    self.data[id].pop(combined_id.replace('_0', '_' + str(current_count)))
                                    next_col = self.data[id].get(combined_id.replace('_0', '_' + str(current_count + 1)))
                                    current_count += 1
                                break
                        self.data[id][combined_id] = first_col + all_ids

        for id in ids:

            # Iterate through each col in each individual ID
            cols = tuple(self.data[id])
            for col in cols:

                if self.author_base is False:

                    # Pop a random alt_label
                    if col == "alt_label":
                        alt_titles = self.data[id].get(col).split(", ")
                        for title in alt_titles:
                            if title != self.data[id].get('id_label'):
                                self.data[id]['alt_label'] = title
                                break

                # Reduce date of birth and death to one year
                if col == 'author_0_dateOfBirth' or col == 'author_0_dateOfDeath':
                    self.check_year_duplicates(id, [col])

                # If literal author exists and author label does not, swap
                elif col == 'literalAuthor':
                    # If literal author exists but author doesn't update label
                    if self.data[id].get('author_0') is None:
                        self.data[id]['author_0_label'] = self.data[id].pop(col)

                # NAME hierarchy
                # --------------
                # 1. author name label (literal name used if not defined)
                # 2. combined names (chosen at random)
                # 3. name in native language
                # 4. alt_label (chosen at random)

                elif col == 'author_0_label':

                    # Create combined names and check if they exist in author_label or author_alt_label
                    first_names = self.data[id].get('author_0_givenName_label_0')
                    if first_names is None:
                        first_names = []
                    else:
                        first_names = [i.replace(" ", '') for i in first_names.split(" , ")]

                    last_names = self.data[id].get('author_0_familyName_label_0')
                    if last_names is None:
                        last_names = []
                    else:
                        last_names = [i.replace(" ", '') for i in last_names.split(" , ")]

                    combined_names = set()
                    if len(first_names) == 1 and len(last_names) >= 1:
                        for first_name in first_names:
                            for last_name in last_names:
                                combined = first_name + " " + last_name
                                if similar(combined, self.data[id][col]) > 0.95:
                                    first_names.remove(first_name)
                                    last_names.remove(last_name)
                                else:
                                    combined_names.add(combined)
                    combined_names = list_to_string(combined_names, self.data[id].get('author_label_alt_0'), self.data[id]['author_0_label'])
                    if len(combined_names) > 0:
                        self.data[id]['author_0_name_1'] = combined_names.split(" , ")[0]

                # Remove name in native language if it is a repeat
                elif 'nameInNativeLanguage' in col:

                    native_name = self.data[id].get(col)
                    if native_name is None:
                        pass
                    else:
                        native_name = native_name.split(",")[0]
                        if native_name in (self.data[id].get('author_0_label'),
                                    self.data[id].get('author_label_alt_0'),
                                    self.data[id].get('author_0_name_1')):
                            self.data[id].pop(col)
                        else:
                            # If combined name was not found use native name
                            if self.data[id].get('author_0_name_1') is None:
                                self.data[id]['author_0_name_1'] = native_name
                            else:
                                # If combined name was found use native name as name_2
                                self.data[id]['author_0_name_2'] = native_name

        # Final name squash
        for id in self.data:

            # If there are any remaining empty name columns, fill with random alt_name
            alt_names = self.data[id].get('author_label_alt_0')
            if alt_names is None:
                continue
            alt_names = alt_names.split(", ")

            if self.data[id].get("author_0_name_1") is None and len(alt_names) > 0:
                self.data[id]["author_0_name_1"] = alt_names.pop(0)
            if self.data[id].get("author_0_name_2") is None and len(alt_names) > 0:
                self.data[id]["author_0_name_2"] = alt_names.pop(0)

        # Check for Q valued names
        label_set = set()
        label_map = {}
        for id in self.data:
            label = self.data[id].get('author_0_label')
            if label is None or len(label) < 2:
                continue
            if label[0] == "Q":
                if label[1] in {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}:
                    author_id = self.data[id].get("author_0")
                    if author_id is None:
                        continue
                    label_set.add(author_id)
                    if author_id not in label_map:
                        label_map[author_id] = set()
                    label_map[author_id].add(id)
            if len(label_set) >= self.max_label_query:
                self.get_bulk_labels(label_set, label_map, True)
                label_set = set()
                label_map = {}
        if len(label_set) > 0:
            self.get_bulk_labels(label_set, label_map, True)

        # Convert JSON dictionary to dataframe
        df = pd.DataFrame.from_dict(self.data, 'index', dtype=object)
        df = df.reindex(sorted(df.columns), axis=1)

        # Remove blank cols
        non_null_columns = [col for col in df.columns if df.loc[:, col].notna().any()]
        df = df[non_null_columns]

        # print(f"Wikidata author_base: {self.author_base}")
        # print(f"Wikidata columns at the start of cleaning:\n {df.columns.tolist()}\n")

        # Add prefixes to identifiers
        if 'oclcControl_0' in df:
            df['oclcControl_0'] = df['oclcControl_0'].apply(
                lambda x: add_url_to_id_pandas(x, 'http://worldcat.org/OCLC/') if not pd.isnull(x) else None)
        if 'author_0_viafID_0' in df:
            df['author_0_viafID_0'] = df['author_0_viafID_0'].apply(
                lambda x: add_url_to_id_pandas(x, "http://viaf.org/viaf/") if not pd.isnull(x) else None)
        if 'viafId_0' in df:
            df['viafId_0'] = df['viafId_0'].apply(
                lambda x: add_url_to_id_pandas(x, "http://viaf.org/viaf/") if not pd.isnull(x) else None)

        # Remove extra name columns
        if self.author_base is False:
            df.rename(columns={'work_id': 'work_Wikidata_URI'}, inplace=True)
            df.rename(columns={'id_label': 'work_Name_1'}, inplace=True)
            df.rename(columns={'alt_label': 'work_Name_2'}, inplace=True)
            df.rename(columns={'countryOfOrigin_0': 'work_CountryOrigin_URI'}, inplace=True)
            df.rename(columns={'countryOfOrigin_0_label': 'work_CountryOrigin'}, inplace=True)
            df.rename(columns={'editionNumber_0': 'work_Edition'}, inplace=True)
            df.rename(columns={'formatOfCreativeWork_0': 'work_Format_URI'}, inplace=True)
            df.rename(columns={'formatOfCreativeWork_0_label': 'work_Format'}, inplace=True)
            df.rename(columns={'isbn10_0': 'work_ISBN10_ID'}, inplace=True)
            df.rename(columns={'isbn13_0': 'work_ISBN13_ID'}, inplace=True)
            df.rename(columns={'languageOfWorkOrName_0': 'work_Language_URI'}, inplace=True)
            df.rename(columns={'languageOfWorkOrName_0_label': 'work_Language'}, inplace=True)
            df.rename(columns={'oclcWorkId_0': 'work_OCLC_ID'}, inplace=True)
            df.rename(columns={'oclcControl_0': 'work_OCLCcontrol_ID'}, inplace=True)
            df.rename(columns={'publicationDate_0': 'work_PublicationDate'}, inplace=True)
            df.rename(columns={'publisher_0': 'work_Publisher_URI'}, inplace=True)
            df.rename(columns={'publisher_0_label': 'work_Publisher'}, inplace=True)
            df.rename(columns={'publicationPlace_0': 'work_PublicationPlace_URI'}, inplace=True)
            df.rename(columns={'publicationPlace_0_label': 'work_PublicationPlace'}, inplace=True)
            df.rename(columns={'viafId_0': 'work_VIAF_URI'}, inplace=True)
            df.rename(columns={'issn_0': 'work_ISSN_ID'}, inplace=True)

            # add any missing columns so it doesn't cause errors in next steps because it is missing
            desired_headers = [
                'work_Wikidata_URI',
                'work_Name_1',
                'work_Name_2',
                'work_CountryOrigin_URI',
                'work_CountryOrigin',
                'work_Edition',
                'work_Format_URI',
                'work_Format',
                'work_ISBN10_ID',
                'work_ISBN13_ID',
                'work_Language_URI',
                'work_Language',
                'work_OCLC_ID',
                'work_OCLCcontrol_ID',
                'work_PublicationDate',
                'work_Publisher_URI',
                'work_Publisher',
                'work_PublicationPlace_URI',
                'work_PublicationPlace',
                'work_VIAF_URI',
                'work_ISSN_ID',
                'work_Description']
            for desired_header in desired_headers:
                if desired_header not in list(df.columns.values):
                    df[desired_header] = ""

        df.rename(columns={'author_0_isni_0': 'author_ISNI_ID'}, inplace=True)
        df.rename(columns={'author_0_viafID_0': 'author_VIAF_URI'}, inplace=True)
        df.rename(columns={'author_0': 'author_Wikidata_URI'}, inplace=True)
        df.rename(columns={'author_0_dateOfBirth': 'author_BirthYear'}, inplace=True)
        df.rename(columns={'author_0_dateOfDeath': 'author_DeathYear'}, inplace=True)
        df.rename(columns={'author_0_label': 'author_Name_1'}, inplace=True)
        df.rename(columns={'author_0_name_1': 'author_Name_2'}, inplace=True)
        df.rename(columns={'author_0_sexOrGender_label_0': 'author_Sex'}, inplace=True)
        df.rename(columns={'instanceOf_0': 'work_WikidataType_URI'}, inplace=True)
        df.rename(columns={'instanceOf_0_label': 'work_WikidataType'}, inplace=True)
        df.rename(columns={'genre_0': 'work_Genre_URI'}, inplace=True)
        df.rename(columns={'genre_0_label': 'work_Genre'}, inplace=True)

        # add any missing columns so it doesn't cause errors in next steps because it is missing
        desired_headers = [
            'author_ISNI_ID',
            'author_VIAF_URI',
            'author_Wikidata_URI',
            'author_BirthYear',
            'author_DeathYear',
            'author_Name_1',
            'author_Name_2',
            'author_Sex',
            'work_WikidataType_URI',
            'work_WikidataType',
            'work_Genre_URI',
            'work_Genre',
            'author_Description',
            'work_Description']
        for desired_header in desired_headers:
            if desired_header not in list(df.columns.values):
                df[desired_header] = ""

        # Remove Wikidata URIs that contain "well-known/genid". These represent an "unknown" value in Wikidata
        for header in list(df.columns.values):
            if "_URI" in header:
                df[header] = df[header].apply(lambda x: fix_wikidata_unknown(x) if not pd.isnull(x) else None)

        # Change index to incremented numerical value
        df = df.reset_index()

        remove_cols = ('literalAuthor',
                       'author_0_nameInNativeLanguage',
                       'title',
                       'author_0_familyName_label_0',
                       'author_0_givenName_label_0',
                       'author_0_label_alt',
                       'author_0_familyName_0',
                       'author_0_givenName_0',
                       'author_0_sexOrGender_0',
                       'countryOfOrigin_',
                       'editionNumber_',
                       'formatOfCreativeWork_',
                       'languageOfWorkOrName_',
                       'unique_id',
                       'index',
                       'author_0_name_2'
                       )

        for col in df:
            for remove_col in remove_cols:
                if remove_col in col:
                    try:
                        df.pop(col)
                    except Exception as e:
                        continue

        # Make workID and names first columns
        if self.author_base is False:
            first_col = df.pop("work_Name_2")
            df.insert(0, "work_Name_2", first_col)
            first_col = df.pop("work_Name_1")
            df.insert(0, "work_Name_1", first_col)
            first_col = df.pop("work_Wikidata_URI")
            df.insert(0, "work_Wikidata_URI", first_col)

        # Label and sort index
        df.index.name = 'unique_id'

        # Save as TSV and json
        if self.author_base is False:
            logging.info("Cleaning work base took: {:.1f} minutes".format((time.time() - start_time) / 60))
            df.to_csv("wikidata_extraction/results/work_results_cleaned_v1.tsv", index=True, sep='\t')
            #df.to_csv("wikidata_extraction/results/work_results_cleaned.tsv", index=True, sep='\t')


            # Save a sample version of the data.
            # Save the first 5% of rows
            n = 5
            df_sample = df.head(int(len(df) * (n / 100)))
            df_sample.to_csv("wikidata_extraction/results/work_results_sample_cleaned_v1.tsv", index=True, sep='\t')

        else:
            logging.info("Cleaning author base took: {:.1f} minutes".format((time.time() - start_time) / 60))
            df.to_csv("wikidata_extraction/results/author_results_cleaned_v1.tsv", index=True, sep='\t')
            #df.to_csv("wikidata_extraction/results/author_results_cleaned.tsv", index=True, sep='\t')

            # Save a sample version of the data.
            # Save the first 5% of rows
            n = 5
            df_sample = df.head(int(len(df) * (n / 100)))
            df_sample.to_csv("wikidata_extraction/results/author_results_sample_cleaned_v1.tsv", index=True, sep='\t')
