import pickle

aba = pickle.load(open("author_base_authors.p", "rb"))
abw = pickle.load(open("author_base_works.p", "rb"))
wbw = pickle.load(open("work_base_works.p", "rb"))
all_works = abw.union(wbw)
common_works = abw.intersection(wbw)
works_in_ab_not_wb = [i for i in abw if i not in wbw]
works_in_wb_not_ab = [i for i in wbw if i not in abw]
print("There are {} author based works".format(len(abw)))
print("There are {} work based works".format(len(wbw)))
print("There are {} common works in both".format(len(common_works)))
print("There is a total of {} works".format(len(all_works)))
assert((len(wbw)-len(works_in_wb_not_ab)) == (len(abw)-len(works_in_ab_not_wb)))
assert((len(all_works)) == (len(wbw)+len(abw)-len(common_works)))
print("wow")