from SPARQLWrapper import SPARQLWrapper, JSON
import datetime
import sys
from difflib import SequenceMatcher
import logging

# logging.basicConfig(level=logging.DEBUG, filename="logfile", filemode="a+",
#                     format="%(asctime)-15s %(levelname)-8s %(message)s")

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format="%(asctime)-15s %(levelname)-8s %(message)s")


class MySet(set):
    """
    Subclass of set so I can override the __repr__ for easier viewing in pandas / debugging
    """
    def __repr__(self):
        """
        :return: If set is empty returns a empty string, if otherwise returns multiple values with comma seperation
        """
        if len(self) == 0:
            return ''
        output = ''
        for elm in self:
            output += str(elm) + " , "
        return output[:-2]


class Query:

    def __init__(self):
        self.label_fails = 0
        self.entity_fails = 0
        self.endpoint_url = "https://query.wikidata.org/sparql"
        self.user_agent = "LINCS-https://lincsproject.ca//%s.%s" % (sys.version_info[0], sys.version_info[1])


    def get_results(self, query,
                    query_type=None,
                    timeout=100000,
                    max_attempts=5,
                    bulk_values=None,
                    split_amount=2):
        """
        Performs Sparql query using self.endpoint_url
        :param query: wikidata query
        :param query_type: either entity or label, used for documenting
        :param timeout: timeout for wikidata query
        :param bulk_values: a set of values, if true bulk query will be done and will split up values if it times out
        :param max_attempts: max attempts to try again
        :param split_amount: equal portions that bulk values will be split into
        :return:
        """
        try:
            # user_agent = "WDQS-example Python/%s.%s" % (sys.version_info[0], sys.version_info[1])
            # Learn more about user_agent; see https://w.wiki/CX6
            sparql = SPARQLWrapper(self.endpoint_url, agent=self.user_agent)

            if bulk_values is not None:
                total_values = len(bulk_values)
                all_results = {}
                bulk = ''
                for uri in bulk_values:
                    bulk += "wd:" + uri.split("/")[-1] + " "
                sparql.setQuery(query.replace("?ALL_VALUES", bulk))
            else:
                sparql.setQuery(query)

            sparql.setReturnFormat(JSON)
            sparql.setTimeout(timeout)
            results = sparql.query().convert()
            if results is not None:
                return results['results']['bindings']
            else:
                return None

        except Exception as e:
            attempts = 1
            if query_type == 'entity':
                self.entity_fails += 1
                logging.info("entity fail")
            if query_type == 'label':
                self.label_fails += 1
                logging.info("label fail")

            while True:

                if attempts > max_attempts:
                    logging.info("Max query attempts reached")
                    logging.info(query)
                    return None

                # If bulk query failed keep splitting into equal segments
                if bulk_values is not None:
                    bulk_split = bulk.split(" ")[:-1]
                    bulk_size = int(total_values / split_amount)
                    for i in range(0, len(bulk_values), bulk_size):
                        bulk = ''
                        start = i
                        end = i + bulk_size
                        if end > len(bulk_split):
                            end = len(bulk_split)
                        for idx in range(start, end):
                            bulk += bulk_split[idx] + " "
                        try:
                            if start == 0:
                                sparql.setQuery(query.replace("?ALL_VALUES", bulk))
                                all_results = sparql.query().convert()
                                if all_results is not None:
                                    all_results = all_results['results']['bindings']
                                else:
                                    continue
                            else:
                                sparql.setQuery(query.replace("?ALL_VALUES", bulk))
                                append_results = sparql.query().convert()
                                if append_results is not None:
                                    all_results = all_results + append_results['results']['bindings']
                        except Exception as e:
                            # If query fails double the split amount and try again
                            attempts += 1
                            if query_type == 'entity':
                                self.entity_fails += 1
                            if query_type == 'label':
                                self.label_fails += 1
                            split_amount *= 2
                            break
                    return all_results

                # If not a bulk query, just try again
                else:
                    try:
                        results = sparql.query().convert()
                        return results
                    except:
                        attempts += 1

            logging.info("data had to be split {} times".format(attempts))
            return all_results


def add_url_to_id_pandas(values, url):
    result = ''
    values = values.replace("  ", " ")
    for value in values.split(" , "):
        if value.replace(" ", "") != "":
            result += f"{url}{value} , "
    return result[:-3]


def fix_wikidata_unknown(x):
    if "well-known/genid" in str(x):
        return ""
    else:
        return x


def list_to_string(a_list, alt_labels, id_labels):
    """
    Takes a list of values and adds to output if not in alt_labels/id_labels
    :param a_list: generally will be a list of names created by a combination of first name and all last names
    :param alt_labels: alt_labels column values
    :param id_labels: id_labels column values
    :return: string of comma separated names from a_list that are not in alt_lables / id_labels
    """
    if len(a_list) == 0:
        return ''
    output = ''
    if alt_labels is not None:
        alt_labels = alt_labels.split(", ")
    else:
        alt_labels = ''
    id_labels = id_labels.split(",")

    for elm in a_list:
        # If combined name not in alt_label or id_labels add it to output
        elm = remove_blanks(elm)
        if elm in alt_labels or elm in id_labels:
            continue
        output += str(elm) + " , "
    # Remove extra " , "
    return output[:-2]


def parse_year(year):
    """
    Parses different types of datetime object
    :param year: year datetime object
    :return:
    """
    try:
        if year[0] == 't':
            return None
        year = datetime.datetime.strptime(year.split("T")[0], '%Y-%m-%d').year
        return year
    except:
        try:
            # If date starts with a negative it is bce
            if year[0] == "-":
                year = year.split("-")[1]
                if len(year) == 4:
                    return -int(str(year))
            else:
                raise Exception
        except:
            if year[0] != 't':
                logging.info("removing invalid year {}".format(year))
            return None


def remove_blanks(string):
    """
    Used for removing leading and trailing blank spaces
    :param string:
    :return:
    """
    if string is None:
        return None
    if len(string) == 0:
        return string
    if string[0] == ' ':
        string = string[1:]
    if len(string) == 0:
        return string
    if string[-1] == ' ':
        string = string[:-1]
    return string


def print_and_log(message):
    """Prints message and logs to logfile"""
    print(message)
    logging.info(message)
