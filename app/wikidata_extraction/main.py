import getopt
import logging
import json
import datetime
from wikidata_extraction.helperFunctions import *
from wikidata_extraction.cleanData import DataClean
from wikidata_extraction.wikidataQuery import WikidataQuery
import s3Helpers
from sparkClean import clean as sparkCleaner


def main():
    #logging.basicConfig(level=logging.DEBUG, filename="logfile", filemode="a+",
    #                    format="%(asctime)-15s %(levelname)-8s %(message)s")

    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format="%(asctime)-15s %(levelname)-8s %(message)s")

    # Remove 1st argument from the list of command line arguments
    argument_list = sys.argv[1:]

    if len(argument_list) == 0:
        print_and_log("Using Default Settings Please run with -h / --help option for available commands")

    # Options
    options = "hdtapcwu"

    # Long options
    long_options = ["help", "delete", "tsv", "authors", "works", "clean", "second", "upload"]

    try:
        # Parsing argument
        arguments, values = getopt.getopt(argument_list, options, long_options)

        # Default arguments
        work_base = True
        author_base = True
        update_tsv = True
        delete_dict = False
        alt_mode = True
        clean_data = True
        upload = True

        # checking each argument
        for currentArgument, currentValue in arguments:

            if currentArgument in ("-h", "--help"):
                print("\t-d --delete : will delete the work label dictionary and rebuild it from scratch")
                print("\t-t --tsv : will not overwrite/update the tsv and json output")
                print("\t-a --authors : skips creating a tsv with just author properties\n")
                print("\t-w --works : skips creating work based author+work properties\n")
                print("\t-c --clean : Skips cleaning results.tsv and author_results.tsv\n")
                print("\t-s --second : different method that keeps multiple authors in one row (disables alt_mode\n")
                print("\t-u --upload : skip uploading to s3\n")
                exit()

            if currentArgument in ("-d", "--delete"):
                if (input("### Are you sure you want to delete the label dictionary? y/[N] :").lower()) == 'y':
                    delete_dict = True

            if currentArgument in ("-t", "--tsv"):
                print("### Will not overwrite/update tsv and json output")
                update_tsv = False

            if currentArgument in ("-w", "--works"):
                print("### Skipping getting work base results")
                work_base = False

            if currentArgument in ("-a", "--authors"):
                print("### Skipping Creating author tsv")
                author_base = False

            if currentArgument in ("-c", "--clean"):
                print("### Skipping Cleaning results.tsv and author_results.tsv")
                clean_data = False

            if currentArgument in ("-s", "--second"):
                print("### Bulking multiple authors into single rows")
                alt_mode = False

            if currentArgument in ("-u", "--upload"):
                print("### Skipping upload to S3")
                upload = False

    except getopt.error as err:
        # output error, and return with an error code
        print(str(err))

    logging.info("Starting Wikidata extraction with arguments {}".format(arguments))

    with open("wikidata_extraction/results/wikidata_metadata.json", "w") as meta_fp:
        json.dump({"authorityVersion": f"SPARQL queries on {str(datetime.date.today())}",
                   "authorityDate": str(datetime.date.today()),
                   "authoritySource": "https://query.wikidata.org/"}, meta_fp)

    wiki = WikidataQuery(delete_dict=delete_dict)

    # Get all author properties
    if author_base is True:
        wiki.build_author_base(update_tsv, get_works=False)

    if work_base is True:
        wiki.build_work_base(alt_mode, update_tsv)

    if clean_data is True:
        if work_base is True:
            cleaner = DataClean(author_base=False, max_label=wiki.max_label_query, service=wiki)
            cleaner.run()

            # Run the second cleaning step that uses spark to split the data into the full version, the id version, and the context version
            sparkCleaner("wikidata_works", "wikidata_extraction/results/work_results_cleaned_v1.tsv", "wikidata_extraction/results/spark_cleaned_wikidata_works_results")
            sparkCleaner("wikidata_works", "wikidata_extraction/results/work_results_sample_cleaned_v1.tsv", "wikidata_extraction/results/spark_cleaned_wikidata_works_sample_results")

        if author_base is True:
            cleaner = DataClean(author_base=True, max_label=wiki.max_label_query, service=wiki)
            cleaner.run()

            # Run the second cleaning step that uses spark to split the data into the full version, the id version, and the context version
            sparkCleaner("wikidata_authors", "wikidata_extraction/results/author_results_cleaned_v1.tsv", "wikidata_extraction/results/spark_cleaned_wikidata_authors_results")
            sparkCleaner("wikidata_authors", "wikidata_extraction/results/author_results_sample_cleaned_v1.tsv", "wikidata_extraction/results/spark_cleaned_wikidata_authors_sample_results")

    if upload is True:
        s3 = s3Helpers.S3Helper()
        s3.bulk_upload("wikidata_works")
        s3.bulk_upload("wikidata_authors")

        s3.bulk_upload("wikidata_works_sample")
        s3.bulk_upload("wikidata_authors_sample")


if __name__ == "__main__":
    while True:
        main()
