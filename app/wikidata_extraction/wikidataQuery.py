import wikidata_extraction.buildQuerys as bq
import time
import json
from wikidata_extraction.helperFunctions import *
import pandas as pd
import pickle
from alive_progress import alive_bar
import logging
import os


class WikidataQuery:
    """
    Generally speaking this class builds a JSON with each key representing the header column and values being stored in
    sets to remove repeats
    """

    def __init__(self,
                 delete_dict,
                 language="en",
                 max_authors=15,
                 # When doing a bulk query for works & author properties this is the limit as to how many entities are
                 # queried at once
                 max_entity_query=350,
                 # This is the max amount of headers used when a column has multiple results
                 max_headers=20,
                 # This is used when looking up common work headers
                 max_label_query=200):
        # logging.basicConfig(level=logging.DEBUG, filename="logfile", filemode="a+",
        #                     format="%(asctime)-15s %(levelname)-8s %(message)s")

        logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format="%(asctime)-15s %(levelname)-8s %(message)s")
        self.language = language
        self.max_authors = max_authors
        self.max_entity_query = max_entity_query
        self.max_headers = max_headers
        self.max_label_query = max_label_query

        with open("wikidata_extraction/work_properties.json") as fp:
            self.query_metadata = json.load(fp)
        # Bulk label query is straight forward, does not need to be built using json file
        with open("wikidata_extraction/queries/bulk_labels.rq") as fp:
            self.bulk_label_query = fp.read()
        # Update label query language
        self.bulk_label_query = self.bulk_label_query.replace("?LANGUAGE", language)
        # Build skeleton query's using work_properties.json
        self.prop_query = bq.build_query(self, uri_type="work")
        self.author_query = bq.build_query(self, uri_type="author")
        self.author_base_query = bq.build_query(self, uri_type="author", author_base=True)
        self.work_labeled_headers = bq.get_labeled_headers(self, uri_type="work")
        # If label map exists, load it, else create it
        try:
            with open("wikidata_extraction/data/label_dictionary.json") as fp:
                if delete_dict is True:
                    self.uri_to_label_map = {}
                else:
                    self.uri_to_label_map = json.load(fp)
        except Exception as e:
            # If file does not exist
            self.uri_to_label_map = {}

        self.df = None
        self.all_results = {}
        self.author_results = {}
        self.alt_rows = set()
        # Except for author_base_works these variables are mainly used for testing differences
        self.author_base_works = set()
        self.work_base_works = set()
        self.author_base_authors = set()
        self.work_base_authors = set()
        self.query = Query()
        self.instanceSet = {}

    def alt_mode_split(self):
        """
        Iterates through each row with multiple authors, creates a row for each author with all fields duplicated.  Each
        row has a index unique id in the form of workID_authorID.
        :return:
        """
        for work in self.alt_rows:
            authors = self.all_results[work].get('author_0')
            for i in range(1, len(authors)):
                author = self.all_results[work].get('author_0').pop()
                self.all_results[work + "_" + author] = dict(self.all_results[work])
                self.all_results[work + "_" + author]['author_0'] = author
            # Convert myset to a string
            self.all_results[work]['author_0'] = self.all_results[work]['author_0'].pop()

    def build_author_base(self, update_tsv, get_works=False):
        """
        Used for getting all properties from a set of authors and writing results to self.author_results dictionary
        :param update_tsv: if true will overwrite existing author_results.tsv/json
        :param get_works: defaults to false as we use work base works instead of author based works
        """
        logging.info("Building author properties base")

        start_time = time.time()
        with open("wikidata_extraction/queries/get_authors.rq", "r") as file:
            query = file.read()

            # check if we set a query limit as environment variable.
            # Used for faster testing so we only get a set number of authors
            QUERY_LIMIT_BOOL = os.environ.get("QUERY_LIMIT_BOOL")
            if QUERY_LIMIT_BOOL == "True":
                QUERY_LIMIT = os.environ.get("QUERY_LIMIT")
                if QUERY_LIMIT:
                    query = query + f" LIMIT {QUERY_LIMIT}"

        authors = self.query.get_results(query)
        with open("wikidata_extraction/queries/bulk_author_properties2.rq", "r") as file:
            query = file.read()
        with open("wikidata_extraction/queries/bulk_author_works.rq") as file:
            query2 = file.read()
        with alive_bar(len(authors)) as bar:
            author_set = set()
            count = 0
            for author in authors:
                bar()
                result = author['author'].get("value")
                self.author_base_authors.add(result)
                author_set.add(result)
                count += 1
                if count >= self.max_entity_query or author is authors[-1]:
                    # Get author properties
                    results = self.query.get_results(query, bulk_values=author_set, query_type='entity')
                    if results is None:
                        print("Potential error in query")
                        continue
                    for result in results:
                        wiki_id = result['author'].get('value')
                        if self.author_results.get(wiki_id) is None:
                            self.author_results[wiki_id] = {}
                        for header in result:
                            if header != "author":
                                value = self.author_results[wiki_id].get(header)
                                if value is None:
                                    self.author_results[wiki_id][header] = MySet()
                                self.author_results[wiki_id][header].add(result[header].get('value'))

                    # Get author works NOT NECCESSARY, get_works is false by default
                    if get_works is True:
                        results2 = self.query.get_results(query2, bulk_values=author_set, query_type='entity')
                        if results2 is None:
                            print("Potential error in query")
                            continue
                        for result in results2:
                            self.author_base_works.add(result['work'].get("value"))

                    author_set = set()
                    count = 0

        pickle.dump(self.author_base_authors, open("wikidata_extraction/data/author_base_authors.p", "wb"))
        pickle.dump(self.author_base_works, open("wikidata_extraction/data/author_base_works.p", "wb"))

        # Convert myset to string by using MySets() repr() override
        for wiki_id in self.author_results:
            for header in self.author_results[wiki_id]:
                if header != "author":
                    self.author_results[wiki_id][header] = repr(self.author_results[wiki_id][header])

        for author_id in self.author_results:
            self.author_results[author_id]['author_0'] = author_id
        df = pd.DataFrame.from_dict(self.author_results, 'index', dtype=object)
        df.index.name = 'authorID'

        if update_tsv is True:
            logging.info("Updating local tsv for Wikidata author base")
            df.to_csv("wikidata_extraction/results/author_results.tsv", index=True, sep='\t')
            with open("wikidata_extraction/results/author_results.json", "w") as outfile:
                json.dump(self.author_results, outfile)
        print_and_log("Building author base took: {:.1f} minutes".format((time.time() - start_time) / 60))

    def bulk_labels(self, uri_to_col_map):
        """
        Used exclusively for work -> work_properties columns maps, not to be used for authors
        :param uri_to_col_map:
        :return: None
        """
        uris = set()
        for all_uris in uri_to_col_map:
            for pairs in uri_to_col_map[all_uris]:
                uris.add(pairs[0])

        results = self.query.get_results(self.bulk_label_query, bulk_values=uris, query_type='label')
        if results is None:
            return None

        for label_result in results:
            self.uri_to_label_map.update({label_result['subject'].get("value"): label_result['label'].get("value")})
        for all_uris in uri_to_col_map:
            for uri_set in uri_to_col_map[all_uris]:
                self.all_results[all_uris][uri_set[1]+"_label"] = self.uri_to_label_map.get(uri_set[0])

    def build_work_base(self, alt_mode=True, update_tsv=True):
        """
        Used for getting all properties from a collection of works
        :param alt_mode: if True will create a new row for each author of a single work
        :param update_tsv: if True will overwrite existing work_results.tsv/json
        :return:
        """

        start_time = time.time()
        with open('wikidata_extraction/queries/get_works1.rq', 'r') as file:
            query1 = file.read()
        with open('wikidata_extraction/queries/get_works2.rq', 'r') as file:
            query2 = file.read()
        with open('wikidata_extraction/queries/get_remove_works1.rq', 'r') as file:
            remove_query1 = file.read()
        with open('wikidata_extraction/queries/get_remove_works2.rq', 'r') as file:
            remove_query2 = file.read()

        # check if we set a query limit as environment variable.
        # Used for faster testing so we only get a set number of works
        QUERY_LIMIT_BOOL = os.environ.get("QUERY_LIMIT_BOOL")
        if QUERY_LIMIT_BOOL == "True":
            QUERY_LIMIT = os.environ.get("QUERY_LIMIT")
            if QUERY_LIMIT:
                query1 = query1 + f" LIMIT {QUERY_LIMIT}"
                query2 = query2 + f" LIMIT {QUERY_LIMIT}"

        work_set, remove_set = set(), set()

        works1 = self.query.get_results(query1)
        works2 = self.query.get_results(query2)
        if works1:
            for i in works1:
                work_set.add(i['work']['value'])
        else:
            print("Work query 1 failed.")
        if works2:
            for i in works2:
                work_set.add(i['work']['value'])
        else:
            print("Work query 2 failed.")

        remove_works1 = self.query.get_results(remove_query1)
        remove_works2 = self.query.get_results(remove_query2)
        if remove_works1:
            for i in remove_works1:
                remove_set.add(i['work']['value'])
        else:
            print("Remove work query 1 failed.")
        if remove_works2:
            for i in remove_works2:
                remove_set.add(i['work']['value'])
        else:
            print("Remove work query 2 failed.")

        work_set = work_set - remove_set

        self.get_work_properties(alt_mode=alt_mode, work_set=work_set)
        self.get_work_labels()
        if alt_mode is True:
            self.alt_mode_split()
        self.get_author_prop_and_labels()
        self.convert_set_to_strings()

        with open("wikidata_extraction/data/label_dictionary.json", "w") as outfile:
            json.dump(self.uri_to_label_map, outfile)

        self.df = pd.DataFrame.from_dict(self.all_results, 'index', dtype=object)

        # Reorder columns by name
        self.df = self.df.reindex(sorted(self.df.columns), axis=1)

        # Make work and id_label the first columns
        self.df.insert(0, 'id_label', self.df.pop("id_label"))
        if alt_mode is True:
            self.df.insert(0, 'work_id', self.df.pop("work_id"))

        # Label and sort index
        self.df.index.name = 'unique_id'
        self.df = self.df.sort_index()

        # Save as TSV
        if update_tsv is True:
            self.df.to_csv("wikidata_extraction/results/work_results.tsv", index=True, sep='\t')
            with open("wikidata_extraction/results/work_results.json", "w") as outfile:
                json.dump(self.all_results, outfile)
        print_and_log("Building work base took: {:.1f} minutes".format((time.time() - start_time) / 60))

    def convert_set_to_strings(self):
        """
        All author properties are stored as mySets to remove duplicates, you can convert to a string using repr command
        for mySet.  If one column has multiple results it will be converted to a comma seperated list, one could easily
        override this to return a list using the json output.
        :return:
        """
        start_time = time.time()
        for result in self.all_results:
            for header in self.all_results[result]:
                if type(self.all_results[result][header]) == MySet:
                    # Convert MySets to strings
                    self.all_results[result][header] = repr(self.all_results[result][header])
                    # If you would prefer a list for exporting to json use
                    # self.all_results[result][header] = list(self.all_results[result][header])

    def get_author_prop_and_labels(self):
        """
        Iterates though all_works and queries author properties and labels creating a new column for each.  Work is done
        in bulk using the bulk_authors set and work_to_authors_map to map back results.
        :return:
        """
        start_time = time.time()
        work_count = 0
        work_to_authors_map = {}
        author_to_prop_map = {}
        bulk_authors = set()
        num_results = len(self.all_results)
        # get author -> properties map
        for work in self.all_results:
            work_count += 1
            author_cols = tuple((i for i in self.all_results[work] if "author" in i))
            if len(author_cols) == 0:
                continue
            work_to_authors_map[work] = {}
            for col in author_cols:
                author_uri = self.all_results[work][col]
                # Work based authors mainly used for debugging / comparing results
                self.work_base_authors.add(author_uri)
                if author_uri in self.author_results:
                    for heading in self.author_results[author_uri]:
                        self.all_results[work][heading] = self.author_results[author_uri].get(heading)
                else:
                    work_to_authors_map[work].update({col: self.all_results[work][col]})
                    if col not in author_to_prop_map:
                        bulk_authors.add(self.all_results[work][col])

            if len(bulk_authors) > self.max_entity_query or work_count == num_results:
                results = self.query.get_results(self.author_query, bulk_values=bulk_authors, query_type='entity')
                for result in results:
                    try:
                        author = result.pop('author').get("value")
                        if author not in author_to_prop_map:
                            author_to_prop_map[author] = set()
                        for col in result:
                            author_to_prop_map[author].add((col, result[col].get('value')))
                    except Exception as e:
                        logging.info(e)
                bulk_authors = set()

        pickle.dump(self.work_base_authors, open("wikidata_extraction/data/work_base_authors.p", "wb"))
        logging.info("\tGetting all author based props and labels labels took: {:.1f} minutes".format(
            (time.time() - start_time) / 60))

        start_time = time.time()
        # Add author headers
        for work in work_to_authors_map:
            author_headers = work_to_authors_map.get(work)
            for author in author_headers:
                uri = work_to_authors_map[work].get(author)
                header_pairs = author_to_prop_map.get(uri)
                if header_pairs is None:
                    continue
                for header_pair in header_pairs:
                    header = author + "_" + header_pair[0]
                    if header not in self.all_results[work]:
                        self.all_results[work][header] = MySet()
                    self.all_results[work][header].add(header_pair[1])

        logging.info("\tMapping back author results took: {:.1f} minutes".format((time.time() - start_time) / 60))

    def get_work_labels(self):
        """
        Iterates through all results and builds a list of sets of uri's that require a label ( is_labeled == True in
        work_properties.json[work_prop][cols] )  Will pass these uri's to self.bulk_labels
        :return: None
        """
        start_time = time.time()
        uri_count = 0
        work_count = 0
        uri_to_col_map = {}
        num_results = len(self.all_results)
        for work in self.all_results:
            uri_to_col_map[work] = set()
            headers = [i for i in self.all_results[work]]
            for header in headers:
                if header.split("_")[0] in self.work_labeled_headers:
                    if self.all_results[work][header] in self.uri_to_label_map:
                        new_header = header+"_label"
                        self.all_results[work][new_header] = self.uri_to_label_map.get(self.all_results[work][header])
                    else:
                        uri_to_col_map[work].add((self.all_results[work][header], header))
                        uri_count += 1
            work_count += 1
            if (uri_count > self.max_label_query) or (work_count == num_results):
                # Remove empty sets
                works = tuple(uri_to_col_map)
                for work_to_label in works:
                    if len(uri_to_col_map[work_to_label]) == 0:
                        uri_to_col_map.pop(work_to_label)
                if len(uri_to_col_map) != 0:
                    self.bulk_labels(uri_to_col_map)
                uri_count = 0
                uri_to_col_map = {}
        logging.info("\tGetting all work based labels took: {:.1f} minutes".format((time.time() - start_time) / 60))

    def get_work_properties(self, alt_mode, work_set):
        """
        Takes in set of works and gets all properties under work_prop and ids in work_properties.json, will also
        begin populating self.all_results which is used throughout the rest of this program
        :param alt_mode: see readme for what alt_mode does
        :param work_set: query results of all work entities converted into a set of values
        :return:
        """
        start_time = time.time()
        work_count = 0
        total_count = 0
        bulk_works = set()
        for work in work_set:
            # If author base is disabled this variable will be empty
            #   Otherwise add work_base works to author_base works
            self.author_base_works.add(work)
            # Just used for checking / debugging
            self.work_base_works.add(work)
        pickle.dump(self.work_base_works, open("wikidata_extraction/data/work_base_works.p", "wb"))
        logging.info("Getting properties for {} works".format(len(self.author_base_works)))
        with alive_bar(len(self.author_base_works)) as bar:
            for work in self.author_base_works:
                bulk_works.add(work)
                work_count += 1
                total_count += 1
                bar()
                # If bulk works has more than max entities or if idx is at last work do a bulk query
                if work_count == self.max_entity_query or total_count == len(self.author_base_works):
                    # If total count equals length of works, set to zero to end loop
                    if total_count == len(self.author_base_works):
                        total_count = 0
                    work_count = 0
                    result = self.query.get_results(self.prop_query, bulk_values=bulk_works, query_type='entity')
                    if result is None:
                        #TODO assure the code never gets here
                        continue
                    # Map back results
                    for idx2 in result:
                        # Remove work ID as base of Dictionary / JSON
                        work_uri = idx2.pop('work')['value']
                        # If URI not in dictionary, add it with a blank dictionary as its value
                        if work_uri not in self.all_results:
                            self.all_results.update({work_uri: {}})
                            # Add all possible header values as a set to prevent duplicates
                            for group in self.query_metadata:
                                if group == "author_prop":
                                    continue
                                for col in self.query_metadata[group]['cols']:
                                    self.all_results[work_uri][col] = MySet()
                            # Since id_label and alt_label and work_Description were done manually we have to add ourselves
                            self.all_results[work_uri]['id_label'] = MySet()
                            self.all_results[work_uri]['alt_label'] = MySet()
                            self.all_results[work_uri]['work_Description'] = MySet()
                        else:
                            # This handles multiples results with the same work id
                            pass
                        # Iterate through all headers in result, get values and add to all_results dict
                        for header in idx2:
                            header_value = idx2[header].get("value")
                            # Remove blank nodes
                            if header_value[0] == 't' and header_value[1:].isdigit():
                                pass
                            else:
                                self.all_results[work_uri][header].add(header_value)
                    # Reset bulk works used for querying
                    bulk_works = set()

        """
        Handle header values with multiple values by creating up to a max of max_headers columns
            param: self.max_headers = max amount of values which are expanded into multiple columns
        """

        all_works = tuple(self.all_results)
        for work in all_works:

            # Swap all creator values into author if not already there
            for i in range(len(self.all_results[work]['creator_0'])):
                creator = self.all_results[work]['creator_0'].pop()
                if creator not in self.all_results[work]['author_0']:
                    self.all_results[work]['author_0'].add(creator)

            # Iterate through each column for each work
            headers = tuple(self.all_results[work])
            for header in headers:
                remove = False
                count = len(self.all_results[work][header])
                if alt_mode is True:
                    if "author" in header and count > 1:
                        if count > self.max_authors:
                            remove = True
                            self.all_results.pop(work)
                            break
                        else:
                            self.alt_rows.add(work)
                            continue
                if count > 1:
                    for i in range(1, min(count, self.max_headers)):
                        self.all_results[work][header.split("_")[0] + "_" + str(i)] = self.all_results[work][
                            header].pop()
                    # Remove any extras by randomly choosing one value
                    self.all_results[work][header] = self.all_results[work][header].pop()
                elif count == 0:
                    # If no results in the set, remove the key from dictionary
                    self.all_results[work].pop(header)
                else:
                    # If only one result pop the value from the set
                    self.all_results[work][header] = self.all_results[work][header].pop()

            # If alt mode is enabled create a work_id col
            if alt_mode is True and remove is False:
                self.all_results[work]['work_id'] = work

        logging.info("\tGetting all work based properties took: {:.1f} minutes".format((time.time() - start_time) / 60))
